package com.creativita.audi.tourapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Debug;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class Nofication_firebase extends FirebaseMessagingService {

    public static final String TAG   = "NOTICIAS";
    private static final String URL1 = "https://audiguayaquil.com/SERVICE/";
    Bitmap image;
    String UrlImg, Urli,  nombre, apellido, UrlTrack;
    String idCita;
    String tipo="";



    /*
   */

    @Override
    public void onNewToken(String mToken) {
        super.onNewToken(mToken);
        Log.e("TOKEN",mToken);
        RegistroToken RT = new RegistroToken();
        String token = FirebaseInstanceId.getInstance().getToken();

        //Guardar en preferencias

        SharedPreferences Preferens = getSharedPreferences("Load_Profile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = Preferens.edit();
        editor.putString("token", token);
        editor.commit();

        Log.d(TAG, "Token: " + token);
        //Pregunta si hay usuario logeado
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user  != null ){

            RT.enviarTokenAlServidor(token, Preferens.getString("TIPO_USER", ""),Preferens.getString("IDUSER_QUERY", ""));
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        String from = remoteMessage.getFrom();
        Log.d(TAG, "Mensaje recibido de: " + from);

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Notificación: " + remoteMessage.getNotification().getBody());
            String titulo = remoteMessage.getData().get("titulo");
            String descripciom = remoteMessage.getData().get("descripcion");

             tipo = remoteMessage.getData().get("tipo");
            idCita = remoteMessage.getData().get("id");
            Log.d(TAG, "Notificación: TIPO " + remoteMessage.getData().get("tipo"));
            Log.d(TAG, "Notificación: ID " + remoteMessage.getData().get("id"));


            nombre = remoteMessage.getData().get("nombre");
            apellido = remoteMessage.getData().get("apellido");
            UrlTrack = remoteMessage.getData().get("url");

            UrlImg=  remoteMessage.getData().get("Urli");
            Urli = UrlImg;
            if(remoteMessage.getData().get("Urli")!=null){

                        image = getBitmapFromURL(UrlImg);
            }

            //Log.d(TAG, titulo);
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user  != null ){

                mostrarNotificacion( remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(),image, tipo);


            }else {
                Log.d(TAG, "No logeado "+Home.IDUSER_QUERY);
            }
        }

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Data: " + remoteMessage.getData());
        }

    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void mostrarNotificacion(String title, String body , Bitmap img, String tipo ) {


        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
if(img!=null){
    Intent intent = new Intent(this, FotoValet.class);
    intent.putExtra("Urli", Urli );
    intent.putExtra("nombre", nombre );
    intent.putExtra("apellido", apellido );

    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
            .setSmallIcon(R.drawable.icono)
            .setContentTitle("" + title )
            .setContentText(body)
            .setAutoCancel(true)
            .setStyle(new NotificationCompat.BigPictureStyle()
            .bigPicture(img))
            .setPriority(Notification.PRIORITY_HIGH)
            .setSound(soundUri)
            .setContentIntent(pendingIntent);
    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.notify(0, notificationBuilder.build());

}else if (tipo.equals("cliente")){
    mostrarNotificacionDireccion(title, body    );
}else{
    mostrarNotificacion( title,  body  );
}


    }

    private void mostrarNotificacionTrack(String title, String body  ) {
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent intent = new Intent(this, UbicacionValetActivity.class);
        intent.putExtra("url", UrlTrack );

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        long[] vibrate = {0, 100, 200, 300};
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icono)
                .setContentTitle("" + title )
                .setContentText(body)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setVibrate(vibrate)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

    }

    private void mostrarNotificacion(String title, String body  ) {
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent intent = new Intent(this, Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        long[] vibrate = {0, 100, 200, 300};
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icono)
                .setContentTitle("" + title )
                .setContentText(body)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setVibrate(vibrate)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

    }

    private void mostrarNotificacionDireccion(String title, String body  ) {/*

        Intent intent = new Intent(this, SingleActivity.class);
        intent.putExtra("id", idCita );

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        long[] vibrate = {0, 100, 200, 300};
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icono)
                .setContentTitle(title )
                .setContentText(body)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setVibrate(vibrate)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

    */}
}
