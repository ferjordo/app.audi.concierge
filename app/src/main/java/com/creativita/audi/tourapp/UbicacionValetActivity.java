package com.creativita.audi.tourapp;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

public class UbicacionValetActivity extends AppCompatActivity {


    String TAG = "Live";
    WebView webView;
    String UrlToTrack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubicacion_valet);

    }
    public void salir(){
        //HyperTrack.completeAction(actionID);
        //HyperTrack.stopTracking();
    }



    @Override
    public void onBackPressed() {
        salir();
        super.onBackPressed();
    }

    @Override
    public void onStart() {
        super.onStart();
//        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        //      mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        //    mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        // mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        // mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //  mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //    mapView.onSaveInstanceState(outState);
    }



}
