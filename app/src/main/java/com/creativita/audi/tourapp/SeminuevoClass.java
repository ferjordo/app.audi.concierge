package com.creativita.audi.tourapp;

import java.util.List;

public class SeminuevoClass{
    String modelo, motor , traccion, equipamento, informacion_adicional;
    int id , km, ac, anio;
    double precio;

    List<String> imgs ;
    String img1, img2, img3, img4;

    public SeminuevoClass(String modelo, String motor, String traccion, String equipamento, String informacion_adicional, int id, int km, int ac, int anio, double precio, List<String> imgs) {
        this.modelo = modelo;
        this.motor = motor;
        this.traccion = traccion;
        this.equipamento = equipamento;
        this.informacion_adicional = informacion_adicional;
        this.id = id;
        this.km = km;
        this.ac = ac;
        this.anio = anio;
        this.precio = precio;
        this.imgs = imgs;
        SetSingleImage(imgs);
    }
    void SetSingleImage (List<String> imgs){
        img1 =  imgs.get(0).toString();
        img2 =  imgs.get(1).toString();
        img3 =  imgs.get(2).toString();
        img4 =  imgs.get(3).toString();
    }
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getTraccion() {
        return traccion;
    }

    public void setTraccion(String traccion) {
        this.traccion = traccion;
    }

    public String getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(String equipamento) {
        this.equipamento = equipamento;
    }

    public String getInformacion_adicional() {
        return informacion_adicional;
    }

    public void setInformacion_adicional(String informacion_adicional) {
        this.informacion_adicional = informacion_adicional;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getAc() {
        return ac;
    }

    public void setAc(int ac) {
        this.ac = ac;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public List<String> getImgs() {
        return imgs;
    }

    public void setImgs(List<String> imgs) {
        this.imgs = imgs;
    }

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }

    public String getImg4() {
        return img4;
    }

    public void setImg4(String img4) {
        this.img4 = img4;
    }
}
