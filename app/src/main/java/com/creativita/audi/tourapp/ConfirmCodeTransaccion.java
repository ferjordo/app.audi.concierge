package com.creativita.audi.tourapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ConfirmCodeTransaccion extends DialogFragment {

    Context mcontext;
    Button btnTransaccion, cancel ;
    EditText editText;
    private static RequestQueue queue;

    String CODE="", DFTRANSACCION="";


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder build = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.fragment_confirm_code_transaccion, null);
        btnTransaccion = v.findViewById(R.id.enviarCode);
        cancel = v.findViewById(R.id.cancelCode);
        editText = v.findViewById(R.id.codetxt);
        mcontext = v.getContext();
        queue = Volley.newRequestQueue(mcontext);

        btnTransaccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               if(editText.getText().toString().equals("")){
                   Toast.makeText(getActivity(),"CAMPOS VACIOS", Toast.LENGTH_LONG).show();
               }else{

                   enviarCodigo(CODE, DFTRANSACCION );
               }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        build.setView(v);

        return build.create();

    }

    void enviarCodigo (final  String code, final  String transaccion){



        String url = Constantes.URL + "verify-transaction";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject oJson = new JSONObject(response);
                            procesarRecovery(oJson);
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("uid", "fsdfsdf" );
                parameters.put("transaction_id", editText.getText().toString() );
                parameters.put("type", "sdfasdfasdf" );
                parameters.put("value", editText.getText().toString() );


                return parameters;
            }
        });



    }
    private void procesarRecovery (JSONObject response) {
        try{
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "0":
                    Toast.makeText(getApplicationContext(),mensaje + "", Toast.LENGTH_LONG).show();
                    break;
                case "1":
                    Toast.makeText(getApplicationContext(),mensaje + "", Toast.LENGTH_LONG).show();
                    dismiss();
                    break;
                case "2":
                    dismiss();
                    Toast.makeText(getApplicationContext(),mensaje, Toast.LENGTH_LONG).show();
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }


}
