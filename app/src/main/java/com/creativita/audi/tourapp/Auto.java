package com.creativita.audi.tourapp;

/*
 * Objeto */

public class Auto {
    private String modelo, placa, imagen;
    private long km;

    public Auto (String modelo, String placa, long km ,String img){
        setModelo(modelo);
        setPlaca(placa);
        setKm(km);
        setImagen(img);
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setKm(long km) {
        this.km = km;
    }

    public void setImagen (String img) {
        this.imagen = img;
    }

    public String getModelo() {
        return modelo;
    }

    public String getPlaca() {
        return placa;
    }

    public long getKm() {
        return km;
    }

    public String getImagen () {
        return this.imagen;
    }
}
