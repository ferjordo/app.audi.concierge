package com.creativita.audi.tourapp;

import android.*;
import android.Manifest;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.firestore.FirebaseFirestore;


import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class estadoReparar  extends Fragment  {

    private static final String URL_BASE = Constantes.URL;
    private RequestQueue queue;
    ListView lstCitas;
    citasAdapter adapter;
    citasAdapterValet adapterValet;
    Context context;
    String estado;
    int estadocitaapp= 2;
    TextView cabecera;
    String dialogMessage, entregadoMessage, dialogMessageFecha;
    String siguiente;
    clienteMap map;
    private GoogleMap myMap;
    FragmentTransaction transaction;
    FragmentManager fragmentManager;
    FrameLayout frame;
    double Latitud, Longitud;
    citaClas citashow;
    boolean DeleteOfListe=true;
    String Telefono;
    int permissionCheck;
    private static final  int REQUEST_CODE_PHONE=0;

    int idCitaOnClick = 0; //Este es el entero q se envia al la actividad de mapa q va sevir como LookupID

    String telefonoCliente ="";

    /* Cuando SE AGREGE OTRO ESTADO A LAS CITAS */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            estado = this.getArguments().getString("estado");
            if (estado.equals("recibir")) {
                siguiente = "reparacion";
            } else if (estado.equals("reparacion")) {
                siguiente = "entregado";
            }else if (estado.equals("entregado")) {
                siguiente = "finalizado";
            }
            Log.d("menu",estado);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_estado_reparar, container, false);

    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        Log.d("menu", "REPARAR");

        lstCitas = (ListView) getActivity().findViewById(R.id.lstCitas);

        context = getActivity().getApplicationContext();
        fragmentManager = getFragmentManager();
        frame =(FrameLayout) getActivity().findViewById(R.id.contentFrame);

        queue = Volley.newRequestQueue(context);

        cabecera = (TextView) getActivity().findViewById(R.id.lblCitas);



        dialogMessage = "¿Seguro qué desea cambiar el estado de la cita a " + siguiente + "? o puede presionar llamar para contactar al cliente";
        entregadoMessage = "Está cita ya se finalizó";
        dialogMessageFecha= "La fecha de su cita no coincide con la fecha actual ¿DESEA CONTINUAR?";


/*
        if (estado.equals("recibir")) {
            //cabecera.setText(cabecera.getText() + " POR  RECIBIR" );
            Log.d("menu", " se puso cabecera" );

        } else if (estado.equals("reparacion")) {
            cabecera.setText(cabecera.getText() + " POR  REPARAR" );
        }else if (estado.equals("entregado")) {
            cabecera.setText(cabecera.getText() + " POR  ENTEGAR" );
        }else if (estado.equals("finalizado")) {
            cabecera.setText(cabecera.getText() + " FINALIZADAS" );
        }*/


        citasPorEstado frag = new citasPorEstado();
        Bundle args = new Bundle();
        args.putString("estado", estado);
        frag.setArguments(args);

        transaction = fragmentManager.beginTransaction();
        transaction.replace(getActivity().findViewById(R.id.contentFrame).getId(), frag);
        transaction.addToBackStack(null);
        //transaction.detach(frag).attach(frag).commit();

        transaction.commit();

    }





    @Override
    public void onResume() {
        super.onResume();
    }
}
