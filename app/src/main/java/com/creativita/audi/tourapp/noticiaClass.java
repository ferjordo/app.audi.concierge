package com.creativita.audi.tourapp;

public class noticiaClass {
    String titulo, campoText, img;

    public noticiaClass(String titulo, String campoText, String img) {
        this.titulo = titulo;
        this.campoText = campoText;
        this.img = img;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCampoText() {
        return campoText;
    }

    public void setCampoText(String campoText) {
        this.campoText = campoText;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
