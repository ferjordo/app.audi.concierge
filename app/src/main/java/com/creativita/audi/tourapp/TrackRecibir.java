package com.creativita.audi.tourapp;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class TrackRecibir extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    LocationManager locationManager;
    String locationProvider;

    String collectionId = "";
    String LookID = "";
    String actionID = "";

    String TAG = "Enviando..";
    String ExpextedID = "";
    String Telefono = "";
    int posicionado = 0;
    String CLIENTE_ID = "";
    Double Lat, Long;
    boolean isFABOpen = false;/// fab buttom ppara abri y cerrar
    private GoogleApiClient apiClient; /* Variale  de google client */

    private GoogleMap myMap;


    String IDCITA = "";
    int logout = 0, id;
    String dialogMessage, URLtoTRACK, Placa, dialogMessageTrack;
    boolean canBack = false;
    boolean backHome = false;

    public static TextView txtEstado;

    int permissionCheck;
    private static final int REQUEST_CODE_PHONE = 0;
    boolean click = false;

    FragmentTransaction transaction;
    FragmentManager fragmentManager;
    FrameLayout frame;
    ConsultasServidor consultasServidor;

    /*
     * MENU DESPLEGABLE
     * OCULTAR U DESOCULTAR TEXTO*/
    LinearLayout lay_1, lay_2, lay_3, lay_4, lay_5;
    TextView txt1, txt2, txt3, txt4, txt5;
    //private ProgressBar progressBar;

    Boolean Update = false;
    UptateTrack track;

    clienteMap map;
    String shareMessage;


    TextView mQuoteTextView;

    FloatingActionButton CambiarCita, finish, call, finishTrack, recibirLlaves, forzar_update;
    public static final String TAG_GPS = "Enviando..";

    public TrackRecibir() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static TrackRecibir newInstance(String param1, String param2) {
        TrackRecibir fragment = new TrackRecibir();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            IDCITA = getArguments().getString("IdCita");
            Placa = getArguments().getString("placa");
            Lat = Double.parseDouble(getArguments().getString("latitud"));
            Long = Double.parseDouble(getArguments().getString("longitud"));
            Telefono = getArguments().getString("telefonoCliente");

            LookID = actionID;
            collectionId = actionID;
            ExpextedID = actionID;

        }
        //mDocRef2  = FirebaseFirestore.getInstance().document("Location/"+IDCITA );
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_track_recibir, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

 /*   @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng ubi = new LatLng(Lat, Long);
        //setMapPosition();


        CameraPosition pos = CameraPosition.builder().target(ubi).zoom(13).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos));

        myMap = googleMap;
        myMap.addMarker(new MarkerOptions().position(ubi).title("PLACA: " + Placa).icon(BitmapDescriptorFactory.fromResource(R.drawable.track_auto)));
        /*if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        myMap.setMyLocationEnabled(true);*/
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        dialogMessage = "¿Seguro qué desea cambiar el estado de la cita a REPARAR ? ";
        dialogMessageTrack="¿ Desde este momento el cliente vera su ubicacacón, desea activar ?";
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS &&
                apiAvailability.isUserResolvableError(resultCode)) {
            Dialog dialog = apiAvailability.getErrorDialog(getActivity(), resultCode,
                    900);
            dialog.setCancelable(false);
            dialog.show();
        }
        fragmentManager = getFragmentManager();
        frame = (FrameLayout)getActivity().findViewById(R.id.contentMap);

        map = clienteMap.newInstance();
        transaction = fragmentManager.beginTransaction();
        transaction.replace(frame.getId(), map);
        transaction.addToBackStack(null);
        transaction.commit();
        map.getMapAsync(this);
        txtEstado = (TextView) getActivity().findViewById(R.id.estadoTrack);// muesstra si esta siguiendo o no cambia color

        lay_1 = (LinearLayout) getActivity().findViewById(R.id.L_cambiarEstado);
        lay_2 = (LinearLayout) getActivity().findViewById(R.id.L_callToOwer);
        lay_3 = (LinearLayout) getActivity().findViewById(R.id.L_finishTrack);
        lay_4 = (LinearLayout) getActivity().findViewById(R.id.L_Recibirllaves);
        lay_5 = (LinearLayout) getActivity().findViewById(R.id.L_forzar_update);



        //txt1, txt2, txt3
        txt1 = (TextView) getActivity().findViewById(R.id.txt_finish);
        txt2 = (TextView) getActivity().findViewById(R.id.txt_call);
        txt3 = (TextView) getActivity().findViewById(R.id.txt_next);
        txt4 = (TextView) getActivity().findViewById(R.id.txt_Recibirllaves);
        txt5 = (TextView) getActivity().findViewById(R.id.txt_forzar_update);





        call = getActivity().findViewById(R.id.callToOwer);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                permissionCheck = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE);
                if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    //Llamar();
                    Intent j = new Intent(Intent.ACTION_CALL);
                    j.setData(Uri.parse("tel:0" + Telefono));
                    startActivity(j);
                } else {
                    // Explicar permiso
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.CAMERA)) {
                        Toast.makeText(getActivity(), "El permiso es necesario para llamar.",
                                Toast.LENGTH_SHORT).show();
                    }

                    // Solicitar el permiso
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE}, REQUEST_CODE_PHONE);
                    }
                }
            }
        });
        finishTrack = getActivity().findViewById(R.id.finishTrack);
        finishTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            Home.canTrack=false;

                //(String latitud, String longitud, String speed , String Placa, String Nombre){
                //ExitFollowColor();
                Toast.makeText(getActivity(), " Detener track ", Toast.LENGTH_LONG).show();
            }
        });
        finish = getActivity().findViewById(R.id.finish);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFABOpen){
                    showFABMenu();
                }else{
                    closeFABMenu();
                }
            }
        });

        CambiarCita = getActivity().findViewById(R.id.cambiarEstado);
        CambiarCita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("Live", "Clic boton 0");
                createSimpleDialog(getActivity(), IDCITA).show();
                //track.setBand(false);
                //track.interrupt();
            }
        });

        recibirLlaves = getActivity().findViewById(R.id.Recibirllaves);
        recibirLlaves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("Live", "Recibio llaves");
                createLlaves(getActivity(), IDCITA).show();
                //track.setBand(false);
                //track.interrupt();
            }
        });
        forzar_update  = getActivity().findViewById(R.id.forzar_update);
        forzar_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("Live", "Recibio llaves");
                activar();
               // createLlaves(getActivity(), IDCITA).show();
                //track.setBand(false);
                //track.interrupt();
            }
        });

        activar();
        //showDialogAcitvarSeguimiento(getActivity()).show();
        sendNotificacion(IDCITA);
        //sendNotificacion("1");
    }
void activar(){
    Home.fragmentFollowtrack=1;

    //onFollowColor();
    Home.PLACA = Placa;
    Home.mDocRef = FirebaseFirestore.getInstance().document( "Location"+ "/" + IDCITA);
    Log.d("GPS", "ID CITA: "+IDCITA);

    Home.canTrack=true;
}

    public  void onFollowColor(){

        txtEstado.setBackgroundColor(getResources().getColor(R.color.GPSactive));
        txtEstado.setText("Modo seguimiento activo \n Evite cerrar la aplicación o se detendrá el modo seguimiento");
    }

    public void ExitFollowColor(){
        txtEstado.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        txtEstado.setText(" Modo seguimiento desactivado");
    }

    private void sendNotificacion( final String id) {

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        String url = Constantes.URL + "Notificacion-track.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      /*  try {
                            //JSONObject oJson = new JSONObject(response);
                           // procesarCambio(oJson, cita);
                        }catch (JSONException e) {
                            e.printStackTrace();

                        }*/
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("cita",""+id);
                return parameters;
            }
        });

    }


    private void showFABMenu(){
        isFABOpen=true;
        lay_1.animate().translationY(-getResources().getDimension(R.dimen.standard_65));
        lay_2.animate().translationY(-getResources().getDimension(R.dimen.standard_125));
        lay_3.animate().translationY(-getResources().getDimension(R.dimen.standard_185));
        lay_5.animate().translationY(-getResources().getDimension(R.dimen.standard_245));
        lay_4.animate().translationY(-getResources().getDimension(R.dimen.standard_305));




        txt1.setVisibility(View.VISIBLE);
        txt2.setVisibility(View.VISIBLE);
        txt3.setVisibility(View.VISIBLE);
        txt4.setVisibility(View.VISIBLE);
        txt5.setVisibility(View.VISIBLE);

    }

    private void closeFABMenu(){
        isFABOpen=false;
        lay_1.animate().translationY(0);
        lay_2.animate().translationY(0);
        lay_3.animate().translationY(0);
        lay_4.animate().translationY(0);
        lay_5.animate().translationY(0);


        txt1.setVisibility(View.GONE);
        txt2.setVisibility(View.GONE);
        txt3.setVisibility(View.GONE);
        txt4.setVisibility(View.GONE);
        txt5.setVisibility(View.GONE);


    }

    public AlertDialog createSimpleDialog(Context context, final String cita) {
        Log.d("Live","Clic boton ");

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Cambiar estado de la cita")
                .setMessage(dialogMessage)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d("Live","Clic boton ");
                                Home.canTrack=false;/// detener seguimiento al cambiar el estado de la cita

                                //ExitFollowColor();
                                cambiarEstado(cita);
                            }
                        });
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();
    }
    public AlertDialog createLlaves(Context context, final String cita) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("He recibido las llaves")
                .setMessage("Confirma haber recibido las llaves del auto asignado")
                .setPositiveButton("Confirmar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                recibirLlaves ( cita);

                            }
                        });
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();
    }

   /* public AlertDialog showDialogAcitvarSeguimiento (Context context){

        Log.d("Live","Clic boton ");

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Habilitar seguimiento")
                .setMessage(dialogMessageTrack)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d("Live","Clic boton ");
                                //
                                txtEstado.setBackgroundColor(getResources().getColor(R.color.GPSactive));
                                txtEstado.setText(" Modo seguimiento activo");
                                Home.PLACA = Placa;
                                Home.mDocRef = FirebaseFirestore.getInstance().document( "Location"+ "/" + IDCITA);
                                Home.canTrack=true;

                            }
                        })

                .setCancelable(false);
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();

    }*/


    public void salir(String IDCITA){
        Toast.makeText(getApplicationContext(), "Saliste",Toast.LENGTH_SHORT).show();
        // Teliver.stopTrip(IDCITA);
    }

    public  void Llamar() {
        Intent j = new Intent(Intent.ACTION_CALL);
        j.setData(Uri.parse("tel:0" + Telefono));
    }

    ///CAMBIAR ESTADO CITA
    public void cambiarEstado (final String cita) {
       RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
       String url = Constantes.URL + "cambiar-estado-cita.php";
       queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            procesarCambio(oJson, cita);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("cita",""+cita);
                parameters.put("estado","reparacion");
                Log.d("ESTADO ", "reparacion");
                return parameters;
            }
        });
    }

    private void procesarCambio (JSONObject response,  String idcita) {
        try {
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "2":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "3":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    //EXITO
                    // salir(actionID); // SALIR DE TILEVIRE LIBRERIA Q HACE LIVE TRAKING
                    Intent home = new Intent(getApplicationContext(),Home.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(home);
                    //EnviarNotificacion(idcita);
                    break;
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }
///FIN DE CARBIAR ESTADO CITA


    ///RECIBIR LLAVES CITA
    public void recibirLlaves (final String cita) {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = Constantes.URL + "recibir-llaves.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            lay_4.setVisibility(View.GONE);
                            //procesarCambio(oJson, cita);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("cita",""+cita);
                return parameters;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
