package com.creativita.audi.tourapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

public class misAutos extends Fragment {

    ListView lstMisAutos;
    AutosAdapter adapter;
    Context context;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mis_autos, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        lstMisAutos = (ListView)getActivity().findViewById(R.id.lstAutos);
        context = getActivity().getApplicationContext();
        fragmentManager = getFragmentManager();

        adapter = new AutosAdapter(context);

        lstMisAutos.setAdapter(adapter);
        lstMisAutos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Auto auto = adapter.getItem(i);
                citasPorAuto frag = new citasPorAuto();
                Bundle args = new Bundle();
                args.putString("modelo", auto.getModelo());
                args.putString("placa", auto.getPlaca());
                args.putString("imagen", auto.getImagen());
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(getActivity().findViewById(R.id.contentFrame).getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }
}

