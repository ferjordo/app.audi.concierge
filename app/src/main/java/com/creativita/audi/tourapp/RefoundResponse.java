package com.creativita.audi.tourapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class RefoundResponse {

    @SerializedName("transaction_id")
    @Expose
    private String transaction_id;

    public String getTransaction_id() {return transaction_id; }

    public void setTransaction_id(String transaction_id) {this.transaction_id = transaction_id;  }
}
