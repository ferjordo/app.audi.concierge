package com.creativita.audi.tourapp;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UpdateCitaAdmin#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UpdateCitaAdmin extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_ID ="id";
    private static final String ARG_LUGAR = "lugar";
    private static final String ARG_AUTO = "auto";
    private static final String ARG_FECHA = "fecha";
    private static final String ARG_ESTADO = "estado";

    private int id ;
    private String lugar;
    private String auto;
    private String fecha;
    private String estado;
    private String precio;

    public EditText newKM;
    public EditText newdescription;
    public EditText Precio;
    public Button actualizarcita;

    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    FrameLayout frame;

    private static final String URL = "https://audiguayaquil.com/SERVICE/";


    public UpdateCitaAdmin() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UpdateCitaAdmin.
     */
    // TODO: Rename and change types and number of parameters
    public static UpdateCitaAdmin newInstance(String param1, String param2) {
        UpdateCitaAdmin fragment = new UpdateCitaAdmin();
        Bundle args = new Bundle();
        args.putString(ARG_LUGAR, param1);
        args.putString(ARG_AUTO, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id =  getArguments().getInt(ARG_ID);
            lugar = getArguments().getString(ARG_LUGAR);
            auto = getArguments().getString(ARG_AUTO);
            fecha = getArguments().getString(ARG_FECHA);
            estado = getArguments().getString(ARG_ESTADO);
            Log.d("Direccion",""+lugar+" "+auto+" "+fecha+" "+estado);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_update_cita_admin, container, false);
    }



    @Override
    public void onActivityCreated(Bundle state) {

        super.onActivityCreated(state);
        newKM =(EditText)getActivity().findViewById(R.id.km2);
        newdescription = (EditText)getActivity().findViewById(R.id.descriptionCita);
        Precio = (EditText)getActivity().findViewById(R.id.precio);
        actualizarcita = (Button)getActivity().findViewById(R.id.btnsenddirection);

        fragmentManager = getFragmentManager();
        frame =(FrameLayout) getActivity().findViewById(R.id.contentFrame);

        actualizarcita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AsignarSegundaDireccon(id, newKM.getText().toString() , newdescription.getText().toString(), Precio.getText().toString() );
            }
        });
    }

    public void AsignarSegundaDireccon (final int cita,final String km, final  String descripcion, final  String precio1) {
        String url = URL + "actualizarcita-descripcion.php";
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            procesaactualización(oJson, cita);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id",""+cita);
                parameters.put("descripcion",""+descripcion);
                parameters.put("km",""+km);
                parameters.put("precio", ""+precio1);
                //Log.d("Direccion","ID: "+cita +"Referencia: "+referencia+" Longitud: "+ longitud+" Latitud:  "+ latitud);
                return parameters;
            }
        };
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    private void procesaactualización (JSONObject response,  int idcita) {
        try {
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    newKM.setText("");
                    newdescription.setText("");
                   // EnviarNotificacion (idcita);
                    citasPorEstado frag = new citasPorEstado();
                    Bundle args = new Bundle();
                    args.putString("estado", "reparacion");
                    frag.setArguments(args);
                    transaction =  fragmentManager.beginTransaction();
                    transaction.replace(getActivity().findViewById(R.id.contentFrame).getId(),frag);
                    transaction.addToBackStack(null);
                    transaction.commit();


                    // dismiss();
                    break;
                case "2":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    //dismiss();
                    break;
                case "3":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    //dismiss();

                    break;
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void EnviarNotificacion(final  int idcita) {

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = URL + "Notificacion-llegar-zentrum.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("NOTIFICACION ",""+response);
                        /* try {
                            JSONObject json = new JSONObject(response);
                            //procesarcaros(json);

                        } catch (JSONException e) {
                          //  Log.d("ERROR JSON", "....." + e.getMessage());
                            Toast.makeText(context,
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }*/
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERROR EN MODELOS", " " + error.getMessage());
                      /*  Toast.makeText(context,
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();*/
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                // parameters.put("tipo", "Cliente");
                parameters.put("cita", ""+idcita);
                parameters.put("notificar", "direccion");
                return parameters;
            }
        });

    }


}
