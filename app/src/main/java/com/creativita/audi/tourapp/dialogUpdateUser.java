package com.creativita.audi.tourapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class dialogUpdateUser extends DialogFragment {

    private static final String URL = Constantes.URL;
    EditText newName,newLastName,newMail, newCelular;
   Button newIDcard;
    int mYear, mMonth, mDay, mHour, mMinute;

    private static RequestQueue queue;
    Context mcontext;
    Button update,cancel;
    String newCorreo;
     static  String urlimg , user , pass, fechaNacimiento="";

    JSONObject response;
    public dialogUpdateUser(){

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder build = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_update_user, null);

        newName = (EditText)v.findViewById(R.id.txtNewName);
        newLastName = (EditText)v.findViewById(R.id.txtNewLastName);
        newMail = (EditText)v.findViewById(R.id.txtNewMail);
        update = (Button)v.findViewById(R.id.btnUpdate);
        cancel = (Button)v.findViewById(R.id.btnCancelar);
        newCelular = (EditText)v.findViewById(R.id.txtNewcel);
        newIDcard = (Button)v.findViewById(R.id.txtCardID);

        mcontext = v.getContext();
        queue = Volley.newRequestQueue(mcontext);

        setUserProfile(Home.jsondata);

        LoadPerfil();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(newName.getText().toString().equals("") || newLastName.getText().toString().equals("") || newMail.getText().toString().equals("")||newCelular.getText().toString().equals("")|| fechaNacimiento.equals("")){
                    //Toast.makeText(getApplicationContext(),"CAMPOS VACIOS", Toast.LENGTH_LONG).show();
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    builder.setMessage( "Revise que todos los campos esten completos ")
                            .setTitle("Datos incompletos");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
                    builder.show();
                }
                else{
                    newCorreo= ""+newMail.getText().toString();
                    Log.d("fecha", " "+ fechaNacimiento);
                    updateUser();
                }
            }
        });
        newIDcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Nacimiento();
            }
        });

        build.setView(v);

        return build.create();
    }
    public void Nacimiento(){


        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR );
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        final Date date1 = new Date(mYear - 18 , mMonth, mDay  );
        final  Date date2 = new Date(mYear - 80 , mMonth, mDay );


        Calendar min=Calendar.getInstance();
        min.set(1950, 1, 1, 0, 0);


        Calendar max=Calendar.getInstance();
        max.set(mYear-18, 11, 31, 0, 0);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        // Log.d("date","After: "+date1.after(date));
                        newIDcard.setText("Fecha:"+ dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
                        fechaNacimiento = ""+year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(max.getTimeInMillis());
        datePickerDialog.getDatePicker().setMinDate(min.getTimeInMillis());

        datePickerDialog.show();    }

    /*obtener correo, nombre nemail y id para el encabezado */
    public  void  setUserProfile(String jsondata){

        try {
            response = new JSONObject(jsondata);
            newMail.setText(response.get("email").toString());
            Log.d("dialog",response.get("email").toString());
            String str = response.get("name").toString();
            String delimiter = " ";
            String[] temp;
            temp = str.split(delimiter);

            newName.setText(temp[0]);
            newLastName.setText(temp[1]);
            newCelular.setText(Home.Celular);
            urlimg = response.get("picture").toString();

//            Log.d("dialog",response.get("name").toString());

        } catch (Exception e){
            e.printStackTrace();
        }
    }



    private void updateUser(){
        Log.d("update", " sI UPDATE");
        String url = URL + "editar-usuario.php";

        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject oJson = new JSONObject(response);
                            procesarUpdate(oJson);
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
         ){
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id", Home.IDUSER_QUERY);
                parameters.put("tipo", Home.TIPO_USER);
                parameters.put("name",newName.getText().toString());
                parameters.put("lastname",newLastName.getText().toString());
                parameters.put("mail",newMail.getText().toString());
                parameters.put("celular",newCelular.getText().toString());
                parameters.put("fecha_nacimiento",fechaNacimiento);
                return parameters;

            }
          });
    }
    void ActualizarUi(String Nombre, String mail, String Celular){
                Home.Nombre = Nombre;
                Home.Mail = mail;
                Home.Celular= Celular;
                userProfile.userMail.setText(mail);
                userProfile.userName.setText(Nombre);

        }
    private void procesarUpdate (JSONObject response) {
        try{
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "0":
                    Toast.makeText(getApplicationContext(),mensaje + " ", Toast.LENGTH_LONG).show();
                    break;
                case "1":
                    Toast.makeText(getApplicationContext(),mensaje + " intente más tarde", Toast.LENGTH_LONG).show();
                    break;
                case "2":
                    ActualizarUi( newName.getText().toString() + " "+newLastName.getText().toString(),
                                 newMail.getText().toString(), newCelular.getText().toString());

                    updateMailFirebase();
                    DoJson(newName.getText().toString() + " "+newLastName.getText().toString(),newMail.getText().toString());
                    dismiss();
                    Toast.makeText(getApplicationContext(),mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "3":
                    Toast.makeText(getApplicationContext(),mensaje , Toast.LENGTH_LONG).show();
                    break;

            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    //CORREGIR ESTO
    public void LoadPerfil() {
        SharedPreferences Preferens = getActivity().getSharedPreferences("Load_Profile", Context.MODE_PRIVATE);
        ///  IsLoged= Preferens.getBoolean("IsLog",false);
       // user = Preferens.getString("user", "");
         pass = Preferens.getString("pass", "");
       // Log.d("User", pass);
       // Log.d("User", Home.Mail);

        reautentificar(Home.Mail, pass);


    }
void reautentificar(String mail,  String pass){
      //  Log.d("pass", mail+" - "+pass);
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    AuthCredential credential = EmailAuthProvider
            .getCredential(mail, pass);

    user.reauthenticate(credential)
            .addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Log.d("", "User re-authenticated.");
                }
            });
}
   public void updateMailFirebase(){
       FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
       ////PARA DESPUES
       try {
           user.updateEmail(newCorreo).addOnCompleteListener(new OnCompleteListener<Void>() {
               @Override
               public void onComplete(@NonNull Task<Void> task) {
                   if (task.isSuccessful()) {
                       Log.d("Update", "Actualización con exíto");
                   }
               }
           });

       }catch (Error error){
           ShowDialog();
       }
       //////PARA DESPUES///

   }

    public  void ShowDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());

        builder.setMessage("Ha transcurrido mucho tiempo desde que inicio sesión es necesarió que cierre la sesión y vualva a ingresar ")
                .setTitle("Cambios no registrados, acciones necesarias");
        builder.setPositiveButton("Cerrar sesión", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //firebaseAuth = FirebaseAuth.getInstance();
                logout();

//firebaseAuth.getCurrentUser()
            }
        });




        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public   void logout() {
        LoginManager.getInstance().logOut();
        FirebaseAuth.getInstance().signOut();
        ///HyperTrack.stopTracking();
        //SavePerfil("", "", "", "", "");
        //goLoginScreen();
    }
    ///
    /// Es necesario guardar en preferencia
    /// Y mostrar directo en text view
    public  void DoJson(String name, String mail){

        Map<String, String> Jsonperfil = new HashMap<>();
        Jsonperfil.put("id",Home.IDUSER_QUERY);
        Jsonperfil.put("name", name );
        Jsonperfil.put("email", mail );
        Jsonperfil.put("picture",urlimg );

        JSONObject jsonObject = new JSONObject(Jsonperfil);
        SavePerfil(jsonObject.toString(),  "yes", Home.IDUSER_QUERY, Home.TIPO_USER, Home.Celular);
        Home.jsondata= jsonObject.toString();

    }

    public void SavePerfil(String data, String loged, String IDUSER_QUERY, String TIPO_USER, String Celular) {
try {
    SharedPreferences Preferens = getActivity().getSharedPreferences("Load_Profile", Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = Preferens.edit();
    editor.putString("JSON", data);
    editor.putString("login", loged);
    editor.putString("IDUSER_QUERY", IDUSER_QUERY);
    editor.putString("TIPO_USER", TIPO_USER);
    editor.putString("Celular",Celular);
    editor.commit();
}catch (Exception error){
    Log.d("perfil", ""+error.getMessage() );
}

    }

}
