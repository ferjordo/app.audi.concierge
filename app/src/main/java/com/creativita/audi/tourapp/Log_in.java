package com.creativita.audi.tourapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Log_in extends AppCompatActivity {
    CallbackManager callbackManager;
    JSONObject jsonDatauser;
    JSONObject response, profile_pic_data, profile_pic_url;
    final String URL = Constantes.URL;
    EditText usernameLog ,passLog;
    String user , pass , BANDCONFIR="yes" , Mail , Name , Telefono, UID;
    String id_User,img_User , Tipo,id="";
    TextView Recovery;
    Integer Estado;
    boolean fb =false;
    LoginResult loginResultGlobal = null;
    Boolean classic =false;
    LinearLayout content1, content2;

    TextView TyC ;
    CheckBox checkterm ;
     ProgressDialog pd;




    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        content1 = (LinearLayout) findViewById(R.id.linear1login);
        content2 = (LinearLayout) findViewById(R.id.linear2login);

        TyC = (TextView)findViewById(R.id.termycond1);
        TyC.setMovementMethod(LinkMovementMethod.getInstance());

        checkterm = (CheckBox)findViewById(R.id.checkterm1);

        callbackManager = CallbackManager.Factory.create();
        Recovery = (TextView) findViewById(R.id.txtrecovery);
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                fb=true;
                loginResultGlobal =loginResult;
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), " Cancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(),
                        ""+error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

         pd = new ProgressDialog(Log_in.this);
        pd.setMessage("Enviando Datos...");


        usernameLog =(EditText)findViewById(R.id.usernameLogX);
        passLog = (EditText) findViewById(R.id.passLogX);
        Recovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               RecoveryPass();
            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    if(!classic) {
                        getUserInfo(loginResultGlobal);
                    }
                }
            }
        };

    }



    private void handleFacebookAccessToken(AccessToken accessToken) {
        progressBar.setVisibility(View.VISIBLE);
        content1.setVisibility(View.INVISIBLE);
        content2.setVisibility(View.INVISIBLE);

        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Error Login FireBase", Toast.LENGTH_LONG).show();
                }
                pd.dismiss();
                progressBar.setVisibility(View.GONE);
                content1.setVisibility(View.VISIBLE);
                content2.setVisibility(View.VISIBLE);
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(firebaseAuthListener);
    }

    private void RecoveryPass(){
        RecoveryPassword dialog = new RecoveryPassword();
        dialog.show(getFragmentManager(),"update");
    }

    protected void getLoginDetails(LoginButton login_button){
        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult login_result) {
                fb=true;
                getUserInfo(login_result);
            }

            @Override
            public void onCancel() {
                // code for cancellation
                Log.d("Data login","Cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                //  code to handle error
                Toast.makeText(getApplicationContext(),
                        ""+exception.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void gotoSin_ing(View view){
        Intent intent = new Intent(Log_in.this,Sin_ing.class);
        startActivity(intent);
    }


    protected void getUserInfo(LoginResult login_result){

        GraphRequest data_request = GraphRequest.newMeRequest(
                login_result.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject json_object,
                            GraphResponse response) {
                        try {
                            Log.d("Data login","Este es el ID......"+response.toString());
                            id= ""+response.getJSONObject().getString("id");
                            profile_pic_data = new JSONObject(json_object.get("picture").toString());
                            profile_pic_url = new JSONObject(profile_pic_data.getString("data"));
                            img_User = profile_pic_url.getString("url");

                          verificarperfil(id);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("ERROR","ERROR......"+e.getMessage());
                        }


                    }
                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields","id,name,email,picture.width(220).height(220)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();

    }
/*
* cabiar de pantalla despues de loeegar*/
    void gotoMain(JSONObject json_object , String confirm , String id_User, String telefono, String uid){
        pd.dismiss();
        Intent intent = new Intent(Log_in.this,Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("loged",1);//bandera para saber si guardar preferencias o no
        intent.putExtra("jsondata",json_object.toString()); /// pbjeto que lleva toda l informacion de usuario
        intent.putExtra("classic","yes");
        intent.putExtra("id",id_User);
        intent.putExtra("tipo",Tipo);

        intent.putExtra("usuario",user);
        intent.putExtra("pass",pass);
        //Log.d("pass", ""+pass);
        intent.putExtra ("telefono", telefono);
        intent.putExtra("UID", uid);
        Log.d("UID", uid);

        startActivity(intent);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    /*
    * Consultar al servidor para el logeo
    * */
    public void verificarperfil(final String id){
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url =URL+"log-in.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            Log.d("Data login","Este es el ID dentro de volley......"+id);
                            JSONObject oJson = new JSONObject(response);
                            procesarRespuesta(oJson);
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("user_FB", id );
                return parameters;
            }
        });
        BANDCONFIR ="no";
    }
    /*funcion para decodificar la respuesta del servicio web*/
    private void    procesarRespuesta(final JSONObject response){
        Log.d("Term", "2 funcion procesar respuesta");

        try{

            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":
                    /*Registro del usuario registrado*/
                    Log.d("Data login","MENSAJE......"+ mensaje);

                    Name = response.getString("nombre");
                    Mail = response.getString("correo");
                    id_User = response.getString("id");
                    Tipo = response.getString("tipo");
                    Telefono =  response.getString("telefono");
                    UID  =  response.getString("uid");

                    if(!fb){
                        Log.d("Term", "3 YA inicio sesion en servidor, Detecta que no inicio sesion con Facebook");

                        firebaseAuth.signInWithEmailAndPassword(Mail, pass)
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        //Log.d("FireBase", "signInWithEmail:success");
                                        //FirebaseUser user = firebaseAuth.getCurrentUser();
                                        Log.d("Term", "4 Inicio sesion en Firebaase");

                                        if(Tipo.equals("2")){
                                            try {
                                                Estado= response.getInt("estado_cuenta");
                                                Log.d("Term", "5 Es tipo 2 y vefica el estado de la cuenta");

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                Log.e("FireBase", ""+e.getMessage());
                                            }
                                        }
                                        try {
                                            if(response.getString("imagen").equals("")){
                                                Log.d("Veficar pic","");
                                                //img_User = response.getString("imagen");
                                            }
                                            else{
                                                ///url de la imagen
                                                img_User = Constantes.URL + response.getString("imagen");
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            Log.e("FireBase", ""+e.getMessage());
                                        }
                                        if(fb){
                                            DoJson();
                                            gotoMain(jsonDatauser ,BANDCONFIR,id_User, Telefono, UID);
                                        }else{
                                            if(!Tipo.equals("2")){
                                                DoJson();
                                                gotoMain(jsonDatauser ,BANDCONFIR,id_User, Telefono, UID);
                                            }
                                            else {
                                                Log.d("Term", "6 Se asegura que es usuario tipo cliente");
                                                if(Estado==1){
                                                    DoJson();
                                                    gotoMain(jsonDatauser ,BANDCONFIR,id_User, Telefono, UID);
                                                }else{
                                                    Log.d("Term", "7 Cuenta no activada");
                                                    Toast.makeText(getApplicationContext(),
                                                            "Active su cuenta, revise nuestro correo de confirmación", Toast.LENGTH_LONG).show();


                                                    //progressBar.setVisibility(View.GONE);
                                                    //content1.setVisibility(View.VISIBLE);
                                                    //content2.setVisibility(View.VISIBLE);
                                                    usernameLog.setText("");
                                                    passLog.setText("");
                                                    LoginManager.getInstance().logOut();
                                                    FirebaseAuth.getInstance().signOut();
                                                    pd.dismiss();
                                                }}}

                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w("FireBase", "signInWithEmail:failure", task.getException());
                                /*Toast.makeText(EmailPasswordActivity.this, "Authentication failed.",Toast.LENGTH_SHORT).show();
                                updateUI(null);*/
                                    }

                                    // ...
                                }
                            });}else{

                        if(Tipo.equals("2")){
                            try {
                                Estado= response.getInt("estado_cuenta");
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e("FireBase", ""+e.getMessage());
                            }
                        }
                        try {
                            if(response.getString("imagen").equals("")){
                                Log.d("Veficar pic","");
                                //img_User = response.getString("imagen");
                            }
                            else{
                                img_User = Constantes.URL + response.getString("imagen");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("FireBase", ""+e.getMessage());
                        }
                        if(fb){
                            DoJson();
                            gotoMain(jsonDatauser ,BANDCONFIR,id_User, Telefono, UID);
                        }else{
                            if(!Tipo.equals("2")){
                                DoJson();
                                gotoMain(jsonDatauser ,BANDCONFIR,id_User, Telefono, UID);
                            }
                            else {
                                if(Estado==1){
                                    DoJson();
                                    gotoMain(jsonDatauser ,BANDCONFIR,id_User, Telefono, UID);
                                }else{
                                    Log.d("Term", "Active su cuenta");
                                    Toast.makeText(getApplicationContext(),
                                            "Active su cuenta, revise nuestro correo de confirmación", Toast.LENGTH_LONG).show();
                                   /* final AlertDialog.Builder builder = new AlertDialog.Builder(this);

                                    builder.setMessage("Active su cuenta, revise nuestro correo de confirmación ")
                                            .setTitle("Activar cuenta");
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        }
                                    });

                                    AlertDialog dialog = builder.create();
                                    */

                                    usernameLog.setText("");
                                    passLog.setText("");
                                    LoginManager.getInstance().logOut();
                                    FirebaseAuth.getInstance().signOut();
                                    pd.dismiss();

                                }}}
                    }

                    break;
                case "2":
                    /*reacion del usuario fallida */
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    usernameLog.setText("");
                    passLog.setText("");

                    LoginManager.getInstance().logOut();
                    FirebaseAuth.getInstance().signOut();
                    pd.dismiss();
                    break;
                case "0":
                    /*Usuario ya existe */
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    usernameLog.setText("");
                    passLog.setText("");
                   // progressBar.setVisibility(View.GONE);
                    //content1.setVisibility(View.VISIBLE);
                    //content2.setVisibility(View.VISIBLE);
                    LoginManager.getInstance().logOut();
                    FirebaseAuth.getInstance().signOut();
                    pd.dismiss();
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public  void LoginCassic(View view ){
        pd.show();
        user = usernameLog.getText().toString().replace(" ","");
        pass = passLog.getText().toString().replace(" ","");
        id = "";
        classic=true;
        Log.d("Term", "1 funcion al dar click");

 /*
request.setRetryPolicy(
        new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    getRequestQueue().add(request);
* */
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url=URL+"log-in.php";
        if(checkterm.isChecked()){
            //progressBar.setVisibility(View.VISIBLE);
            //content1.setVisibility(View.INVISIBLE);
            //content2.setVisibility(View.INVISIBLE);

            if (!(user.equals("")||pass.equals(""))){/////campos em blanco
                StringRequest request = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try{
                                    JSONObject oJson = new JSONObject(response);
                                    procesarRespuesta(oJson);
                                }catch (JSONException e){
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(),
                                            "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                /*EN CASO DE ERROR */
                                Toast.makeText(getApplicationContext(),
                                        "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                            }
                        }
                ) {
                    /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> parameters = new HashMap<>();
                        parameters.put("user_FB", user );
                        parameters.put("user_pass", pass );
                        return parameters;
                    }
                };
                request.setRetryPolicy(
                        new DefaultRetryPolicy(
                                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                                0,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(request);
            }else{
                Log.d("Term", "CAmpos vacios");
                Toast.makeText(getApplicationContext(),
                        "Complete los campos vacios", Toast.LENGTH_LONG).show();
               /* final AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setMessage("Complete los campos vacios")
                        .setTitle("Campos vacios");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });*/
                progressBar.setVisibility(View.GONE);
                content1.setVisibility(View.VISIBLE);
                content2.setVisibility(View.VISIBLE);
            }
        }else{
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage("Acepte los términos y condiciones para poder utlizar el servicio")
                    .setTitle("Términos y condiciones");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();

        }

    }

    public  void DoJson(){
        Map<String, String> Jsonperfil = new HashMap<>();
        Jsonperfil.put("id",id);
         Jsonperfil.put("name", Name );
         Jsonperfil.put("email", Mail );
        Jsonperfil.put("picture", img_User);
        JSONObject jsonObject = new JSONObject(Jsonperfil);
        jsonDatauser= jsonObject;
    }
}



