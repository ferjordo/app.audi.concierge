package com.creativita.audi.tourapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;



public class DialogInfoCita extends DialogFragment {

    Button dimis;
    public DialogInfoCita(){}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AlertDialog.Builder build = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.dialoginfocita, null);
         dimis = (Button)v.findViewById(R.id.dimiss);
         dimis.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 dismiss();
             }
         });

        build.setView(v);

        return build.create();


    }


}
