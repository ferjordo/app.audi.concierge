package com.creativita.audi.tourapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ObjectInput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdapterSeminuevos extends ArrayAdapter<SeminuevoClass> {

    private static final String URL_BASE = Constantes.URL+"seminuevos.php";
    //private static final String URL_Imagen = "https://audiguayaquil.com/SERVICE/";
    private List<SeminuevoClass> items;
    private RequestQueue queue;
    Context context;
    public  String TAG="seminuevo" ;
    public AdapterSeminuevos(final Context context) {
        super(context, 0);
        this.context = context;
        Log.d(TAG , "Inicio Adapter");
        queue = Volley.newRequestQueue(this.context);
        queue.add(new StringRequest(Request.Method.POST, URL_BASE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      //  Log.d(TAG , ""+response);
                        try {
                            JSONArray oJson = new JSONArray(response);
                          //  Log.d(TAG , ""+oJson.toString(2));
                            items = parseJson(oJson);
                            addAll( items);
                        } catch (JSONException e) {
                            Toast.makeText(context,
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("cliente", "");
                return parameters;
            }
        });
    }

    private List<SeminuevoClass> parseJson(JSONArray jsonObjectArray) {
        List<SeminuevoClass> Modelos = new ArrayList<>();
        JSONArray jsonArray;
        Log.d(TAG , "Desconpone lista de Seminuevos");
        // jsonArray = jsonObject.getJSONArray("imagenes");
        for (int i = 0; i < jsonObjectArray.length(); i++) {
            try {

                JSONObject objeto = jsonObjectArray.getJSONObject(i);
                int id = objeto.getInt("id");
                JSONArray imagenes = objeto.getJSONArray("imagenes");
                String modelo = objeto.getString("modelo");
                Double motor = objeto.getDouble("motor");
                int km = objeto.getInt("km");
                Double precio = objeto.getDouble("precio");
                int ac = objeto.getInt("ac");
                String traccion = objeto.getString("traccion");
                String equipamento = objeto.getString("equipamento");
                String informacion_adicional = objeto.getString("informacion_adicional");
                int anio = objeto.getInt("anio");


                Log.d(TAG, ""+objeto.toString());
                List<String> imgs = new ArrayList<>();;
                for (int j = 0; j < imagenes.length(); j++) {
                    try {

                        JSONObject imagen = imagenes.getJSONObject(j);
                        // Log.d("JSON ENCODE>>>>>>>", ""+imagen.getString("url"));
                        Log.d("JSON ENCODE>>>>>>>", ""+imagen.getString("url"));
                        imgs.add(imagen.getString("url"));
                    }catch (JSONException e){
                        Log.e("JSON ENCODE>>>>>>>", "Error de parsing: " + e.getMessage());
                    }
                }
                //public SeminuevoClass(String modelo, String motor, String traccion, String equipamento, String informacion_adicional,
                // int id, int km, int ac, int anio, double precio, List<String> imgs) {
                SeminuevoClass modelos = new SeminuevoClass(modelo,""+motor, traccion, equipamento, informacion_adicional,id, km, ac,anio, precio, imgs);

                Modelos.add(modelos);
            }catch ( JSONException e ){
                Log.e("JSON ENCODE>>>>>>>", "Error de parsing: " + e.getMessage());
            }
        }


        return Modelos;
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public SeminuevoClass getItem(int position) {
        return items.get(position);
    }

    @NonNull
    @Override
    public View getView(int i, View view, @NonNull ViewGroup viewGroup) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());

        //Salvando la referencia del View de la fila
        View listItemView;

        final AdapterSeminuevos.ViewHolder holder;

        //Comprobando si el View no existe
        //Si no existe, entonces inflarlo
        listItemView = null == view ? layoutInflater.inflate(
                R.layout.listas_seminuevo,
                viewGroup,
                false) : view;

        SeminuevoClass item = this.getItem(i);

        holder = new AdapterSeminuevos.ViewHolder();
        holder.textTitulo = (TextView) listItemView.findViewById(R.id.semi_titulo_lista);
        holder.imagenPrincipal = (ImageView) listItemView.findViewById(R.id.semunuevo_list);
        Picasso.with(context).load( item.getImgs().get(0).toString())
                .into(holder.imagenPrincipal);
        holder.textTitulo.setText("Audi " + item.getModelo());
        Log.d(TAG,""+item.getImgs().get(0).toString());

        // Procesar item
        return listItemView;
    }

    private static class ViewHolder {
        ImageView imagenPrincipal;
        TextView textTitulo;
    }

}

