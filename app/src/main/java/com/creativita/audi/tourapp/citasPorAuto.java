package com.creativita.audi.tourapp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class citasPorAuto extends Fragment {

    private static final String URL_BASE = Constantes.URL;
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final  int REQUEST_CODE_CAMERA=101;
    private static final  int PHOTO_SELECTED=102;

    ListView lstCitasAuto;
    citasAdapter adapter;
    Context context;
    String placa, imagenurl, modelo;
    ImageView imgAuto;
    TextView lblPlaca, lblcpaPlacaAuto2;
    int permissionCheck;
    PackageManager pm;
    String mCurrentPhotoPath, imageFileName;
    RequestQueue queue;
    FragmentTransaction transaction;
    FragmentManager fragmentManager;
    FrameLayout frame;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            modelo = this.getArguments().getString("modelos");
            placa = this.getArguments().getString("placa");
            imagenurl = this.getArguments().getString("imagen");
        }
     }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_citas_por_auto, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        fragmentManager = getFragmentManager();
        frame =(FrameLayout) getActivity().findViewById(R.id.contentFrame);
        context = getActivity().getApplicationContext();


        pm = getActivity().getPackageManager();
        queue = Volley.newRequestQueue(context);

        lblPlaca = (TextView)getActivity().findViewById(R.id.lblcpaPlacaAuto);
        lblcpaPlacaAuto2 = (TextView)getActivity().findViewById(R.id.lblcpaPlacaAuto2);
        imgAuto = (ImageView)getActivity().findViewById(R.id.imgcpaAutoImage);
        lstCitasAuto = (ListView)getActivity().findViewById(R.id.lstCitasAuto);

        adapter = new citasAdapter(context,Home.IDUSER_QUERY,placa);


        lblPlaca.setText(lblPlaca.getText().toString() + placa);
        lblPlaca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateCar();
            }
        });

        lblcpaPlacaAuto2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateCar();
            }
        });

        Picasso.with(context).load(URL_BASE + imagenurl)
                .into(imgAuto);

        lstCitasAuto.setAdapter(adapter);
        lstCitasAuto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                citaClas cita = adapter.getItem(i);
                SingleCita frag = new SingleCita();
                Bundle args = new Bundle();
                args.putInt("id", cita.getCitaid());
                args.putString("lugar", cita.getLugarEntrega());
                args.putString("auto", cita.getAuto());
                args.putString("fecha", cita.getFecha());
                args.putString("estado", cita.getEstado());
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });

        imgAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setTitle("Cambiar  foto del auto");
                builder.setIcon(getResources().getDrawable(R.drawable.icono));
                builder.setMessage("Seleccione una opción para elegir la imagen");
                builder.setPositiveButton("Cámara", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        permissionCheck = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.CAMERA);
                        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                            captureImage();
                            //selectImage();
                        }
                        else {
                            // Explicar permiso
                            if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                                Toast.makeText(getActivity().getApplicationContext(), "El permiso es necesario para utilizar la cámara.",
                                        Toast.LENGTH_SHORT).show();
                            }

                            // Solicitar el permiso
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CALL_PHONE}, REQUEST_CODE_CAMERA);
                            }
                        }
                    }
                });
                builder.setNegativeButton("Galería", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //dialogInterface.dismiss();

                        permissionCheck = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
                        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                            // captureImage();
                            selectImage();
                        }
                        else {
                            // Explicar permiso
                            if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                                Toast.makeText(getActivity().getApplicationContext(), "El permiso es necesario para utilizar la cámara.",
                                        Toast.LENGTH_SHORT).show();
                            }

                            // Solicitar el permiso
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CALL_PHONE}, PHOTO_SELECTED);
                            }
                        }

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });



    }
    private void selectImage () {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PHOTO_SELECTED);
    }


    private void updateCar() {
        dialogUpdateCar dialog = new dialogUpdateCar();


        Bundle args = new Bundle();
        args.putString("placa", ""+placa);
        dialog.setArguments(args);
        dialog.show(getFragmentManager(),"update");

    }

    private void captureImage () {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(pm) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            if (photoFile != null) {

                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            }
            //startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory() + "/picupload/autos";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();

        File image = new File(storageDir + "/" + imageFileName + ".jpg");

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            int scaleFactor = Math.min(1, 1);
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor << 1;
            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            Matrix mtx = new Matrix();
            mtx.postRotate(90);
            Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);
            updateImage(rotatedBMP);
        }else if(requestCode == PHOTO_SELECTED && resultCode == Activity.RESULT_OK){
            Uri selectedImage = data.getData();
            try {
                Bitmap bitmap1 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                Log.d("img", "  "+selectedImage.getPath().toString());

               /*BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImage.getPath().toString(), bmOptions);
                int photoW = bmOptions.outWidth;
                int photoH = bmOptions.outHeight;
                int scaleFactor = Math.min(photoW/photoW, photoH/photoH);
                bmOptions.inJustDecodeBounds = false;
                bmOptions.inSampleSize = scaleFactor << 1;
                Bitmap bitmap = BitmapFactory.decodeFile(selectedImage.getPath().toString(), bmOptions);
                Matrix mtx = new Matrix();
                mtx.postRotate(90);
                Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);*/
                updateImage(bitmap1);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void updateImage (final Bitmap img) {
        final Drawable image = new BitmapDrawable(getResources(),img);
        String url = URL_BASE + "cambiar-imagen-auto.php";
        queue.add(new MultipartRequest(Request.Method.POST, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            verificarImagen(oJson);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context,
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(context,
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("cliente", Home.IDUSER_QUERY);
                parameters.put("placa", placa);
                return parameters;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("txt_image", new DataPart(imageFileName+".jpeg", AppHelper.getFileDataFromDrawable(context, image), "image/jpeg"));
                return params;
            }
        });
    }

    private void verificarImagen (JSONObject response) {
        try {
            String mensaje = response.getString("mensaje");
            Toast.makeText(context,
                    mensaje, Toast.LENGTH_LONG).show();

        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context,
                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    // Permiso aceptado
                    captureImage();
                else
                    // Permiso denegado
                    Toast.makeText(context, "No se ha aceptado el permiso", Toast.LENGTH_SHORT).show();
                return;
            // Gestionar el resto de permisos
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
