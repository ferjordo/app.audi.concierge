package com.creativita.audi.tourapp;

/**
 * Created by PC_TWO on 28/2/2018.
 */

public class Constantes {

    //public static final String URL_PMTZ = "https://concierge-02.herokuapp.com/";
    public static final String URL_PMTZ = "http://audiguayaquil.com/SERVICE/paymentez/api/";

    //public static final String URL_PMTZ = "http://192.168.100.207:5000/";

    //audiguayaquil.com/SERVICE/paymentez/api/metodos
    static  String URL = "https://audiguayaquil.com/SERVICE/";
    static  String URL_TO_IMG = "https://audiguayaquil.com/SERVICE/";

    static String LOGGED_IN_CUSTOMER = "logged_in_customer";

    static String IS_LOGGED_IN = "is_logged_in";

    static String TRACKING_ID = "tracking_id";

    static int PERMISSION_REQ_CODE = 1;

    /*
    TPP2-EC-CLIENT
sDAwQAdBqetYhVZFFLpOg6FU2cmjF0

TPP2-EC-SERVER
NIJ6kjmMK482BWAngpa9QpKVtj4cUe

AUDIECUADOR-EC-CLIENT
ca4DeXXsgzqKx7RMleLsbHJQMxukIM

AUDIECUADOR-EC-SERVER
koqhyn2S5LYXO0Vn0yGeezF5LDDZ02
    * */

    static boolean PAYMENTEZ_IS_TEST_MODE = false;
    //static String PAYMENTEZ_CLIENT_APP_CODE = "TPP2-EC-CLIENT";
    //static String PAYMENTEZ_CLIENT_APP_KEY = "sDAwQAdBqetYhVZFFLpOg6FU2cmjF0";
    //static String PAYMENTEZ_CLIENT_APP_CODE = "AUDI-EC-CLIENT";
    //static String PAYMENTEZ_CLIENT_APP_KEY = "HBE3gAdkVEOcsMjr6eyLMzSdieHS3y";

    static String PAYMENTEZ_CLIENT_APP_CODE = "AUDIECUADOR-EC-CLIENT";
    static String PAYMENTEZ_CLIENT_APP_KEY = "ca4DeXXsgzqKx7RMleLsbHJQMxukIM";


    static  int SELECT_CARD_REQUEST = 1004;

}
