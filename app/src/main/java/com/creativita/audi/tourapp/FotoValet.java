package com.creativita.audi.tourapp;

import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;


public class FotoValet extends AppCompatActivity {
String Urli;
ImageView img;
    private static final String URL1 = Constantes.URL;

    Button back;
    TextView Tnombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto_valet);

        back = (Button)findViewById(R.id.backHome1);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FotoValet.this, Home.class);
                startActivity(i);
            }
        });
        Intent intent = getIntent();
        Urli =  intent.getStringExtra("Urli");
        String nombre = intent.getStringExtra("nombre");
        String apellido = intent.getStringExtra("apellido");
        Tnombre = (TextView)findViewById(R.id.nombrevalet1);
        Tnombre.setText(nombre+ " "+ apellido);
        img = (ImageView)findViewById(R.id.imgValet);

        Picasso.with(getApplicationContext())
                .load(Urli)
                .resize(750, 1100)
                .centerCrop()
                .into(img);




    }

}