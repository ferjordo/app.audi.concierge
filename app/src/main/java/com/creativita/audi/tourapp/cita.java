package com.creativita.audi.tourapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import static com.facebook.FacebookSdk.getApplicationContext;

/*
* request.setRetryPolicy(
        new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    getRequestQueue().add(request);
*
* */

public class cita extends Fragment  implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener {

    static final String URL = Constantes.URL;
    String metodo;
    Spinner autosPlaca;
    DatePicker datePicker ;
    TimePicker timePicker;
    ToggleButton toggleButton;
    int hora_Cita=0;
    LinearLayout contn;

    boolean bandPlazaLagos = false;

    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    FrameLayout frame;

    static EditText zona;
    static Button btncita, btn1, btn2 , btn3, btn4 , btn5, btn6;
    RadioGroup radioGroup;
    TextView TV ;
    static TextView  txthora, Calle1 ;
    static Button txtfecha;
    String  LONGITUD="" , LATITUD="";
    LinearLayout conttentubicación;
    ToggleButton ubicaciontoggleButton ;

    TextView TyC ;
    CheckBox checkterm ;

    private ProgressBar progressBar;

    ProgressDialog pd;

    RadioButton otraUbicacion;
    RadioButton plazalagos;




    private static final int PETICION_PERMISO_LOCALIZACION = 101;
    LocationRequest locationRequest;
    FusedLocationProviderApi fusedLocationProviderApi;
    private GoogleApiClient apiClient;

    RadioButton tdbtn;
    static String año, dia, mes, hora ,minutos;

    List<String> autos;
    Context context;
    static   Boolean isweakend=false, ISCHECKED=false;
    EditText num_contac;
    String celular;

    final Calendar c = Calendar.getInstance();
    // GoogleApiClient mGoogleApiClient;
    public cita() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        fragmentManager = getFragmentManager();
        frame = (FrameLayout) getActivity().findViewById(R.id.contentFrame);
        Home.fragmentToPicker=1;
        //TV = (TextView)getActivity().findViewById(R.id.tv) ;

        Calle1 = (TextView) getActivity().findViewById(R.id.calle1);
        //Calle2 = (EditText) getActivity().findViewById(R.id.calle2);
        zona = (EditText) getActivity().findViewById(R.id.zona);

        context = getActivity().getApplicationContext();
        getautos();
        autosPlaca = (Spinner) getActivity().findViewById(R.id.spnautos);

         txtfecha = (Button) getActivity().findViewById(R.id.textFecha);

        progressBar = (ProgressBar) getActivity().findViewById(R.id.citaprogress);
        //progressBar.setVisibility(View.INVISIBLE);

        contn =(LinearLayout)getActivity().findViewById(R.id.contentAllCita);
        conttentubicación= (LinearLayout)getActivity().findViewById(R.id.conttentubicación);


        TyC = (TextView)getActivity().findViewById(R.id.termycondC);
        TyC.setMovementMethod(LinkMovementMethod.getInstance());

        checkterm = (CheckBox)getActivity().findViewById(R.id.checktermC);

        num_contac = (EditText)getActivity().findViewById(R.id.num_contacto);
        pd = new ProgressDialog(getActivity());


        radioGroup = (RadioGroup)getActivity().findViewById(R.id.radigroup);
        radioGroup.check(R.id.btnotro);
        btn1= (Button) getActivity().findViewById(R.id.btn1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn1.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));
                Log.d("btn","btn 2 "+btn2.isEnabled());
                Log.d("btn","btn 1 "+btn1.isEnabled());
                Log.d("btn","btn 3 "+btn3.isEnabled());
                Log.d("btn","Clic btn 1 ");

                if(btn2.isEnabled()){
                    btn2.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                }
                if (btn3.isEnabled()){
                    btn3.setBackground(getResources().getDrawable(R.drawable.borderbottom));

                }
                if (btn4.isEnabled()){
                    btn4.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                }



                hora_Cita=1;
            }
        });

        btn2= (Button) getActivity().findViewById(R.id.btn2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn2.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));
                if(btn1.isEnabled()){
                    btn1.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                }
                if (btn3.isEnabled()){
                    btn3.setBackground(getResources().getDrawable(R.drawable.borderbottom));

                }
                if (btn4.isEnabled()){
                    btn4.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                }


                hora_Cita=2;
            }
        });

        btn3= (Button) getActivity().findViewById(R.id.btn3);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btn3.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));

                if(btn1.isEnabled()){
                    btn1.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                }
                if (btn2.isEnabled()){
                    btn2.setBackground(getResources().getDrawable(R.drawable.borderbottom));

                }
                if (btn4.isEnabled()){
                    btn4.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                }

                hora_Cita=3;
            }
        });

        btn4= (Button) getActivity().findViewById(R.id.btn4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn4.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));

                if(btn1.isEnabled()){
                    btn1.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                }
                if (btn2.isEnabled()){
                    btn2.setBackground(getResources().getDrawable(R.drawable.borderbottom));

                }
                if (btn3.isEnabled()){
                    btn3.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                }


                hora_Cita=4;
            }
        });

        otraUbicacion =(RadioButton)getActivity().findViewById(R.id.otraUbicacion);
        otraUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conttentubicación.setVisibility(View.VISIBLE);
                btn1.setVisibility(View.VISIBLE);// Muestra el boton de 9:30
                bandPlazaLagos= false;
            }
        });

        plazalagos =(RadioButton)getActivity().findViewById(R.id.plazaLagos);
        plazalagos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bandPlazaLagos= true;
                btn1.setVisibility(View.GONE); // Oculta el boton de 9:30
                conttentubicación.setVisibility(View.GONE);

            }
        });

       /* ubicaciontoggleButton.setOnClickListener(new View.OnClickListener()
             {
              @Override
              public void onClick(View v)
              {

                  if(ubicaciontoggleButton.isChecked()){
                      Log.d("plaza",""+ubicaciontoggleButton.getText());
                      ///dice otra ubicación par ael vehiculo
                      conttentubicación.setVisibility(View.VISIBLE);
                      bandPlazaLagos= false;
                  }else{
                      bandPlazaLagos= true;
                      ///dice q va  el vehiculo estara en plaza lagos
                      //asignar Lugar de referencia y la titud y longgitud
                      conttentubicación.setVisibility(View.GONE);
                      Log.d("plaza",""+ubicaciontoggleButton.getText());
                  }
             }
            }
        );*/



        btncita = (Button) getActivity().findViewById(R.id.btncita);
        btncita.setOnClickListener(new View.OnClickListener() {
                @Override
                 public void onClick(View view) {
                    Log.d("celular",""+celular);

                    if(isweakend)
                        {
                            if (!(zona.getText().toString().equals(""))|| bandPlazaLagos) {
                                 //getLocation();
                                     if (Home.PICK_LATITUDE==0 ) {
                                         if(hora_Cita==0){
                                             Toast.makeText(getApplicationContext(), "HORA NO ELEGIDA", Toast.LENGTH_LONG).show();
                                         }else{
                                             if(otraUbicacion.isChecked()==false || plazalagos.isChecked()==false){
                                                 if(checkterm.isChecked()){//!!!!!!!!!!!!!!!!TÉRMINOS Y CONDICIONES!!!!!!!!!!!!
                                                     int selectedId = radioGroup.getCheckedRadioButtonId();
                                                     tdbtn = (RadioButton) getActivity().findViewById(selectedId);
                                                     metodo = tdbtn.getText().toString();

                                                    ConfirmarCita(view.getContext()).show();
                                                     //Toast.makeText(getApplicationContext(), "ACEPTADO...", Toast.LENGTH_LONG).show();

                                                 }else{
                                                     Toast.makeText(getApplicationContext(), "ACEPTE LOS TERMINOS Y CONDICIONES", Toast.LENGTH_LONG).show();
                                                 }

                                             }else{
                                                 Toast.makeText(getApplicationContext(), "VERIFIQUE TODOS LOS DATOS", Toast.LENGTH_LONG).show();
                                             }
                                         }
                                        // Toast.makeText(getApplicationContext(), "GPS DESHABILITADO", Toast.LENGTH_LONG).show();

                                      } else {
                                         if(hora_Cita==0){
                                             Toast.makeText(getApplicationContext(), "HORA NO ELEGIDA", Toast.LENGTH_LONG).show();
                                            }else{
                                             if(checkterm.isChecked()){//!!!!!!!!!!!!!!!!TÉRMINOS Y CONDICIONES!!!!!!!!!!!!

                                                 int selectedId = radioGroup.getCheckedRadioButtonId();
                                                 tdbtn = (RadioButton) getActivity().findViewById(selectedId);
                                                 metodo = tdbtn.getText().toString();
                                                 ConfirmarCita(view.getContext()).show();

                                             //separarcita(view);
                                             }else{
                                                 Toast.makeText(getApplicationContext(), "ACEPTE LOS TERMINOS Y CONDICIONES", Toast.LENGTH_LONG).show();
                                             }
                                         }
                                     }
                             }else{
                                 Toast.makeText(getApplicationContext(),"CAMPOS VACIOS o GPS DESHABILITADO", Toast.LENGTH_LONG).show();
                                    }
                    }else {
                        Toast.makeText(getApplicationContext(),"POR FAVOR SELECCIONE UN DíA VALIDO  ", Toast.LENGTH_LONG).show();
                    }
                 }
            });
       // Calle1.setEnabled(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cita, container, false);
    }
/*
request.setRetryPolicy(
        new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    getRequestQueue().add(request);
* */
   public void separarcita(final View view){

       pd.setMessage("Enviando datos de la cita...");
       pd.show();



       Log.d("cita", Home.IDUSER_QUERY+" "+Calle1.getText().toString()+" "+año+" "+metodo+" "+hora_Cita+" " +
               " "+autosPlaca.getSelectedItem().toString()+" "+Home.PICK_LATITUDE+" "+Home.PICK_LONGITUD);

       celular = num_contac.getText().toString();
       if(!celular.equals("null")){
        String url = URL + "separar-cita.php";
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
           queue.add(new StringRequest(Request.Method.POST, url,
                   new Response.Listener<String>() {

                       @Override
                       public void onResponse(String response) {
                           try {
                               JSONObject oJson = new JSONObject(response);
                               obtnerresultado(oJson ,view );
                           } catch (JSONException e) {
                               e.printStackTrace();

                               Toast.makeText(context,
                                       "Error del servidor. Intente más tarde "+e.getMessage(), Toast.LENGTH_LONG).show();
                           }
                       }
                   }
                   ,
                   new Response.ErrorListener() {
                       @Override
                       public void onErrorResponse(VolleyError error) {
                           /*EN CASO DE ERROR */
                           pd.dismiss();
                           Log.d("cita", " "+error.getCause()+" "+error.getLocalizedMessage()+" "+error.getMessage());

                           Snackbar.make(view, "Compruebe su conexion a internet", Snackbar.LENGTH_LONG)
                                   .setAction("Action", null).show();
                       }
                   }
           ) {
               /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
               @Override
               protected Map<String, String> getParams() throws AuthFailureError {
                   Map<String, String> parameters = new HashMap<>();
                   parameters.put("cliente", Home.IDUSER_QUERY);
                   //Log.d("SPIT",Home.IDUSER_QUERY);
                   parameters.put("fecha", año+"-"+mes+"-"+dia);
                   parameters.put("metodo", "Tarjeta");
                   parameters.put("horacita", ""+hora_Cita);
                   parameters.put("celular", ""+celular);

                   String str = autosPlaca.getSelectedItem().toString();
                   String delimiter = ":";
                   String[] temp;
                   temp = str.split(delimiter);

                   parameters.put("auto", temp[1]);

                   if(bandPlazaLagos){
                       parameters.put("entrega","Audi Plaza Lagos" );//,
                       parameters.put("latitud", "-2.0979646");
                       parameters.put("longitud", "-79.8767075");

                   }else{
                       parameters.put("entrega",Calle1.getText().toString() +" \n"+zona.getText().toString() );
                       parameters.put("latitud", ""+Home.PICK_LATITUDE);
                       parameters.put("longitud", ""+Home.PICK_LONGITUD);

                   }

                   Log.d("cita", Home.IDUSER_QUERY+" "+Calle1.getText().toString()+" "+año+" "+metodo+" "+hora_Cita+" "+" "+temp[1]+" "+Home.PICK_LATITUDE+" "+Home.PICK_LONGITUD);

                   return parameters;
               }
           });


    }else {
            Snackbar.make(view, "Por favor vaya a su perfil y agerge un número de telefono", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }



    private void obtnerresultado(JSONObject response, View view) {
        try {
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            Log.d("Cita", estado+" "+ mensaje);
            switch (estado) {
                case "1":

                    Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG).setAction("Action", null).show();

                    //String idCita = response.getString("idcita");
                    //SaveIdCita(""+idCita);
                    //EnviarNotificacion();
                    Calle1.setText("");
                    zona.setText("");
                    isweakend= false;
                    Home.PICK_LATITUDE=0;
                    Home.PICK_LONGITUD=0;
                    progressBar.setVisibility(View.INVISIBLE);

                    pd.dismiss();

                    ExitoCita2 frag = new ExitoCita2();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(frame.getId(), frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    break;
                case "2":
                    pd.dismiss();

                   // contn.setVisibility(View.VISIBLE);
                   // progressBar.setVisibility(View.INVISIBLE);
                    Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    break;
                case "0":
                    pd.dismiss();

                    //progressBar.setVisibility(View.INVISIBLE);
                    //contn.setVisibility(View.VISIBLE);
                    Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    break;
                case "3":
                    pd.dismiss();

                    //progressBar.setVisibility(View.INVISIBLE);
                    //contn.setVisibility(View.VISIBLE);
                    Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    break;
                case "4":
                    pd.dismiss();

                    //progressBar.setVisibility(View.INVISIBLE);
                    //contn.setVisibility(View.VISIBLE);
                    Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    //LoginManager.getInstance().logOut();
                    break;
                case "5":
                    pd.dismiss();

                    //progressBar.setVisibility(View.INVISIBLE);
                    //contn.setVisibility(View.VISIBLE);
                    Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    //LoginManager.getInstance().logOut();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }




    /**
    * Aparte de pedir los autos este metodo tambien trae el número de
    * telefono para saber si puede separar citas.
    * */
    void  getautos () {
       RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
       String url = URL + "autos-cliente.php";
       queue.add(new StringRequest(Request.Method.POST, url,
               new Response.Listener<String>() {
                   @Override
                   public void onResponse(String response) {
                       try {
                           JSONObject json = new JSONObject(response);
                           procesarcaros(json);
                       } catch (JSONException e) {

                           Toast.makeText(context,
                                   "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                       }
                   }
               },
               new Response.ErrorListener() {
                   @Override
                   public void onErrorResponse(VolleyError error) {
                        Log.e("servidor", ""+error.getMessage());
                       Toast.makeText(context,
                               "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                   }
               }
       ) {
           /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
           @Override
           protected Map<String, String> getParams() throws AuthFailureError {
               Map<String, String> parameters = new HashMap<>();
               parameters.put("cliente", Home.IDUSER_QUERY );

               return parameters;
           }
       });
   }

    void procesarcaros( JSONObject response){
       int estado = 0;
       try {
           estado = response.getInt("estado");
           switch (estado) {
               case 1:
                   //////////AQUI ENVIRA MENSJE PERMANTE//////////
                   showDialogAcitvarSeguimiento(getActivity()).show();
                   break;
               case 2:
                   JSONArray autos = response.getJSONArray("autos");
                   this.autos = new ArrayList<String>();
                   for (int i = 0; i < autos.length(); i++) {
                       JSONObject array = autos.getJSONObject(i);
                       array.getString("placa");
                       this.autos.add(array.getString("modelo")+" :"+array.getString("placa"));
                   }
                   celular =  response.getString("celular");
                   Log.d("celular", celular);
                   break;
           }
       } catch (JSONException e) {
           e.printStackTrace();
       }



       if(estado == 2){
           ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, this.autos);
           adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
           autosPlaca.setAdapter(adapter);
       }
   }
    public AlertDialog showDialogAcitvarSeguimiento (Context context){

        Log.d("Live","Clic boton ");

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Agregar Audi")
                .setMessage("Se necesita agregar un auto para poder separar una cita")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d("Live","Clic boton ");
                                //
                                addCar frag = new addCar();
                                transaction = fragmentManager.beginTransaction();
                                transaction.replace(frame.getId(), frag);
                                transaction.addToBackStack(null);
                                transaction.commit();

                            }
                        })

                .setCancelable(false);
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();

    }

    public void SaveIdCita(String idCita){
        SharedPreferences Preferens = getActivity().getSharedPreferences("Load_Profile", Context.MODE_PRIVATE);
        SharedPreferences.Editor  editor = Preferens.edit();
        editor.putString("idcita",idCita);
        editor.putString("steep","0");
        editor.commit();
    }

    public void getLocation(){
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        fusedLocationProviderApi = LocationServices.FusedLocationApi;
        apiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        if (apiClient != null) {
            apiClient.connect();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (apiClient != null) {
            apiClient.connect();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PETICION_PERMISO_LOCALIZACION) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Permiso concedido
                @SuppressWarnings("MissingPermission")
                Location lastLocation =
                        LocationServices.FusedLocationApi.getLastLocation(apiClient);
               String locationProviders = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                if (locationProviders == null || locationProviders.equals("")) {
                    dialogGPSupdate dialog = new dialogGPSupdate();
                    dialog.show(getFragmentManager(),"update");
                }else{

                     lastLocation =  LocationServices.FusedLocationApi.getLastLocation(apiClient);
                    Toast.makeText(getApplicationContext(), "location :" + lastLocation.getLatitude() + " , " + lastLocation.getLongitude(), Toast.LENGTH_SHORT).show();
                    LONGITUD=""+lastLocation.getLongitude();
                    LATITUD=""+lastLocation.getLatitude();
                }
            }
        }
    }
    public AlertDialog ConfirmarCita(Context cont) {


        String fecha =  año+"-"+mes+"-"+dia;
        String hora="0";
                if(hora_Cita==1){
                 hora ="9:30";
                }else if(hora_Cita==2){
                    hora ="11:00";
                }else if (hora_Cita==3){
                    hora ="13:00";
                }else if (hora_Cita==4){
                    hora ="15:00";
                }


                String str = autosPlaca.getSelectedItem().toString();


        String delimiter = ":";
        String[] temp;
        temp = str.split(delimiter);
        String auto =""+( temp[1]);
        String dir;
        if(bandPlazaLagos){
             dir = "Audi Plaza Lagos" ;//,
        }else{
          dir =  Calle1.getText().toString() +" \n"+zona.getText().toString() ;
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(cont);

        builder.setTitle("Esta seguro de separa esta cita")
                .setMessage("Placa:"+auto+"\n Dirección: "+dir+"\n Fecha: "+fecha+" Hora: "+hora)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                separarcita(getView());


                            }
                        })
        .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PETICION_PERMISO_LOCALIZACION);
        } else {
            fusedLocationProviderApi.requestLocationUpdates(apiClient,  locationRequest,  this);
            String locationProviders = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (locationProviders == null || locationProviders.equals("")) {
                dialogGPSupdate dialog = new dialogGPSupdate();
                dialog.show(getFragmentManager(),"update");
            }else{
                Location zentrum= LocationServices.FusedLocationApi.getLastLocation(apiClient);;
                Location lastLocation =
                        LocationServices.FusedLocationApi.getLastLocation(apiClient);
                zentrum.setLatitude(-2.169143);
                zentrum.setLongitude(-79.9194962);

                LONGITUD=""+lastLocation.getLongitude();
                LATITUD=""+lastLocation.getLatitude();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        LONGITUD = ""+location.getLatitude();
        LATITUD=""+location.getLongitude();
    }

}
