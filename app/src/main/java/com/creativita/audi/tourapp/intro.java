package com.creativita.audi.tourapp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.widget.Toast;
import com.github.paolorotolo.appintro.AppIntro;




public class intro extends AppIntro {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int firtColor = Color.parseColor("#608cb2");
        int seconColor = Color.parseColor("#5aad71");
        int thirtColor = Color.parseColor("#9f5aad");


        String font_path = "font/AudiTypeV03-ExtendedNormal.otf";  //definimos un STRING con el valor PATH ( o ruta por                                                                                    //donde tiene que buscar ) de nuetra fuente


        String font_path2 = "font/AudiTypeV03-Normal.otf";

        Typeface TF = Typeface.createFromAsset(getAssets(),font_path);


        addSlide(SampleSlide.newInstance(R.layout.fragment_first));
        addSlide(SampleSlide.newInstance(R.layout.fragment_second));
        addSlide(SampleSlide.newInstance(R.layout.fragment_thirt));

        setFadeAnimation();
        showSkipButton(false);
        setProgressButtonEnabled(true);

    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);

        Toast t = Toast.makeText(currentFragment.getContext(), " Saltaste el intro", Toast.LENGTH_LONG);
        t.show();

    }

    ///Al momento de terminar el tour
    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent i = new Intent(intro.this, Log_in.class);
        startActivity(i);
    }
}
