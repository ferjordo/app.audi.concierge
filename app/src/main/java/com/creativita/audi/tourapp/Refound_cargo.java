package com.creativita.audi.tourapp;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paymentez.android.rest.model.ErrorResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Url;



public class Refound_cargo extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    EditText  codigoTrnasaccion, contrasena;
    ProgressDialog pd;
    Button send;
    FrameLayout frame;


    FragmentManager fragmentManager;
    FragmentTransaction transaction;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Refound_cargo() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static Refound_cargo newInstance(String param1, String param2) {
        Refound_cargo fragment = new Refound_cargo();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
          /*  mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);*/
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        pd = new ProgressDialog(getActivity());

        fragmentManager = getFragmentManager();
        frame = (FrameLayout) getActivity().findViewById(R.id.contentFrame);

        codigoTrnasaccion = (EditText)getActivity().findViewById(R.id.refount_code);
        contrasena = (EditText)getActivity().findViewById(R.id.refound_pass);
        send  = (Button)getActivity().findViewById(R.id.btn_refound) ;

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pass = contrasena.getText().toString().replace(" ","");
                String cdTransaccion = codigoTrnasaccion.getText().toString().replace(" ","");


                if(pass.equals("")||cdTransaccion.equals("")){
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    builder.setMessage("Revise que todos los campos esten completos")
                            .setTitle("Campos Incompletos");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }else{
                    pd.setMessage("Autentificando y realizando rembolso...");
                    pd.show();
                   // autentifiar( pass,cdTransaccion);
                    ReembolsoVolley( cdTransaccion, pass);
                }


            }
        });





    }

    public  void autentifiar(final String pass,final String cdTransaccion){
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url=Constantes.URL+"log-in.php";
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject oJson = new JSONObject(response);
                            procesarRespuesta(oJson, cdTransaccion);
                        }catch (JSONException e){
                            e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(getActivity(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("user_FB", Home.Mail );
                parameters.put("user_pass", pass );
                return parameters;
            }
        };
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS*2,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }
    /*funcion para decodificar la respuesta del servicio web*/
    private void    procesarRespuesta(final JSONObject response, String cdTransaccion){
        try{

            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":
                    Log.d("Refound", "Logeado con exito");
                    pd.setMessage("Realizando Rembolso..");
                    ReembolsoVolley( cdTransaccion, "");
                    //ProcesarRefound( cdTransaccion);

                    break;
                case "2":
                    pd.dismiss();
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    builder.setMessage("Revise que los datos sean correctos")
                            .setTitle("Contraseña incorrecta");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();


                    break;

            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void    procesarRespuestaRembolo(final JSONObject response){

        try{

            String estado = response.getString("status");
            String mensaje = response.getString("detail");
            switch (estado) {
                case "success":
                    Log.d("Refound", "Logeado con exito");
                    pd.dismiss();

                    ExitoCita frag = new ExitoCita();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(frame.getId(), frag);
                    transaction.addToBackStack(null);
                    transaction.commit();

                    break;
                case "failure":
                    pd.dismiss();
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    builder.setMessage(mensaje)
                            .setTitle("Transacción no realizada");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();


                    break;
                    //pending
                case "pending":
                    pd.dismiss();
                    final AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());

                    builder1.setMessage("Su rebolso esta pendiente")
                            .setTitle("¨Pendiente ");
                    builder1.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ExitoCita frag = new ExitoCita();
                            transaction = fragmentManager.beginTransaction();
                            transaction.replace(frame.getId(), frag);
                            transaction.addToBackStack(null);
                            transaction.commit();
                        }
                    });

                    AlertDialog dialog1 = builder1.create();
                    dialog1.show();


                    break;

            }
        }catch (JSONException e){
            pd.dismiss();
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setMessage("Datos incorrecto, revise los datos proporcionados ")
                    .setTitle("Error al realizar la transación");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
            e.printStackTrace();
        }
    }





    public  void ReembolsoVolley(final String cdTransaccion, final String pass){
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url=Constantes.URL_PMTZ+"refund-charge";
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject oJson = new JSONObject(response);
                            procesarRespuestaRembolo(oJson);
                        }catch (JSONException e){
                            e.printStackTrace();
                            Toast.makeText(getActivity(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(getActivity(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("transaction_id", cdTransaccion );
                parameters.put("password", pass );
                parameters.put("email", Home.Mail );


                return parameters;
            }
        };
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS*2,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_refound_cargo, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
