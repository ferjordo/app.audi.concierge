package com.creativita.audi.tourapp;

/**
 * Created by PC_TWO on 21/8/2017.
 */

public class Valet {
    String id;
    String nombre;
    String apellido;
    String mail;

    public Valet(  String  id, String nombre, String apellido ){
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;

    }
    public Valet(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
