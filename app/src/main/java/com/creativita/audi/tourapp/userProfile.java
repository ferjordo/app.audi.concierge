package com.creativita.audi.tourapp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class userProfile extends Fragment {

    static final String URL = "https://audiguayaquil.com/SERVICE/";
    static final int REQUEST_IMAGE_CAPTURE = 1;
    Context context;
   static TextView userName, userMail;
    ImageView userImage;
    Button btnChangePass;

    int permissionCheck;
    private static final  int REQUEST_CODE_CAMERA=101;
    private static final  int PHOTO_SELECTED=102;


    String profile;
    PackageManager pm;
    String mCurrentPhotoPath, idFace;
    String imageFileName;
    RequestQueue queue;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            profile = this.getArguments().getString("userProfile");
        }

        Log.d("Id","QUERY "+Home.IDUSER_QUERY);
        Log.d("Id","TIPO  "+Home.TIPO_USER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_profile, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        context = getActivity().getApplicationContext();
        pm = getActivity().getPackageManager();
        queue = Volley.newRequestQueue(context);

        userName = (TextView)getActivity().findViewById(R.id.lblUserName);
        userMail = (TextView)getActivity().findViewById(R.id.lblUserMail);
        userImage = (ImageView)getActivity().findViewById(R.id.imgUserImage);
        btnChangePass = (Button)getActivity().findViewById(R.id.btnPassword);

        Log.d("Id","QUERY "+Home.IDUSER_QUERY);
        Log.d("Id","TIPO  "+Home.TIPO_USER);
        try {
            JSONObject response = new JSONObject(profile);
            userName.setText(response.get("name").toString());
            userMail.setText(response.get("email").toString());
            idFace = response.getString("id");
          /*  Picasso.with(context).load(response.getString("picture"))
                    .into(userImage);
*/
           Picasso.with(context)
                    .load(response.getString("picture"))
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                   .resize(250, 250)
                    .into(userImage);
        }catch (Exception e){
            Log.e("ERROR JSON>>>>>",e.getMessage());
        }

        userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUser();
            }
        });
        userMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUser();
            }
        });

        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setTitle("Cambiar  foto de perfil");
                builder.setIcon(getResources().getDrawable(R.drawable.icono));
                builder.setMessage("Seleccione una opción para elegir foto de perfil");
                builder.setPositiveButton("Cámara", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        permissionCheck = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.CAMERA);
                        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                            captureImage();
                            //selectImage();
                        }
                        else {
                            // Explicar permiso
                            if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                                Toast.makeText(getActivity().getApplicationContext(), "El permiso es necesario para utilizar la cámara.",
                                        Toast.LENGTH_SHORT).show();
                            }

                            // Solicitar el permiso
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CALL_PHONE}, REQUEST_CODE_CAMERA);
                            }
                        }
                    }
                });
                builder.setNegativeButton("Galería", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //dialogInterface.dismiss();

                        permissionCheck = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
                        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                           // captureImage();
                            selectImage();
                        }
                        else {
                            // Explicar permiso
                            if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                                Toast.makeText(getActivity().getApplicationContext(), "El permiso es necesario para utilizar la cámara.",
                                        Toast.LENGTH_SHORT).show();
                            }

                            // Solicitar el permiso
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CALL_PHONE}, PHOTO_SELECTED);
                            }
                        }

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        btnChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pass = new Intent(getActivity(),changePassword.class);
                pass.putExtra("id",idFace);
                pass.putExtra("mail", Home.Mail);
                Log.d("Pass",""+Home.Mail);
                startActivity(pass);
            }
        });
    }

    private void updateUser() {
        dialogUpdateUser dialog = new dialogUpdateUser();
        dialog.show(getFragmentManager(),"update");
        Log.d("update",""+dialog.isHidden());
    }

    private void selectImage () {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PHOTO_SELECTED);
    }

    private void captureImage () {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
        if (cameraIntent.resolveActivity(pm) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e("error","Error no captura img");
            }
            if (photoFile != null) {

                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            }
            //startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory() + "/picupload/autos";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();
        File image = new File(storageDir + "/" + imageFileName + ".jpg");
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;
            int scaleFactor = Math.min(photoW/photoW, photoH/photoH);
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor << 1;
            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            Matrix mtx = new Matrix();
            mtx.postRotate(90);
            Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);



            updateImage(rotatedBMP);
        }else if(requestCode == PHOTO_SELECTED && resultCode == Activity.RESULT_OK){
            Uri selectedImage = data.getData();
            try {
                Bitmap bitmap1 = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                Log.d("img", "  "+selectedImage.getPath().toString());

               /*BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImage.getPath().toString(), bmOptions);
                int photoW = bmOptions.outWidth;
                int photoH = bmOptions.outHeight;
                int scaleFactor = Math.min(photoW/photoW, photoH/photoH);
                bmOptions.inJustDecodeBounds = false;
                bmOptions.inSampleSize = scaleFactor << 1;
                Bitmap bitmap = BitmapFactory.decodeFile(selectedImage.getPath().toString(), bmOptions);
                Matrix mtx = new Matrix();
                mtx.postRotate(90);
                Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);*/
                updateImage(bitmap1);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void updateImage (final Bitmap img) {
        final Drawable image = new BitmapDrawable(getResources(),img);
        String url = URL + "imagen-usuario.php";
        queue.add(new MultipartRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            verificarImagen(oJson);
                        } catch (JSONException e) {
                            Toast.makeText(context,
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(context,
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id", Home.IDUSER_QUERY);
                parameters.put("tipo", Home.TIPO_USER);
                return parameters;
            }
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("txt_image", new DataPart(imageFileName+".jpeg", AppHelper.getFileDataFromDrawable(context, image), "image/jpeg"));
                return params;
            }
        });
    }

    private void verificarImagen (JSONObject response) {
        try {
            String mensaje = response.getString("mensaje");
            if(response.getString("estado").equals("3")){
                String url = Constantes.URL+""+ response.getString("img");
                Log.d("img", url);
                Toast.makeText(context, mensaje, Toast.LENGTH_LONG).show();
                DoJson(url);
                Picasso.with(context)
                        .load(url)
                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .into(userImage);

            }
        } catch (JSONException e) {
            Toast.makeText(context,
                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    // Permiso aceptado
                    captureImage();
                else
                    // Permiso denegado
                    // Toast.makeText(getActivity().getApplicationContext()), "No se ha aceptado el permiso", Toast.LENGTH_SHORT).show();
                    Log.d("Denegado","Denegado");
                return;
            // Gestionar el resto de permisos
            case PHOTO_SELECTED:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    // Permiso aceptado
                    selectImage();
                else
                    // Permiso denegado
                    // Toast.makeText(getActivity().getApplicationContext()), "No se ha aceptado el permiso", Toast.LENGTH_SHORT).show();
                    Log.d("Denegado","Denegado");
                return;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    public  void DoJson(String url){
        Map<String, String> Jsonperfil = new HashMap<>();
        Jsonperfil.put("id",Home.IDUSER_QUERY);
        Jsonperfil.put("name", Home.Nombre );
        Jsonperfil.put("email", Home.Mail );
        Jsonperfil.put("picture", url);

        JSONObject jsonObject = new JSONObject(Jsonperfil);
        SavePerfil(jsonObject.toString(),  "yes", Home.IDUSER_QUERY, Home.TIPO_USER, Home.Celular);
       Home.jsondata= jsonObject.toString();

    }
    public void SavePerfil(String data, String loged, String IDUSER_QUERY, String TIPO_USER, String Celular) {

        SharedPreferences Preferens = getActivity().getSharedPreferences("Load_Profile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = Preferens.edit();
        editor.putString("JSON", data);
        editor.putString("login", loged);
        editor.putString("IDUSER_QUERY", IDUSER_QUERY);
        editor.putString("TIPO_USER", TIPO_USER);
        editor.putString("Celular",Celular);
        editor.commit();
    }
}
