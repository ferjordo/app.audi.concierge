package com.creativita.audi.tourapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class citasActivasAdapter extends ArrayAdapter<citaClas> {

    private static final String URL_BASE = Constantes.URL;
    private List<citaClas> items;
    private RequestQueue queue;
    Context context;

    public citasActivasAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public citasActivasAdapter (final Context context) {
        super(context,0);
        this.context = context;
        queue = Volley.newRequestQueue(this.context);
        String url = URL_BASE + "citas-activas-cliente.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("ADAPTER>>>>>>> ","Solicitando datos al servidor");
                        try {
                            JSONObject oJson = new JSONObject(response);
                            items = parseJson(oJson);
                            addAll(items);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("Data login", "....." + e.getMessage());
                            Toast.makeText(context,
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id", Home.IDUSER_QUERY);
                return parameters;
            }
        });
    }

    private List<citaClas> parseJson(JSONObject jsonObject) {
        List<citaClas> citas = new ArrayList<>();
        JSONArray jsonArray;
        int estado;
        try {
            estado = jsonObject.getInt("estado");
            switch (estado) {
                case 1:
                    String mensaje = jsonObject.getString("mensaje");
                    Toast.makeText(context,
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case 2:
                    jsonArray = jsonObject.getJSONArray("citas");
                    for(int i = 0;i<jsonArray.length(); i++) {
                        try {
                            JSONObject objeto= jsonArray.getJSONObject(i);
                            citaClas cita;
                            cita = new citaClas(objeto.getInt("id"),
                                    objeto.getString("fecha"),
                                    objeto.getString("metodo de pago"),
                                    objeto.getString("lugar de entrega"),
                                    objeto.getString("auto"),
                                    objeto.getString("estado"),
                                    objeto.getString("latitud"),
                                    objeto.getString("longitud"));

                            // Log.d("ADAPTER>>>>>>> "," "+objeto.getString("telefono"));
                            citas.add(cita);
                        }catch (JSONException e) {
                            Log.e("JSON ENCODE>>>>>>>", "Error de parsing: "+ e.getMessage());
                        }
                    }
                    break;
            }
        }catch (JSONException e){
            Log.e("JSON ENCODE>>>>>>>", "Error de parsing: "+ e.getMessage());
        }
        return citas;
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public citaClas getItem(int position) {
        return items.get(position);
    }

    public void remove (int position) {
        this.items.remove(position);
    }

    public View getView(int i, View view, @NonNull ViewGroup viewGroup) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());

        //Salvando la referencia del View de la fila
        View listItemView;

        citasActivasAdapter.ViewHolder holder;

        //Comprobando si el View no existe
        //Si no existe, entonces inflarlo
        listItemView = null == view ? layoutInflater.inflate(
                R.layout.citas_lista,
                viewGroup,
                false) : view;

        citaClas item = this.getItem(i);

        holder = new citasActivasAdapter.ViewHolder();
        holder.textFecha = (TextView) listItemView.findViewById(R.id.txtCitaFecha);
        holder.textMetodo = (TextView) listItemView.findViewById(R.id.txtCitaMetodo);
        holder.textLugar = (TextView) listItemView.findViewById(R.id.txtCitaLugar);
        holder.textAuto = (TextView) listItemView.findViewById(R.id.txtCitaAuto);

        holder.textFecha.setText(item.getFecha());
        holder.textMetodo.setText(item.getMetodoPago());
        holder.textLugar.setText(item.getLugarEntrega());
        holder.textAuto.setText(item.getAuto());

        return listItemView;
    }
    private static class ViewHolder {
        TextView textFecha;
        TextView textMetodo;
        TextView textLugar;
        TextView textAuto;
    }
}
