package com.creativita.audi.tourapp;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */

public class Seminuevo extends Fragment {
    TextView titulo, Año, descipciom, Tmodelo, Tmotor, Tkm, Tprecio, Tac, Ttraccion, Tequipamento, Tinfo;
    ImageView principal, pequena1, pequena2, pequena3;
    String traccion, equipamento, informacion ,modelo, img, img1, img2, img3, kilometros, anio, motor ;
    Double precio;
    int ac;
    ProgressBar progressBar;
    ScrollView content;
    Button cotizar;
    FragmentTransaction transaction;
    FragmentManager fragmentManager;
    FrameLayout frame;

    public Seminuevo() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_seminuevo_single, container, false);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            modelo = this.getArguments().getString("modelo");
            motor = this.getArguments().getString("motor");
            kilometros =""+ this.getArguments().getInt("km");
            precio = this.getArguments().getDouble("precio");
            ac = this.getArguments().getInt("ac");
            traccion = this.getArguments().getString("traccion");
            equipamento = this.getArguments().getString("equipamento");
            informacion = this.getArguments().getString("informacion");
            img = this.getArguments().getString("img1");
            img1 = this.getArguments().getString("img2");
            img2 = this.getArguments().getString("img3");
            img3 = this.getArguments().getString("img4");
            Log.d("seminuevi", ""+img+" "+img1+" "+img2+" "+img3);
        }

        progressBar = (ProgressBar)getActivity().findViewById(R.id.prossSemuinuevo);
        progressBar.setVisibility(View.GONE);
        content= (ScrollView)getActivity().findViewById(R.id.contentSemi);

        fragmentManager = getFragmentManager();
        frame =(FrameLayout) getActivity().findViewById(R.id.contentFrame);

        titulo = (TextView) getActivity().findViewById(R.id.semi_title);
        Año = (TextView) getActivity().findViewById(R.id.anio);
        descipciom = (TextView) getActivity().findViewById(R.id.txtSemiDrescrip);
        principal = (ImageView) getActivity().findViewById(R.id.imgSemiPrincipal);
        pequena1 = (ImageView) getActivity().findViewById(R.id.imgSemilitle1);
        pequena2 = (ImageView) getActivity().findViewById(R.id.imgSemilitle2);
        pequena3 = (ImageView) getActivity().findViewById(R.id.imgSemilitle3);
        cotizar = (Button)getActivity().findViewById(R.id.cotizarr);

        Tmotor = (TextView) getActivity().findViewById(R.id.txtSemiMotor);
        Tkm = (TextView) getActivity().findViewById(R.id.txtSemiKm);
        Tprecio = (TextView) getActivity().findViewById(R.id.txtSemiPrecio);
        Tac = (TextView) getActivity().findViewById(R.id.txtSemiAc);
        Ttraccion = (TextView) getActivity().findViewById(R.id.txtSemiTraccion);
        Tequipamento = (TextView) getActivity().findViewById(R.id.txtSemiEquipamento);
        Tinfo = (TextView) getActivity().findViewById(R.id.txtSemiInfo);

        Tmotor.setText(Tmotor.getText()+motor);
        Tkm.setText(Tkm.getText()+kilometros);
        if(ac==1){
            Tac.setText(Tac.getText()+"Si");
        }else{
            Tac.setText(Tac.getText()+"No");
        }

        Ttraccion.setText(Ttraccion.getText()+traccion);
        Tequipamento.setText(Tequipamento.getText()+equipamento);
        Tinfo.setText(Tinfo.getText()+informacion);
        Tprecio.setText(Tprecio.getText()+""+precio);


        Picasso.with(getContext()).load(img)
                .into(principal);
        Picasso.with(getContext()).load(img1)
                .into(pequena1);
        Picasso.with(getContext()).load(img2)
                .into(pequena2);
        Picasso.with(getContext()).load(img3)
                .into(pequena3);

        pequena1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(getContext()).load( img1)
                        .into(principal);                    }
        });
        pequena2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(getContext()).load(img2)
                        .into(principal);
            }
        });
        pequena3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(getContext()).load(img3)
                        .into(principal);
            }
        });


        //Tmodelo, Tmotor, Tkm, Tprecio, Tac, Ttraccion, Tequipamento, Tinfo;
        cotizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                content.setVisibility(View.INVISIBLE);
                EnviarMail();

            }
        });

     /*   switch (modelo) {
            case "a1":
                /*titulo.setText("Audi "+titulo);
                Año.setText(anio);
                descipciom.setText("Año: "+anio+".\n" +"Modelo: Audi "+titulo+" .\n" +"Kilometraje:"+kilometros);
                principal.setImageDrawable(getResources().getDrawable(R.drawable.a1_1));

                Picasso.with(getApplicationContext()).load(Constantes.URL + img)
                        .into(principal);
                Picasso.with(getApplicationContext()).load(Constantes.URL + img)
                        .into(pequena1);
                Picasso.with(getApplicationContext()).load(Constantes.URL + img)
                        .into(pequena2);
                Picasso.with(getApplicationContext()).load(Constantes.URL + img)
                        .into(pequena3);

                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Picasso.with(getApplicationContext()).load(Constantes.URL + img1)
                                .into(principal);

                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Picasso.with(getApplicationContext()).load(Constantes.URL + img2)
                                .into(principal);
                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Picasso.with(getApplicationContext()).load(Constantes.URL + img3)
                                .into(principal);
                    }
                });

            titulo.setText("Audi A1");
            Año.setText("2017");
            descipciom.setText("Año: 2016?.\n" +"Modelo: Sedan A1 .\n" +"Kilometraje: 8" +"0.000km");
            principal.setImageDrawable(getResources().getDrawable(R.drawable.a3_1));
            pequena1.setImageDrawable(getResources().getDrawable(R.drawable.a3_3));
            pequena2.setImageDrawable(getResources().getDrawable(R.drawable.a3_4));
            pequena3.setImageDrawable(getResources().getDrawable(R.drawable.a3_2));

            pequena1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    principal.setImageDrawable(getResources().getDrawable(R.drawable.a3_3));
                }
            });
            pequena2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    principal.setImageDrawable(getResources().getDrawable(R.drawable.a3_4));
                }
            });
            pequena3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    principal.setImageDrawable(getResources().getDrawable(R.drawable.a3_2));
                }
            });
            break;

            case "a3":
                titulo.setText("Audi A3");
                Año.setText("2017");
                descipciom.setText("Año: 2017?.\n" +"Modelo: Sedan A3 .\n" +"Kilometraje: 60.000km");
                principal.setImageDrawable(getResources().getDrawable(R.drawable.a3_1));
                pequena1.setImageDrawable(getResources().getDrawable(R.drawable.a3_3));
                pequena2.setImageDrawable(getResources().getDrawable(R.drawable.a3_4));
                pequena3.setImageDrawable(getResources().getDrawable(R.drawable.a3_2));

                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a3_3));
                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a3_4));
                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a3_2));
                    }
                });
                break;
        }*/

        }
    public void EnviarMail () {
        //cambiarVentana ();
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = Constantes.URL + "cotizar.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        cambiarVentana ();
                       /* try {
                            JSONObject oJson = new JSONObject(response);

                            //procesarCambio(oJson, cita);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }*/
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        content.setVisibility(View.VISIBLE);

                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("nombre",""+Home.Nombre);
                parameters.put("mail",""+Home.Mail);
                parameters.put("telefono",""+Home.Celular);
                parameters.put("modelo","Audi "+modelo+" "+anio);

                return parameters;
            }
        });
   }

void cambiarVentana (){

    ExitoCita frag = new ExitoCita();

    transaction =  fragmentManager.beginTransaction();
    transaction.replace(frame.getId(),frag);
    transaction.addToBackStack(null);
    transaction.commit();
}
}
