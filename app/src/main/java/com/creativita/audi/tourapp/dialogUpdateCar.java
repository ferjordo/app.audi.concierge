package com.creativita.audi.tourapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class dialogUpdateCar extends DialogFragment {

    private static final String URL = Constantes.URL;
    EditText placaLetra,placaNum,kilometraje;
    private static RequestQueue queue;
    Context mcontext;
    Button update,cancel, eliminar;

    FragmentTransaction transaction;
    FragmentManager fragmentManager;
    FrameLayout frame;

    String placa;
    View v;
    JSONObject response;
    public dialogUpdateCar(){

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder build = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
         v = inflater.inflate(R.layout.dialog_update_car, null);
        placaLetra = (EditText)v.findViewById(R.id.placa_letra);
        placaNum = (EditText)v.findViewById(R.id.placa_num);
        kilometraje = (EditText)v.findViewById(R.id.km);
        update = (Button)v.findViewById(R.id.btnUpdate_car);
        cancel = (Button)v.findViewById(R.id.btnCancelar_car);
        eliminar = (Button)v.findViewById(R.id.eliminarauto);
        mcontext = v.getContext();
        queue = Volley.newRequestQueue(mcontext);
        setUserProfile(Home.jsondata);
        placa= this.getArguments().getString("placa");


        fragmentManager = getFragmentManager();
        frame =(FrameLayout) getActivity().findViewById(R.id.contentFrame);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(placaLetra.getText().toString().equals("") || placaNum.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"CAMPOS VACIOS", Toast.LENGTH_LONG).show();
                }
                else{
                    updatecar();
                }
            }
        });
        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EliminarAuto();
            }
        });

        build.setView(v);

        return build.create();
    }

    public  void  setUserProfile(String jsondata){

        try {
            response = new JSONObject(jsondata);
  //          Log.d("dialog",response.get("email").toString());

//            Log.d("dialog",response.get("name").toString());

        } catch (Exception e){
            e.printStackTrace();
        }
    }



    private void updatecar(){
        String url = URL + "editar-auto.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject oJson = new JSONObject(response);
                            procesarUpdate(oJson);
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("placa", placa);

                Log.d("placa",""+placa);
                parameters.put("placa_new",placaLetra.getText().toString()+"-"+placaNum.getText().toString() );
                Log.d("placa",placaLetra.getText().toString()+"-"+placaNum.getText().toString());
                parameters.put("km",kilometraje.getText().toString());
                return parameters;
               }
        });
    }

    private  void EliminarAuto (){
        String url = URL + "eliminar-auto.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject oJson = new JSONObject(response);
                            procesarEliminar(oJson);
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR     */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("placa", placa);
                Log.d("placa",""+placa);
                return parameters;
            }
        });
    }

    private void procesarUpdate (JSONObject response) {
        try{
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "0":
                    Toast.makeText(getApplicationContext(),mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "1":
                    Toast.makeText(getApplicationContext(),mensaje + " intente más tarde", Toast.LENGTH_LONG).show();
                    break;
                case "2":

                    Toast.makeText(getApplicationContext(),mensaje, Toast.LENGTH_LONG).show();
                    dismiss();
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void procesarEliminar (JSONObject response) {
        try{
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "0":
                    Toast.makeText(getApplicationContext(),mensaje, Toast.LENGTH_LONG).show();
                             break;
                case "1":
                    Toast.makeText(getApplicationContext(),mensaje + " ", Toast.LENGTH_LONG).show();

                    misAutos frag = new misAutos();
                    transaction =  fragmentManager.beginTransaction();
                    transaction.replace(frame.getId(),frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    dismiss();

                    break;
                case "2":

                    Toast.makeText(getApplicationContext(),mensaje, Toast.LENGTH_LONG).show();
                   // dismiss();
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }


}
