package com.creativita.audi.tourapp;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class addmodelo extends Fragment {
  String URL =Constantes.URL;
    EditText modelo ;
    Button btn;
    public addmodelo() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        modelo = (EditText)getActivity().findViewById(R.id.modelotext);
        btn = (Button)getActivity().findViewById(R.id.btn_aggmodel);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!modelo.getText().toString().equals("")){
                    Aggmodelo();
                }
                else{
                    Toast.makeText(getActivity().getApplicationContext(),"CAMPOS VACIOS ",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_addmodelo, container, false);
    }

    public void Aggmodelo ( ){
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = URL+"agregar-modelo.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject oJson = new JSONObject(response);
                            procesarRespuesta(oJson);
                        }catch (JSONException e){
                            Toast.makeText(getActivity().getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("model", modelo.getText().toString() );
                return parameters;
            }
        });
    }

    private void procesarRespuesta(JSONObject response){
        try{
            String estado = response.getString("estado");
            switch (estado) {
                case "2":
                    /*Modelo ingresado con exito*/
                    modelo.setText("");
                    Toast.makeText(getApplicationContext(),
                            response.getString("mensaje"), Toast.LENGTH_LONG).show();
                    break;
                case "0":
                    /*modelo ya existe*/
                    Toast.makeText(getApplicationContext(),
                            response.getString("mensaje"), Toast.LENGTH_LONG).show();
                    break;
                case "1":
                    /*Fallo al ingresar modelo*/
                    Toast.makeText(getApplicationContext(),
                            response.getString("mensaje"), Toast.LENGTH_LONG).show();
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

}
