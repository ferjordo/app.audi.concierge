package com.creativita.audi.tourapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdapterModelo   extends ArrayAdapter<modeloClass> {

    private static final String URL_BASE = Constantes.URL+"listar-modelos_page.php";
    private static final String URL_Imagen = "https://audiguayaquil.com/SERVICE/";
    private List<modeloClass> items;
    private RequestQueue queue;
    Context context;

    public AdapterModelo(final Context context) {
        super(context, 0);
        this.context = context;
        queue = Volley.newRequestQueue(this.context);
        queue.add(new StringRequest(Request.Method.POST, URL_BASE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray oJson = new JSONArray(response);
                            items = parseJson(oJson);
                            addAll( items);
                        } catch (JSONException e) {
                            Toast.makeText(context,
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("cliente", "");
                return parameters;
            }
        });
    }

    private List<modeloClass> parseJson(JSONArray jsonObjectArray) {
        List<modeloClass> Modelos = new ArrayList<>();
        JSONArray jsonArray;

       // jsonArray = jsonObject.getJSONArray("imagenes");
        for (int i = 0; i < jsonObjectArray.length(); i++) {
            try {

                JSONObject objeto = jsonObjectArray.getJSONObject(i);
                int id = objeto.getInt("id");
                String modelo = objeto.getString("modelo");
                String titulo = objeto.getString("titulo");
                String descripcion = objeto.getString("descripcion");
                JSONArray imagenes = objeto.getJSONArray("imagenes");

                //Log.d("JSON ENCODE>>>>>>>", ""+imagenes.toString());
                List<String> imgs = new ArrayList<>();;
                for (int j = 0; j < imagenes.length(); j++) {
                    JSONObject imagen = imagenes.getJSONObject(j);
                   // Log.d("JSON ENCODE>>>>>>>", ""+imagen.getString("url"));
                    Log.d("JSON ENCODE>>>>>>>", ""+imagen.getString("url"));
                    imgs.add(imagen.getString("url"));
                }
//              public modeloClass(String modelo, String titulo, String descripcion, List<String> imgs) {

                modeloClass modelos = new modeloClass(id, modelo,  titulo, descripcion, imgs);

                Modelos.add(modelos);
            }catch ( JSONException e ){
                Log.e("JSON ENCODE>>>>>>>", "Error de parsing: " + e.getMessage());
            }
        }


        return Modelos;
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public modeloClass getItem(int position) {
        return items.get(position);
    }

    @NonNull
    @Override
    public View getView(int i, View view, @NonNull ViewGroup viewGroup) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());

        //Salvando la referencia del View de la fila
        View listItemView;

        final AdapterModelo.ViewHolder holder;

        //Comprobando si el View no existe
        //Si no existe, entonces inflarlo
        listItemView = null == view ? layoutInflater.inflate(
                R.layout.listas_modelos,
                viewGroup,
                false) : view;

        modeloClass item = this.getItem(i);

        holder = new AdapterModelo.ViewHolder();
        holder.textTitulo = (TextView) listItemView.findViewById(R.id.modeloTitulo);
        holder.imagenPrincipal = (ImageView) listItemView.findViewById(R.id.modeloPirncilal);
        Picasso.with(context).load( item.getImgs().get(0).toString())
                .into(holder.imagenPrincipal);
        holder.textTitulo.setText("Audi " + item.getModelo());

        // Procesar item
        return listItemView;
    }

    private static class ViewHolder {
        ImageView imagenPrincipal;
        TextView textTitulo;
    }

}

