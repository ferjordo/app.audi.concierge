package com.creativita.audi.tourapp;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Debug;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;


public class citasActivas extends Fragment {
    ListView lstMicitas;
    citasActivasAdapter adapter;
    Context context;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    FrameLayout frame;


    public citasActivas() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_citas_activas, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lstMicitas = (ListView)getActivity().findViewById(R.id.lstMiCitas);
        context = getActivity().getApplicationContext();
        frame =(FrameLayout) getActivity().findViewById(R.id.contentFrame);

        fragmentManager = getFragmentManager();

        adapter = new citasActivasAdapter(context);

        lstMicitas.setAdapter(adapter);
        lstMicitas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                /***/
                String estado="";
                citaClas cita = adapter.getItem(i);
                SingleCita frag = new SingleCita();
                if(cita.getEstado().equals("1")){
                    estado= "recibir";
                }else if(cita.getEstado().equals("2")){
                    estado= "reparacion";
                }else if (cita.getEstado().equals("3")){
                    Log.d ("CitaActiva","YESS..!");
                    estado= "entregado";
                }else if (cita.getEstado().equals("4")){
                    estado= "finalizado";
                }
                Bundle args = new Bundle();
                args.putInt("id", cita.getCitaid());
                args.putString("lugar", cita.getLugarEntrega());
                args.putString("auto", cita.getAuto());
                args.putString("fecha", cita.getFecha());
                args.putString("estado", estado);
               // Log.d ("CitaActiva",""+cita.getEstado());
                Log.d ("CitaActiva","ID: "+cita.getCitaid()+" ESTADO: "+cita.getEstado()+" DIRECCIÓN: "+ cita.getLugarEntrega()+" AUTO: "+cita.getAuto());
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();


                /***/
              /*  citaClas cita = adapter.getItem(i);
                SingleCita frag = new SingleCita();
                Bundle args = new Bundle();
                Log.d ("CitaActiva",""+cita.getCitaid());
                Log.d ("CitaActiva",""+cita.getEstado());
                args.putInt("id", cita.getCitaid());
                args.putString("lugar", cita.getLugarEntrega());
                args.putString("auto", cita.getAuto());
                args.putString("fecha", cita.getFecha());
                if(cita.getEstado()=="1"){
                    args.putString("estado", "recibir");
                }else if(cita.getEstado()=="2"){
                    args.putString("estado", "reparacion");
                }else if (cita.getEstado()=="3"){
                    args.putString("estado", "entregado");
                }else if (cita.getEstado()=="4"){
                    args.putString("estado", "finalizado");
                }

                Log.d ("CitaActiva",""+cita.getCitaid()+" "+cita.getEstado()+" "+cita.getCitaid()+" "+ cita.getLugarEntrega()+" "+cita.getAuto());
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
*/
            }
        });
    }
}
