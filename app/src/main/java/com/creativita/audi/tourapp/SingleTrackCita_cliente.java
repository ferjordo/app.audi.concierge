package com.creativita.audi.tourapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.ArrayList;
import java.util.List;

public class SingleTrackCita_cliente extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    int IDCITA;
    public static DocumentReference mDocRef;
    public static final String LATITUD_KEY = "latitud";
    public static final String LONGITUD_KEY = "longitud";
    public static final String NOMBRE_KEY = "nombre";
    public static final String SPEED_KEY = "speed";
    public static final String PLACA_KEY = "placa";
    public static final String INIT_KEY = "nombre";
    public static final String HISTORIAL_KEY = "historial";


    List<LatLng> list = new ArrayList<>(); /// Lista de todas las ubicaciones


    boolean mapReady= false;
    final LatLng ubi = new LatLng(-2.172725, -79.9078817);
    LatLng fromLocation = new LatLng(-2.172725, -79.9078817);
    boolean first= true;

    final MarkerOptions mk =  new MarkerOptions ();
TextView nombreValet, PlacaAuto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_track_cita_cliente);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
         IDCITA = intent.getIntExtra("id",0);
         nombreValet = (TextView) findViewById(R.id.nombreValet);
         PlacaAuto = (TextView)findViewById(R.id.placaAuto);
        mDocRef = FirebaseFirestore.getInstance().document( "Location"+ "/" + IDCITA);

    }
    public void Atras (View view){
        onBackPressed();
    }
    @Override
    protected void onStart() {

        super.onStart();


        mDocRef.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent( DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                if(documentSnapshot.exists()){
                    // QuoteFirebase myQuote = documentSnapshot.toObject(QuoteFirebase.class);
                    //documentSnapshot.getDocumentReference(""+)
                    String latitud = documentSnapshot.getString(LATITUD_KEY);
                    String longitud = documentSnapshot.getString(LONGITUD_KEY);
                    String speed = documentSnapshot.getString(SPEED_KEY);
                    String placa = documentSnapshot.getString(PLACA_KEY);
                    String nombre = documentSnapshot.getString(NOMBRE_KEY);
                   // list =(List<LatLng>) documentSnapshot.get(HISTORIAL_KEY);

                    PlacaAuto.setText("Placa: "+placa);
                    nombreValet.setText("Nombre: "+nombre);
                    if(nombre.equals("...")){
                        nombreValet.setText("El valet ha detenido el modo de seguimiento ");

                    }
/*
                    if(nombre.isEmpty()){
                        PlacaAuto.setText("Valet aun no activa el modo de seguimiento");
                        nombreValet.setText("Buscando Valet");
                    }*/
                   // int = documentSnapshot.getString()
                    //String cita  = documentSnapshot.

                    //Toast.makeText(getApplicationContext(), Double.valueOf(latitud)+" , "+Double.valueOf(longitud), Toast.LENGTH_LONG).show();
                 //   Toast.makeText(getApplicationContext(), longitud, Toast.LENGTH_LONG).show();
                    if(mapReady ){
                        mMap.clear();
                        if(first){
                            fromLocation= new LatLng(Double.valueOf(latitud), Double.valueOf(longitud));
                            mk.position(fromLocation);
                            mk.title(nombre);
                            mk.icon(BitmapDescriptorFactory.fromResource(R.drawable.track_auto));
                            mMap.addMarker(mk);
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.valueOf(latitud),Double.valueOf(longitud)), 18.0f));

                            first=false;
                        }
                        LatLng toLocation = new LatLng(Double.valueOf(latitud), Double.valueOf(longitud));
                        Marker marker = mMap.addMarker(new MarkerOptions().position(fromLocation).title(nombre).icon(BitmapDescriptorFactory.fromResource(R.drawable.track_auto)));//first location

                        MarkerAnimation.animateMarkerToICS(marker, toLocation, new LatLngInterpolator.Spherical());
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.valueOf(latitud),Double.valueOf(longitud)),18.0f));
                           //mMap.addPolygon()
                        list.add(fromLocation);
                        PolylineOptions polyOptions = new PolylineOptions();
                        polyOptions.color(getResources().getColor(R.color.rojo_semitransparente));
                        polyOptions.width(5);
                        polyOptions.addAll(list);
                        /*
                        * new PolylineOptions()
                        .add(fromLocation, toLocation)
                        .width(5)
                        .color(getResources().getColor(R.color.rojo_semitransparente))
                        * */
                        Polyline linePolyline = mMap.addPolyline(polyOptions);
                        fromLocation = toLocation;

                    }



                }
            }
        });
    }
    public  void showialog(){
       // showDialogStopSeguimiento().show();
    }
    public AlertDialog showDialogStopSeguimiento (Context context){

        Log.d("Live","Clic boton ");

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Modo de seguimiento")
                .setMessage("Ya ha terminado el modo de seguimiento")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d("Live","Clic boton ");
                                /*
                                Home.PLACA = Placa;
                                Home.mDocRef = FirebaseFirestore.getInstance().document( "Location"+ "/" + IDCITA);
                                Home.canTrack=true;*/
                                onBackPressed();
                            }
                        })

                .setCancelable(false);
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mk.position(ubi);
        mk.title("Audi Zentrum Guayaquil");
        //mMap.addMarker(mk);
       // mMap.moveCamera(CameraUpdateFactory.newLatLng(ubi));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-2.172725, -79.9078817), 18.0f));

        mapReady=true;
    }
}
