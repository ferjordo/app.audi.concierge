package com.creativita.audi.tourapp;


import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.QuickContactBadge;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.text.TextUtils;
import android.provider.Settings;
import android.widget.ToggleButton;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paymentez.android.Paymentez;
import com.paymentez.android.model.Card;
import com.paymentez.android.rest.model.ErrorResponse;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;


public class SingleCita extends Fragment {

    private static final String URL_BASE = Constantes.URL;

    private static final String ARG_ID ="id";
    private static final String ARG_LUGAR = "lugar";
    private static final String ARG_AUTO = "auto";
    private static final String ARG_FECHA = "fecha";
    private static final String ARG_ESTADO = "estado";

    private String CARD_TOKEN  = "";
    private String CARD_TYPE   = "";
    private String CARD_LAST4  = "";
    private String CARD_NAME   = "";
    private double ORDER_AMOUNT = 0.0;


    private int id ;
    static int hora_Cita = 0;
    private String lugar;
    private String auto;
    private String fecha;
    private String estado;
    public  static  String fechaEnviar ="null";

    private static final String URL = Constantes.URL;
    public JSONObject response;

    public static ImageView logoCard;
    public static TextView numCard, nameCard;

    public TextView LugarReferencia, descripcion, Precio, notifiacion;
    public static TextView  Direccion , fechaSingle, title, txtparaDetail;
    static Button btn1, btn2 , btn3, btn4 , btn5, btn6, btnValet, ubicacionValet;

    //RadioGroup RadioDireccion;
    RadioButton taller;
    RadioButton plazaLagos;

    RadioButton Recibido;
    RadioButton Reparado;
    RadioButton finalizado;
    RadioButton Entregado;
    Button send ;
    Button eliminar;
   // Button ubicacion2;
   static ToggleButton esHoy;
   static LinearLayout eshoyContent ,HorasBotones;

    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    FrameLayout frame;
    LinearLayout linearLayout ;
    ProgressBar progressBarSingle;
    RelativeLayout card_select;
    TextView TyC;
    CheckBox checkterm ;
    ProgressDialog pd;



    ConsultasServidor consultasServidor;
    public SingleCita() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        Home.fragmentToPicker = 2;
        fragmentManager = getFragmentManager();
        frame = (FrameLayout) getActivity().findViewById(R.id.contentFrame);
        linearLayout = (LinearLayout)getActivity().findViewById(R.id.contenSingleElement) ;
        progressBarSingle  = (ProgressBar) getActivity().findViewById(R.id.progressBarSingle) ;

        fechaSingle = (TextView) getActivity().findViewById(R.id.fechaSingle);
        descripcion = (TextView) getActivity().findViewById(R.id.despcription);
        Direccion = (TextView) getActivity().findViewById(R.id.LugarToEntrga);
        LugarReferencia = (TextView) getActivity().findViewById(R.id.LugarToEntrgaReference);
        Precio = (TextView) getActivity().findViewById(R.id.precioS);
        notifiacion= (TextView)getActivity().findViewById(R.id.txtnotificacions);
        txtparaDetail = (TextView)getActivity().findViewById(R.id.txtparaDetail);

        ////////////////Cambios para la enrega y recepción en PLAZA lagos
        taller =(RadioButton)getActivity().findViewById(R.id.taller);
        plazaLagos = (RadioButton)getActivity().findViewById(R.id.plazaLagos);
        esHoy = (ToggleButton)getActivity().findViewById(R.id.esHoy);
        eshoyContent = (LinearLayout)getActivity().findViewById(R.id.eshoyContent);
        HorasBotones = (LinearLayout)getActivity().findViewById(R.id.HorasBotones);

        TyC = (TextView)getActivity().findViewById(R.id.termycondS);
        TyC.setMovementMethod(LinkMovementMethod.getInstance());

        checkterm = (CheckBox)getActivity().findViewById(R.id.checktermS);

        logoCard = (ImageView) getActivity().findViewById(R.id.imageSelectBrandCard);
        numCard = (TextView) getActivity().findViewById(R.id.textSelectCardNumber);
        nameCard = (TextView) getActivity().findViewById(R.id.textSelectCardHoldersName);

        if(Home.UID.equals("") || Home.UID.equals(null)||Home.UID.equals("null")){
            getUID ( Home.Mail, Home.pass) ;
        }

        pd = new ProgressDialog(getActivity());


        card_select = (RelativeLayout) getActivity().findViewById(R.id.card_select);
        card_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("UID", "Se ha dado clic");
                CargarDatosTarjeta();

            }
        });

        title = (TextView) getActivity().findViewById(R.id.infotitle);
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInfoCita dialogInfoCita = new DialogInfoCita();
                dialogInfoCita.show(fragmentManager,"noticias");
            }
        });
        Recibido =(RadioButton)getActivity().findViewById(R.id.recibido);
        //Recibido.setEnabled(false);
        Recibido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInfoCita dialogInfoCita = new DialogInfoCita();
                dialogInfoCita.show(fragmentManager,"noticias");
                //Recibido.setChecked(true);
            }
        });
        Reparado =(RadioButton)getActivity().findViewById(R.id.reparado);
        //Reparado.setEnabled(false);

        Reparado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInfoCita dialogInfoCita = new DialogInfoCita();
                dialogInfoCita.show(fragmentManager,"noticias");
                Reparado.setChecked(false);

                if(estado.equals("recibir")){

                    Reparado.setChecked(false);}
                    else{
                    Reparado.setChecked(true);
                }
            }
        });
        Entregado =(RadioButton)getActivity().findViewById(R.id.entregado);
        /*  recibir reparacion entregado finalizado */
        Entregado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInfoCita dialogInfoCita = new DialogInfoCita();
                dialogInfoCita.show(fragmentManager,"noticias");

                if(estado.equals("recibir")||estado.equals("reparacion")){
                    Entregado.setChecked(false);}
                    else{
                    Entregado.setChecked(true);
                }
            }
        });
        finalizado=(RadioButton)getActivity().findViewById(R.id.finalizado);
        finalizado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInfoCita dialogInfoCita = new DialogInfoCita();
                dialogInfoCita.show(fragmentManager,"noticias");
                Log.d("estado",""+estado);

                if(estado.equals("recibir")||estado.equals("reparacion")||estado.equals("entregado")){
                    finalizado.setChecked(false);}else {
                    finalizado.setChecked(true);
                }

            }
        });

        btnValet = (Button)getActivity().findViewById(R.id.verValet);
        btnValet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FotoInfoValet dialogFotoValet = new FotoInfoValet();
                dialogFotoValet.setIdCita(""+id);
                dialogFotoValet.show(fragmentManager,"valet");
                Log.d("Datos Valet", ""+id);

            }
        });

        send = (Button)getActivity().findViewById(R.id.btnsenddirection);
        //ubicacion2 = (Button)getActivity().findViewById(R.id.btnubiccion2);
        ubicacionValet = (Button)getActivity().findViewById(R.id.ubicacionValet);


        ///Se necesita hacer el servicio web donde se consulte si ya se ha realizado el star del trakin
        ubicacionValet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //startTracking(""+id );
                goToTrack(id);
            }
        });
        //ubicacionValet.setVisibility(View.GONE);

         eliminar = (Button)getActivity().findViewById(R.id.btnEliminar);
            eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    linearLayout.setVisibility(View.GONE);
                    progressBarSingle.setVisibility(View.VISIBLE);
                EliminarCita(id);
                }
            });
        eliminar.setVisibility(View.GONE);

        Log.d("Direccion"," "+estado);
        if (estado.equals("recibir")){
            Recibido.setChecked(true);
            LugarReferencia.setEnabled(false);
            Direccion.setEnabled(false);

            notifiacion.setVisibility(View.VISIBLE);
            //send.setVisibility(View.GONE);

            eliminar.setVisibility(View.VISIBLE);
            //  ubicacion2.setEnabled(false);
            //linearLayout
        }else if (estado.equals("reparacion")){
            ubicacionValet.setVisibility(View.GONE);
            Recibido.setChecked(true);
            Reparado.setChecked(true);
            finalizado.setChecked(false);

            notifiacion.setVisibility(View.VISIBLE);
            //send.setVisibility(View.GONE);
            eliminar.setVisibility(View.GONE);
            LugarReferencia.setEnabled(true);
            Direccion.setEnabled(true);
            ubicacionValet.setVisibility(View.GONE);
          //  ubicacion2.setEnabled(true);

        }else if (estado.equals("entregado")){
            Recibido.setChecked(true);
            Reparado.setChecked(true);
            ubicacionValet.setVisibility(View.GONE);
            Entregado.setChecked(true);
            finalizado.setChecked(false);
            LugarReferencia.setEnabled(true);
            Direccion.setEnabled(true);
            notifiacion.setVisibility(View.VISIBLE);
            eliminar.setVisibility(View.GONE);
            //ubicacion2.setEnabled(true);
           // send.setVisibility(View.VISIBLE);
            Log.d("Term", "Mostrar 1 SEND");

        }else {
            ubicacionValet.setVisibility(View.GONE);
            Recibido.setChecked(true);
            Reparado.setChecked(true);
            Entregado.setChecked(true);
            finalizado.setChecked(true);
            LugarReferencia.setEnabled(false);
            Direccion.setEnabled(false);
            notifiacion.setVisibility(View.INVISIBLE);
           // send.setVisibility(View.GONE);
            eliminar.setVisibility(View.GONE);

          //  ubicacion2.setEnabled(false);

        }
        btn1= (Button) getActivity().findViewById(R.id.sbtn1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn1.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));
                btn2.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                btn3.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                btn4.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                hora_Cita=1;
            }
        });

        btn2= (Button) getActivity().findViewById(R.id.sbtn2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn2.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));
                btn1.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                btn3.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                btn4.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                hora_Cita=2;
            }
        });

        btn3= (Button) getActivity().findViewById(R.id.sbtn3);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn3.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));
                btn1.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                btn2.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                btn4.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                hora_Cita=3;
            }
        });

        btn4= (Button) getActivity().findViewById(R.id.sbtn4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn4.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));
                btn1.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                btn2.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                btn3.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                hora_Cita=4;
            }
        });

        btn5= (Button) getActivity().findViewById(R.id.sbtn5);


        btn6= (Button) getActivity().findViewById(R.id.sbtn6);


        /*
        plazaLagos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("fecha", "Plaza Lagos");
                eshoyContent.setVisibility(View.VISIBLE);
                HorasBotones.setVisibility(View.VISIBLE);
                //Consular Fechas para l día de hoy
                Log.d("fecha", "Consultar al servidor citas disponibles de hoy");
            }
        });
        */
        taller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eshoyContent.setVisibility(View.GONE);
                HorasBotones.setVisibility(View.GONE);
                fechaSingle.setVisibility(View.GONE);
                Log.d("fecha", "Taller");
            }
        });
        final Calendar c = Calendar.getInstance();
        int mMonth = c.get(Calendar.MONTH)+1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        int  weekDay =  c.get(Calendar.DAY_OF_WEEK);
        Log.d("date", "Días: "+ weekDay);
        Log.d("date", "Días: Mes:"+ mMonth+ " Lunes: "+Calendar.MONDAY+" \n Martes: "+Calendar.TUESDAY+"\n " +
                "Miercoles: "+Calendar.WEDNESDAY+"\n Jueves: "+Calendar.THURSDAY+"\n Viernes: "+Calendar.FRIDAY+
                "\n Sábado: "+Calendar.SATURDAY );

        if (weekDay==1  || weekDay==7){
            esHoy.setChecked(true);
            esHoy.setEnabled(false);
            plazaLagos.isChecked();
            Log.d("date",""+plazaLagos.isChecked() );
            if(plazaLagos.isChecked()){
                if(esHoy.isChecked()){
                    fechaSingle.setVisibility(View.VISIBLE);
                    Log.d("date", "Es "+esHoy.getText()+" "+esHoy.isChecked());
                }else{
                    fechaSingle.setVisibility(View.GONE);
                    Log.d("date", "Es "+esHoy.getText()+" "+esHoy.isChecked());
                }
            }

            Log.d("date", " Es sabado o domingo");
        }else {
            if((mMonth==12 && mDay == 31) || (mDay==1 && mMonth==1)){
                esHoy.setChecked(true);
                esHoy.setEnabled(false);


            }else {
            Log.d("date", " Día entre semana valido");
            }
        }

        esHoy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//                fechaEnviar="null";
                SingleCita.btn2.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                SingleCita.btn3.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                SingleCita.btn4.setBackground(getResources().getDrawable(R.drawable.borderbottom));
                hora_Cita=0;

             if(esHoy.isChecked()){
                 //////                 OTRO DÍAA!!!!!!!!!!!!!!!!!
                 fechaSingle.setVisibility(View.VISIBLE);
                 //txtfecha.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                 Log.d("fecha", "Es... "+esHoy.getText()+" ");
             }else{
                 //////                     HOY!!!!!!!!!!!!!!!!!

                 fechaSingle.setVisibility(View.GONE);
                 Calendar calendar = Calendar.getInstance();
                 int mHora = calendar.get(Calendar.HOUR_OF_DAY);
                 int mMes = calendar.get(Calendar.MONTH)+1;
                 int anio = calendar.get(Calendar.YEAR);
                 int dia = calendar.get(Calendar.DAY_OF_MONTH);
                 String fecha_hoy = anio+"-"+mMes+"-"+dia;
                 Log.d("fecha", anio+"-"+mMes+"-"+dia);


                 conulta_hora_x_cita(fecha_hoy, mHora);
                 Log.d("fecha", "Es "+esHoy.getText());


             }

            }
        });


        consultarDescripcion(id);


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkterm.isChecked()){
                        if (taller.isChecked() == plazaLagos.isChecked()) {
                            Log.d("sendir", "No ha sido elegido");
                            dialogDirNoAsignada(view.getContext()).show();

                        } else {
                            String fechaFinalizar = ObtnerFechaActual();
                            if (taller.isChecked()) {

                                Checkout( id,  -2.0979646,  -79.8767075  , " Audi Zentrum Guayaquil" ,"Taller" , auto,  fechaFinalizar, 7  );

                            }else{
                                if (!esHoy.isChecked()) {
                                    if (hora_Cita == 0) {
                                        dialoghoraNoAsignada(view.getContext()).show();
                                    } else {
                                            if(LugarReferencia.getText().toString().equals("")||Direccion.getText().toString().equals("")){
                                                dialogFechaNoAsignada(view.getContext()).show();
                                            }else{
                                                Log.d("sendir", "\nID: "+id + "\n Latitud: " + Home.PICK_LATITUDE + "\n Latitud: " + Home.PICK_LONGITUD +"\n Dirección: " +  Direccion.getText().toString() +"\n Luagar Referencia: " + LugarReferencia.getText().toString() +"\n Auto: " +  auto +"\n Fecha: " +  fechaFinalizar + "\n hora: " + hora_Cita);
                                                //AsignarSegundaDireccon ( id, Home.PICK_LATITUDE , Home.PICK_LONGITUD  , Direccion.getText().toString() ,LugarReferencia.getText().toString() , auto,  fechaEnviar, hora_Cita  );
                                                Checkout( id, Home.PICK_LATITUDE , Home.PICK_LONGITUD  , Direccion.getText().toString() ,LugarReferencia.getText().toString() , auto,  fechaEnviar, hora_Cita  );
                                            }
                                    }
                                }else{
                                    if (hora_Cita == 0) {
                                        dialoghoraNoAsignada(view.getContext()).show();
                                    } else {
                                            if(LugarReferencia.getText().toString().equals("")||Direccion.getText().toString().equals("")||fechaEnviar.equals("null")){
                                                dialogFechaNoAsignada(view.getContext()).show();
                                            }else{
                                                ///enviar a servidor!!!!
                                                Log.d("sendir", "\nID: "+id + "\n Latitud: " + Home.PICK_LATITUDE + "\n Latitud: "
                                                        + Home.PICK_LONGITUD +"\n Dirección: " +  Direccion.getText().toString() +"\n Luagar Referencia: "
                                                        + LugarReferencia.getText().toString() +"\n Auto: " +  auto +"\n Fecha: " +  fechaEnviar + "\n hora: " +
                                                        hora_Cita);
                                                //AsignarSegundaDireccon ( id, Home.PICK_LATITUDE , Home.PICK_LONGITUD  , Direccion.getText().toString() ,LugarReferencia.getText().toString() , auto,  fechaEnviar, hora_Cita  );
                                                Checkout ( id, Home.PICK_LATITUDE , Home.PICK_LONGITUD  , Direccion.getText().toString() ,LugarReferencia.getText().toString() , auto,  fechaEnviar, hora_Cita  );

                                            }
                                    }
                                }
                            }

                        }
                    }else{
                    dialogTerminosYCondiciones(view.getContext()).show();
                    //Toast.makeText(getApplicationContext(),"ACEPTE LOS TÉRMINOS  CODICIONES", Toast.LENGTH_LONG).show();
                }

            }
        });


    }
    /** FUNCION PARA VOOLVER A AUTENTICA EN FIREBASE**/
    void getUID (String mail,  String pass){
        Log.d("pass", mail+" - "+pass);
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        AuthCredential credential = EmailAuthProvider
                .getCredential(mail, pass);

        user.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Home.UID =  user.getUid();
                        Log.d("UID", Home.UID);
                    }
                });
    }
    public  String ObtnerFechaActual(){
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH) + 1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        int weekDay = c.get(Calendar.DAY_OF_WEEK);
        int mHora = c.get(Calendar.HOUR_OF_DAY);

        String fechaFinalizar = mYear + "-" + mMonth + "-" + mDay;

        return fechaFinalizar;
    }
    public AlertDialog dialogTerminosYCondiciones(Context cont) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cont);

        builder.setTitle("Términos y condiciones")
                .setMessage("Aún no a seleccionado los términos y condiones ")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();
    }
    public AlertDialog dialogDirNoAsignada(Context cont) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cont);

        builder.setTitle("Seleciones una localidad")
                .setMessage("Selecione una opción para poder  retirar su auto")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();
    }
    public AlertDialog dialoghoraNoAsignada(Context cont) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cont);

        builder.setTitle("Elija una hora")
                .setMessage("Selecione una hora  ")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();
    }
    public AlertDialog dialogFechaNoAsignada(Context cont) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cont);

        builder.setTitle("Campos vacios")
                .setMessage("Rellene todos los campos para poder indicar una dirección de entrega  ")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();
    }
    private void goToTrack(int id){
    Intent intent = new Intent(getApplicationContext(), SingleTrackCita_cliente.class);
    intent.putExtra("id", id);
    startActivity(intent);
}



    public static SingleCita newInstance(String param1, String param2) {
        SingleCita fragment = new SingleCita();
        Bundle args = new Bundle();
        args.putString(ARG_LUGAR, param1);
        args.putString(ARG_AUTO, param2);
        fragment.setArguments(args);
        return fragment;
    }



    public void CargarActivity(String url){
        // Start User Session by starting MainActivity
        Intent mainActivityIntent = new Intent(
                getApplicationContext(), UbicacionValetActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mainActivityIntent.putExtra("IdCita",id);
        Log.d("Live", "::::"+id);
        mainActivityIntent.putExtra("url",url);
        mainActivityIntent.putExtra("Telefono",Home.Celular);//No tiene sentido enviar este nuemero se necesita lnumero de la cita
        //Log.d("Live","URL: " +url+ " Telefono: "+Home.Celular);

        startActivity(mainActivityIntent);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id =  getArguments().getInt(ARG_ID);
            Home.CITA_KEY = ""+id; /// PARA HACER CONSULTA DE LA UBICACIÓN EN TIEMPO REAL
            lugar = getArguments().getString(ARG_LUGAR);
            auto = getArguments().getString(ARG_AUTO);
            fecha = getArguments().getString(ARG_FECHA);
            estado = getArguments().getString(ARG_ESTADO);
            Log.d("Direccion arg","ID: "+id+" LUGAR: "+lugar+" AUTO: "+auto+" FECHA: "+fecha+" ESTADO: "+estado);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_single_cita, container, false);
    }

    public void AsignarSegundaDireccon (final int cita, final double latitud, final  double longitud, final  String direccion , final  String referencia,final String placa, final String fechal, final int horas ) {
        pd.setMessage("Enviando datos de la cita...");

        String url = URL + "asignar-lugar-recibir.php";
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            procesaDireccion(oJson, cita, placa);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id",""+cita);
                parameters.put("direcion",""+direccion);
                parameters.put("referencia",""+referencia);
                parameters.put("latitud",""+latitud);
                parameters.put("longitud",""+longitud);
                parameters.put("fecha",fechal);
                parameters.put("hora",""+horas);
                Log.d("Direccion","ID: "+cita+" Dirección: "+direccion+"" +
                        " Referencia: "+referencia+" Latitud: "+latitud+" Longitud: "+longitud+" Fecha Envio: "+fechaEnviar+" Hora: "+hora_Cita);
                return parameters;
            }
        };
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    private void procesaDireccion (JSONObject response, int id, String placa) {
        try {
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");

            linearLayout.setVisibility(View.VISIBLE);
            progressBarSingle.setVisibility(View.GONE);
            switch (estado) {
                case "1":
                    pd.dismiss();
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    Direccion.setText("");
                    LugarReferencia.setText("");
                    Log.d("NOTIFICACION "," cambiar ventana: enviar notificación");
                    //EnviarNotificacion(id, placa);
                    ExitoCita frag = new ExitoCita();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(frame.getId(), frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    linearLayout.setVisibility(View.VISIBLE);
                    progressBarSingle.setVisibility(View.GONE);
                   // dismiss();
                    break;
                case "2":
                    //linearLayout.setVisibility(View.VISIBLE);
                   // progressBarSingle.setVisibility(View.GONE);
                    pd.dismiss();
                    Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();
                    //dismiss();
                    break;
                case "3":
                   // linearLayout.setVisibility(View.VISIBLE);
                    //progressBarSingle.setVisibility(View.GONE);
                    pd.dismiss();
                    Toast.makeText(getApplicationContext(),  mensaje, Toast.LENGTH_LONG).show();
                    //dismiss();
                case "0":
                    pd.dismiss();
                    //linearLayout.setVisibility(View.VISIBLE);
                    //progressBarSingle.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(),  mensaje, Toast.LENGTH_LONG).show();
                    //dismiss();
                    break;
            }
        }catch (JSONException e) {
            pd.dismiss();
            e.printStackTrace();
        }
    }

    public void  consultarDescripcion(final int cita){

        String url = URL + "consultar-descripcion-cita.php";
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            procesaDescripcion(oJson);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id",""+cita);
                return parameters;
            }
        });


    }

    public void  EliminarCita(final int cita){

        String url = URL + "eliminar-cita.php";
        RequestQueue queue = Volley.newRequestQueue(getActivity().getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            procesaEliminar(oJson);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                            linearLayout.setVisibility(View.VISIBLE);
                            progressBarSingle.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }

        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id",""+cita);
                return parameters;
            }
        };
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);


    }

    private void procesaDescripcion (JSONObject response) {
        try {

            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            String detalle = response.getString("detalle");
            String estadoCita = response.getString("estadoCita");
            Double precio = response.getDouble("precio");
            ORDER_AMOUNT =  precio;

            String fecha = response.getString("fecha");
            String hora = response.getString("hora");

            String direccion = response.getString("direccion");
            String referencia = response.getString("referencia");

            //String segundaDireccion = ""+response.getString("segundaDireccion");

            Log.d("Detalles", direccion + " " +fecha+" "+detalle+" "+precio+" "+referencia+" "+direccion);
            switch (estado) {
                case "1":
                    if (estadoCita.equals("3")){
                        ///// calcular que mensaje mostras si es mas important preguntar primero por el precio o por el estado
                        if(precio.equals("0")){
                            descripcion.setText("Aun no se añade una descipción a su cita");
                            Precio.setText("Precio: $0");
                            send.setVisibility(View.GONE);

                        }else{
                            descripcion.setText(detalle);
                            Precio.setText("Precio: $"+precio);
                            if (direccion.equals("null")){
                                send.setVisibility(View.VISIBLE);

                            }else{
                                txtparaDetail.setText("Su auto esta previsto que lo entregen  el "+fecha+" en "+direccion+ " "+referencia);
                                notifiacion.setText("Usted ya ha asignado una dirección de entrega a esta cita");
                                send.setVisibility(View.GONE);/// ES NECESARIO MOSTRAR DESPUES DE PRUEBAS

                            }
                        }

                    }else{
                        send.setVisibility(View.GONE);

                    }


                    break;
                case "2":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    //dismiss();
                    break;

            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void procesaEliminar (JSONObject response) {
        try {
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            Log.d("Eliminar", mensaje + " "+estado);
            switch (estado) {
                case "5":
                    ExitoCita frag = new ExitoCita();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(frame.getId(), frag);
                    transaction.addToBackStack(null);
                    transaction.commit();

                    break;
                case "2":
                    linearLayout.setVisibility(View.VISIBLE);
                    progressBarSingle.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();

                    break;

            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void EnviarNotificacion(final int idcita, final String placa) {

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = URL_BASE + "Notificacion-track.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("NOTIFICACION "," cambiar ventana: "+response);
                        /*ExitoCita frag = new ExitoCita();
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(frame.getId(), frag);
                        transaction.addToBackStack(null);
                        transaction.commit();*/
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERROR EN ENVIAR ", " " + error.getMessage());
                      /*  Toast.makeText(context,
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();*/
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                Log.d("NOTIFICACION "," "+idcita+" "+" "+placa);
                parameters.put("id", ""+idcita);
                parameters.put("placa", ""+placa);
                parameters.put("notificar", "nueva");
                return parameters;
            }
        });

    }

    public void conulta_hora_x_cita (final String fecha, final int hora){
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = URL + "consultar-cupo-cita.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("data",response);
                            JSONObject oJson = new JSONObject(response);
                            Get_Hora_cita_single(oJson, hora);
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tard "+ e.getMessage() , Toast.LENGTH_LONG).show();
                            Log.e("data", "Error de parsing: "+ e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("fecha", fecha);
                return parameters;
            }
        });


    }
    private void Get_Hora_cita_single ( JSONObject jsonObject, int hora) {
        SingleCita.btn1.setEnabled(true);
        SingleCita.btn2.setEnabled(true);
        SingleCita.btn3.setEnabled(true);
        SingleCita.btn4.setEnabled(true);

        SingleCita.btn1.setBackground(getResources().getDrawable(R.drawable.borderbottom));
        SingleCita.btn2.setBackground(getResources().getDrawable(R.drawable.borderbottom));
        SingleCita.btn3.setBackground(getResources().getDrawable(R.drawable.borderbottom));
        SingleCita.btn4.setBackground(getResources().getDrawable(R.drawable.borderbottom));

        JSONArray jsonArray;
        int estado =0;
        try {
            estado = jsonObject.getInt("estado");
            Log.d("data", " Este es el estado:"+estado);
            switch (estado) {
                case 0:
                        btn1.setEnabled(true);
                        btn2.setEnabled(true);
                        btn3.setEnabled(true);
                        btn4.setEnabled(true);
                        // SingleCita.btn5.setEnabled(true);
                        //  SingleCita.btn6.setEnabled(true);
                        //setBackground(getResources().getDrawable(R.drawable.borderbottom));

                    break;
                case 1:

                        btn1.setEnabled(true);
                        btn2.setEnabled(true);
                        btn3.setEnabled(true);
                        btn4.setEnabled(true);
                        //SingleCita.btn5.setEnabled(true);
                        //SingleCita.btn6.setEnabled(true);

                    String mensaje = jsonObject.getString("mensaje");
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case 2:
                    Log.d("data", " Operación dentro del estado:"+estado);
                    jsonArray = jsonObject.getJSONArray("horas");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject objeto = jsonArray.getJSONObject(i);
                            Log.d("data", ""+objeto.getInt("hora"));
                            int idHora = objeto.getInt("hora");
                            if(idHora==1){

                                    btn1.setEnabled(false);
                                    btn1.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));

                                Log.d("data", "Cambia de color");
                            }else if(idHora==2){

                                    btn2.setEnabled(false);
                                    btn2.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));




                            } else if (idHora==3){
                                    btn3.setEnabled(false);
                                    btn3.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));

                            } else if (idHora==4){
                                    btn4.setEnabled(false);
                                    btn4.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));



                            } else if (idHora == 5){
                                    btn5.setEnabled(false);
                                    btn5.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));



                            } else if (idHora == 6){
                                    btn6.setEnabled(false);
                                    btn6.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));



                            }

                        } catch (JSONException e) {
                            Log.e("data", "Error de parsing: " + e.getMessage());
                        }
                        Log.d("data", " Dentro del for de arreglo de horas despues de cambiar de color " );
                    }
                    break;
            }

        }catch (JSONException d){
            Log.e("JSON ENCODE>>>>>>>", "NO hay nada o mala formato: " + d.getMessage());
        }
        bloquearHoras(hora);
    }
    public void bloquearHoras (int Hora){
        if(Hora> 14){
            Log.d("hora","Mayor a 14");
            esHoy.setChecked(true);
            esHoy.setEnabled(false);
            fechaSingle.setVisibility(View.VISIBLE);
        }else if(Hora>=12){
            Log.d("hora","Mayor a 12");
            btn3.setEnabled(false);
            btn3.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
            Log.d("hora","Se deberia bloquear los botones");
            btn2.setEnabled(false);
            btn2.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
        }else if(Hora>=10){
            Log.d("hora","Mayor a 10");
            btn2.setEnabled(false);
            btn2.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
        }else{
            Log.d("hora","asdasd");
        }
    }
    public void CargarDatosTarjeta(final int cita, final double latitud, final  double longitud, final  String direccion , final  String referencia,final String placa, final String fechal, final int horas ){

        Intent intent = new Intent(getApplicationContext(), ListCardsActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("mail",Home.Mail);
        intent.putExtra("UID",Home.UID);
        startActivity(intent);


    }
    public void CargarDatosTarjeta(){

       /* datosTarjeta frag = new datosTarjeta();
        transaction = fragmentManager.beginTransaction();
        transaction.replace(frame.getId(), frag);
        transaction.addToBackStack(null);
        transaction.commit();*/
        Intent intent = new Intent(getApplicationContext(), ListCardsActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("mail",Home.Mail);
        intent.putExtra("UID",Home.UID);
        startActivityForResult(intent, Constantes.SELECT_CARD_REQUEST);


    }
    /*
    *  final ProgressDialog pd = new ProgressDialog(ListCardsActivity.this);
        pd.setMessage("");
        pd.show();
        pd.dismiss();

        */
    public void Checkout(final int cita, final double latitud, final  double longitud, final  String direccion , final  String referencia,final String placa, final String fechal, final int horas ){
        //final BackendService backendService = RetrofitFactory.getClient().create(BackendService.class);
        if(CARD_TOKEN == null || CARD_TOKEN.equals("")){
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Es necesario selecionar una tarjeta")
                    .setTitle("Elegir tarjeta");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();


        }else{
            pd.setMessage("Procesando cobro...");
            pd.show();

            //String ORDER_ID = ""+System.currentTimeMillis();

            String ORDER_ID =""+cita+""+placa+""+fechal;

            String ORDER_DESCRIPTION = "ORDER #"+cita+""+placa+""+fechal;
            String DEV_REFERENCE = ORDER_ID;

          /*  backendService.createCharge(""+Home.UID, ""+Paymentez.getSessionId(getActivity()),
                    ""+CARD_TOKEN, ORDER_AMOUNT, ""+DEV_REFERENCE, ""+ORDER_DESCRIPTION, ""+Home.Mail).enqueue(new Callback<CreateChargeResponse>() {
                @Override
                public void onResponse(Call<CreateChargeResponse> call, retrofit2.Response<CreateChargeResponse> response) {

                    CreateChargeResponse createChargeResponse = response.body();
                    if(response.isSuccessful() && createChargeResponse != null && createChargeResponse.getTransaction() != null) {

                        Log.d("TRANSACCION",""+response.body().toString());

                        //AsignarSegundaDireccon (  cita, latitud,  longitud,  direccion ,  referencia, placa,  fechal,  horas);

                    }else {
                        Log.d("TRANSACCION",""+response.body().toString());

                        Gson gson = new GsonBuilder().create();
                        // ErrorResponse errorResponse = gson.fromJson(response.errorBody().string(), ErrorResponse.class);
                        pd.dismiss();
                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                        // builder.setMessage("Intente usar otra trarjeta " + errorResponse.getError().getType())
                        builder.setMessage("El pago no fue procesado intentelo denuevo más tarde, o intente utlizar otra tarjeta  ")

                                .setTitle("Pago no procesado");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();


                    }
                }

                @Override
                public void onFailure(Call<CreateChargeResponse> call, Throwable e) {
                    pd.dismiss();
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    builder.setMessage("message: " +  e.getLocalizedMessage())

                            .setTitle("Error");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();

                }
            });*/
            MakeCharge (""+Home.UID, ""+Paymentez.getSessionId(getActivity()),
                    ""+CARD_TOKEN, ORDER_AMOUNT, ""+DEV_REFERENCE, ""+ORDER_DESCRIPTION, ""+Home.Mail, cita, latitud,  longitud,  direccion ,  referencia, placa,  fechal,  horas);
        }

    }
    public void MakeCharge(final String uid, final String session_id, final String token, final  double amount, final String dev_reference,
                           final String description, final String mail, final int cita, final double latitud, final double  longitud, final String direccion , final String referencia, final String placa,  final String fechal, final int horas ){

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = URL + "paymentez/api/charge-and";

        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            procesar(json,  cita, latitud,   longitud,  direccion ,  referencia,  placa,  fechal,  horas );
                        } catch (JSONException e) {

                            Toast.makeText(getActivity(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("servidor", ""+error.getMessage());
                        Toast.makeText(getActivity(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("uid", uid );
                parameters.put("session_id", session_id );
                parameters.put("token", token );
                parameters.put("amount", ""+amount );
                parameters.put("dev_reference", dev_reference );
                parameters.put("description", description );
                parameters.put("email", mail );

                return parameters;
            }
        }.setRetryPolicy(
                new DefaultRetryPolicy(
                        500000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)));

    }
    void procesar( JSONObject response, final int cita, final double latitud, final double  longitud, final String direccion , final String referencia, final String placa,  final String fechal, final int horas ){
        pd.cancel();
        Log.d("TRANSACCION", ""+response.toString());
        try {
           JSONObject transaccion1 =  response.getJSONObject("transaction");

           if(transaccion1.getString("status").equals("failure")){
               final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                       // builder.setMessage("Intente usar otra trarjeta " + errorResponse.getError().getType())
                       builder.setMessage("El pago no fue procesado intentelo denuevo más tarde, o intente utlizar otra tarjeta  ")

                               .setTitle("Pago no procesado");
                       builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int id) {

                           }
                       });
                       AlertDialog dialog = builder.create();
                       dialog.show();

           }else if (transaccion1.getString("status").equals("pending")){

               ConfirmCodeTransaccion dialog = new ConfirmCodeTransaccion();
               dialog.show(getFragmentManager(),"pending");

           }else if (transaccion1.getString("status").equals("success")){
               AsignarSegundaDireccon (  cita, latitud,  longitud,  direccion ,  referencia, placa,  fechal,  horas);
           }


        } catch (JSONException e) {
            e.printStackTrace();
        }



    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constantes.SELECT_CARD_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                 CARD_TOKEN = data.getStringExtra("CARD_TOKEN");
                 CARD_TYPE = data.getStringExtra("CARD_TYPE");
                 CARD_LAST4 = data.getStringExtra("CARD_LAST4");
                 CARD_NAME = data.getStringExtra("CARD_NAME");


                Log.d("SESSION-TOKEN", "TOKEN "+ CARD_TOKEN+" SESSION "+Paymentez.getSessionId(getActivity()));

                if(CARD_LAST4 != null && !CARD_LAST4.equals("")){
                    numCard.setText("XXXX " + CARD_LAST4);
                    nameCard.setText("" + CARD_NAME);
                    logoCard.setImageResource(Card.getDrawableBrand(CARD_TYPE));
                }

            }
        }
    }
}

