package com.creativita.audi.tourapp;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Noticias extends Fragment {

    FragmentTransaction transaction;
    FragmentManager fragmentManager;
    FrameLayout frame;
    ListView lstNoticas;
    AdapterNoticias adapter;
    Context context;


    public Noticias() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_noticias, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        lstNoticas = (ListView)getActivity().findViewById(R.id.lstNoticas);
        context = getActivity().getApplicationContext();
        fragmentManager = getFragmentManager();
        adapter = new AdapterNoticias(context);
        fragmentManager = getFragmentManager();
        frame =(FrameLayout) getActivity().findViewById(R.id.contentFrame);


        lstNoticas.setAdapter(adapter);
        lstNoticas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

               /* SeminuevoClass modelo = adapter.getItem(i);
                Seminuevo frag = new Seminuevo();
                Bundle args = new Bundle();
                args.putString("modelo", modelo.getModelo());
                args.putString("motor", modelo.getMotor());
                args.putInt("km", modelo.getKm());
                args.putDouble("precio", modelo.getPrecio());
                args.putInt("ac", modelo.getAc());
                args.putString("traccion", modelo.getTraccion());
                args.putString("equipamento", modelo.getEquipamento());
                args.putString("informacion", modelo.getInformacion_adicional());
                args.putString("img1", modelo.getImg1());
                args.putString("img2", modelo.getImg2());
                args.putString("img3", modelo.getImg3());
                args.putString("img4", modelo.getImg4());

                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
*/
            }
        });
    }
}
