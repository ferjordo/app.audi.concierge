package com.creativita.audi.tourapp;

import java.util.List;

public class modeloClass {
    String modelo, titulo, descripcion;
    int id;
     List<String> imgs ;
     String img1, img2, img3, img4;

    public modeloClass(int id, String modelo, String titulo, String descripcion, List<String> imgs) {
        this.id = id;
        this.modelo = modelo;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.imgs = imgs;
        SetSingleImage(imgs);
    }
    void SetSingleImage (List<String> imgs){
        img1 =  imgs.get(0).toString();
        img2 =  imgs.get(1).toString();
        img3 =  imgs.get(2).toString();
        img4 =  imgs.get(3).toString();
    }

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }

    public String getImg4() {
        return img4;
    }

    public void setImg4(String img4) {
        this.img4 = img4;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<String> getImgs() {
        return imgs;
    }

    public void setImgs(List<String> imgs) {
        this.imgs = imgs;
    }
}
