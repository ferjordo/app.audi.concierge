package com.creativita.audi.tourapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paymentez.android.model.Card;
import com.paymentez.android.rest.model.ErrorResponse;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListCardsActivity extends AppCompatActivity {

    ArrayList<Card> listCard;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Button buttonAddCard, buttonTransaccion;

    Context mContext;
    BackendService backendService;
    TextView nocardtxt;

    String UID, MAIL; //GET STRING INTENT

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_cards);

        final Intent intent = getIntent();
        UID = intent.getStringExtra("UID");
        MAIL = intent.getStringExtra("mail");
        Log.d("UID", UID);

        mContext = this;
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        buttonAddCard = (Button) findViewById(R.id.buttonAddCard);
        buttonAddCard.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(mContext, addCard.class);
                intent.putExtra("mail", MAIL);
                intent.putExtra("UID", UID);
                startActivity(intent);
            }
        });
        buttonTransaccion = (Button) findViewById(R.id.buttonTransaccion);
            buttonTransaccion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(mContext, VerifyTransactionActivity.class);
                    intent1.putExtra( "mail", MAIL);
                    intent1.putExtra("UID", UID);
                    startActivity(intent1);

                }
            });

        nocardtxt = (TextView)findViewById(R.id.nocardtxt);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        backendService = RetrofitFactory.getClient().create(BackendService.class);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        getCards();
    }

    public void getCards(){

        final ProgressDialog pd = new ProgressDialog(ListCardsActivity.this);
        pd.setMessage("Buscando tarjetas...");
        pd.show();

        backendService.getCards(UID).enqueue(new Callback<GetCardsResponse>() {
            @Override
            public void onResponse(Call<GetCardsResponse> call, Response<GetCardsResponse> response) {
                pd.dismiss();
                GetCardsResponse getCardsResponse = response.body();
                if(response.isSuccessful()) {

                    listCard = (ArrayList<Card>) getCardsResponse.getCards();

                            if(!listCard.isEmpty()){
                                nocardtxt.setVisibility(View.GONE);
                            }
                    mAdapter = new MyCardAdapter(listCard, new MyCardAdapter.OnCardSelectedListener() {
                        @Override public void onItemClick(Card card) {

                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("CARD_TOKEN",card.getToken());
                            returnIntent.putExtra("CARD_TYPE",card.getType());
                            returnIntent.putExtra("CARD_LAST4",card.getLast4());
                            returnIntent.putExtra("CARD_NAME",card.getHolderName());

                            Log.d("UID", " "+card.getToken() +" "+card.getType()+" "+card.getLast4() );

                            setResult(Activity.RESULT_OK,returnIntent);
                            finish();

                        }
                    }, new MyCardAdapter.OnCardDeletedClickListener() {
                        @Override public void onItemClick(Card card) {
                            deleteCard(card);

                        }
                    });
                    mRecyclerView.setAdapter(mAdapter);
                }else {
                    Gson gson = new GsonBuilder().create();
                    try {
                        ErrorResponse errorResponse = gson.fromJson(response.errorBody().string(), ErrorResponse.class);

                        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                        builder.setMessage( errorResponse.getError().getType())
                                .setTitle("Error");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetCardsResponse> call, Throwable e) {
                pd.dismiss();

                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                builder.setMessage( e.getLocalizedMessage())
                        .setTitle("Error");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });
    }


    public void deleteCard(Card card){

        final ProgressDialog pd = new ProgressDialog(ListCardsActivity.this);
        pd.setMessage("");
        pd.show();

        backendService.deleteCard(UID, card.getToken()).enqueue(new Callback<DeleteCardResponse>() {
            @Override
            public void onResponse(Call<DeleteCardResponse> call, Response<DeleteCardResponse> response) {
                pd.dismiss();
                DeleteCardResponse deleteCardResponse = response.body();
                if(response.isSuccessful()) {
                    getCards();
                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                    builder.setMessage(deleteCardResponse.getMessage())
                            .setTitle("Successfully Deleted Card");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();

                }else {
                    Gson gson = new GsonBuilder().create();
                    try {
                        ErrorResponse errorResponse = gson.fromJson(response.errorBody().string(), ErrorResponse.class);
                        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                        builder.setMessage(errorResponse.getError().getType())
                                .setTitle("Error");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<DeleteCardResponse> call, Throwable e) {
                pd.dismiss();
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                builder.setMessage(e.getLocalizedMessage())
                        .setTitle("Error");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();


            }
        });
    }

    public void viewTransation(){
        Intent intent = new Intent(getApplicationContext(), VerifyTransactionActivity.class);
        intent.putExtra("mail",Home.Mail);
        intent.putExtra("UID",Home.UID);
        startActivity(intent);
    }
}