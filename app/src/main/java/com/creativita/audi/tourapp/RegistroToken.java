package com.creativita.audi.tourapp;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class RegistroToken {

    String TAG = "FireBase";

    private static final String URL = "https://audiguayaquil.com/SERVICE/";
    RequestQueue queue = Volley.newRequestQueue(getApplicationContext());


    RegistroToken (){}

public  void enviarTokenAlServidor (final  String token, final String TipoUser, final String id){


    String url = URL + "ActualizarToken.php";

    queue.add(new StringRequest(Request.Method.POST, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject oJson = new JSONObject(response);
                        VerificarResgistroToken(oJson);
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }
    ){
        @Override
        protected Map<String, String> getParams() throws AuthFailureError {
            Map<String, String> parameters = new HashMap<>();
            parameters.put("tipo",TipoUser);
            parameters.put("token",token);
            parameters.put("id",id);
            return parameters;
        }
    });


}
    private void VerificarResgistroToken (JSONObject response) {
        try {
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "0":
                    Log.d(TAG, " "+mensaje );
                    //Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();

                    break;
                case "1":
                    Log.d(TAG, " "+mensaje );
                    //Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();

                    break;
                case "2":
                    Log.d(TAG, " "+mensaje );
                    // Toast.makeText(getApplicationContext(),mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "3":
                    Log.d(TAG, " "+mensaje );
                    break;
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
