package com.creativita.audi.tourapp;


import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;

import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class Secondmenu extends Fragment {

    FragmentTransaction transaction;
    FragmentManager fragmentManager;
    FrameLayout frame;

    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    public Secondmenu() {
    }



    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        fragmentManager = getFragmentManager();
        frame =(FrameLayout) getActivity().findViewById(R.id.contentFrame);

        ImageView cita =  (ImageView) getActivity().findViewById(R.id.btnsepararcota2);

        cita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                cita frag = new cita();
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        ImageView perfil =  (ImageView) getActivity().findViewById(R.id.perfill);

        perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userProfile frag = new userProfile();
                Bundle args = new Bundle();
                args.putString("userProfile", Home.jsondata);
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

    ImageView noticias = (ImageView)getActivity().findViewById(R.id.btnnoticas2);

    noticias.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Noticias frag = new Noticias();
            transaction =  fragmentManager.beginTransaction();
            transaction.replace(frame.getId(),frag);
            transaction.addToBackStack(null);
            transaction.commit();


        }
    });
        ImageView modelos = (ImageView)getActivity().findViewById(R.id.btnmodelos2);
        modelos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Modelos frag = new Modelos();
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
              /*este dielog oes para expliacar las citas
                DialogInfoCita dialogInfoCita = new DialogInfoCita();
                dialogInfoCita.show(fragmentManager,"noticias");*/


            }
        });

    }


    /*
    * */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_secondmenu, container, false);

    }

}
