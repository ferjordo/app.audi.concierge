


package com.creativita.audi.tourapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SingleActivity extends AppCompatActivity {



    private int hora_Cita;
    private String  idCita;
    private String estado;
    public  static  String fechaEnviar;

    private static final String URL = "https://audiguayaquil.com/SERVICE/";

    public JSONObject response;


    public TextView LugarReferencia, descripcion, Precio, notifiacion;
    public static TextView Direccion,   fechaSingle;
    static Button  btn1, btn2 , btn3, btn4 , btn5, btn6,btnValet, btnValetUbicacion;

    String lugarEntrga, auto, idValet, descricion, precio;
    int mYear, mMonth, mDay, mHour;

    private int PLACE_PICKER_REQUEST = 1;
    static double PICK_LATITUDE, PICK_LONGITUD;
    static String PICK_DIRECCION;
    String URL_TRACK;
    RadioButton Recibido;
    RadioButton Reparado;
    RadioButton finalizado;
    RadioButton Entregado;
    Button send ;

    LinearLayout linearLayout ;
    ProgressBar progressBarSingle;

  //  Button ubicacion2;
  private int[] icons = new int[]{R.drawable.nav_blue, R.drawable.nav_green};
    private Random random;

    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);
        random = new Random();

        fragmentManager = getFragmentManager();
        linearLayout = (LinearLayout)findViewById(R.id.contenSingleElementActivity) ;
        progressBarSingle  = (ProgressBar) findViewById(R.id.progressBarSingleActivity) ;


        Intent intent = getIntent();
        idCita =  intent.getStringExtra("id");
       // URL_TRACK = intent.getStringExtra("url")

        fechaSingle = (TextView)findViewById(R.id.fechaSingle3);
        descripcion = (TextView)findViewById(R.id.despcription3);
        Direccion  = (TextView)findViewById(R.id.LugarToEntrga3);
        LugarReferencia =(TextView)findViewById(R.id.LugarToEntrgaReference3);
        Precio = (TextView)findViewById(R.id.precioS3);
        notifiacion= (TextView)findViewById(R.id.txtnotificacion);

        Recibido =(RadioButton)findViewById(R.id.recibido3);
        Recibido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInfoCita dialogInfoCita = new DialogInfoCita();
                dialogInfoCita.show(fragmentManager,"noticias");
            }
        });
        Reparado =(RadioButton)findViewById(R.id.reparado3);
        Reparado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInfoCita dialogInfoCita = new DialogInfoCita();
                dialogInfoCita.show(fragmentManager,"noticias");

                if(estado.equals("recibir")){
                    Reparado.setChecked(false);}
                else{
                    Reparado.setChecked(true);
                }
            }
        });
        Entregado =(RadioButton)findViewById(R.id.entregado3);
        Entregado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInfoCita dialogInfoCita = new DialogInfoCita();
                dialogInfoCita.show(fragmentManager,"noticias");
                if(estado.equals("recibir")||estado.equals("reparacion")){
                    Entregado.setChecked(false);}
                else{
                    Entregado.setChecked(true);
                }
            }
        });

        finalizado=(RadioButton)findViewById(R.id.finalizado3);
        finalizado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInfoCita dialogInfoCita = new DialogInfoCita();
                dialogInfoCita.show(fragmentManager,"noticias");

                if(estado.equals("recibir")||estado.equals("reparacion")||estado.equals("entregado")){
                    finalizado.setChecked(false);}else {
                    finalizado.setChecked(true);
                }

            }
        });
        send = (Button)findViewById(R.id.btnsenddirection3);
       // ubicacion2 = (Button)findViewById(R.id.btnubiccion3);

        btnValet = (Button)findViewById(R.id.verValetActicity);
        btnValet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            FotoInfoValet dialogFotoValet = new FotoInfoValet();
            dialogFotoValet.setIdCita(""+idCita);
            dialogFotoValet.show(fragmentManager,"valet");
            Log.d("Datos Valet", ""+idCita);

            }
        });

        /////////CONTINUAR AQUI!!!!!!!!!!!!!
        btnValetUbicacion = (Button)findViewById(R.id.ubicaionValetActicity);
        btnValetUbicacion.setVisibility(View.GONE);
        btnValetUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            startTracking(idCita);
            }
        });
        btnValetUbicacion.setVisibility(View.GONE);

        Log.d("Direccion"," "+estado);

        consultarInformacionCita(idCita);



        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Direccion.getText().toString().equals("")||LugarReferencia.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Campos Vacios", Toast.LENGTH_LONG).show();

                }else{
                    linearLayout.setVisibility(View.GONE);
                    progressBarSingle.setVisibility(View.VISIBLE);
                   AsignarSegundaDireccon(idCita ,PICK_LATITUDE, PICK_LONGITUD,Direccion.getText().toString() ,LugarReferencia.getText().toString());
                }
            }
        });



    }




    public void cargarBotones(){

        if (estado.equals("recibir")){
            Recibido.setChecked(true);
            LugarReferencia.setEnabled(false);
            Direccion.setEnabled(false);
            notifiacion.setVisibility(View.VISIBLE);
            send.setVisibility(View.INVISIBLE);
            //ubicacion2.setEnabled(false);
        }else if (estado.equals("reparacion")){
            Recibido.setChecked(true);
            Reparado.setChecked(true);
            finalizado.setChecked(false);
            notifiacion.setVisibility(View.VISIBLE);
            LugarReferencia.setEnabled(true);
            Direccion.setEnabled(true);
            send.setVisibility(View.INVISIBLE);
            //ubicacion2.setEnabled(true);

        }else if (estado.equals("entregado")){
            Recibido.setChecked(true);
            Reparado.setChecked(true);
            Entregado.setChecked(true);
            finalizado.setChecked(false);
            LugarReferencia.setEnabled(true);
            Direccion.setEnabled(true);
            notifiacion.setVisibility(View.INVISIBLE);
           // ubicacion2.setEnabled(true);
            send.setVisibility(View.VISIBLE);

        }else {
            Recibido.setChecked(true);
            Reparado.setChecked(true);
            Entregado.setChecked(true);
            finalizado.setChecked(true);
            LugarReferencia.setEnabled(false);
            Direccion.setEnabled(false);
            send.setVisibility(View.INVISIBLE);

           //
            //
            //
            // ubicacion2.setEnabled(false);

        }

    }

    public void SetOnclicButtom(){
        btn1= (Button) findViewById(R.id.sbtn31);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn1.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));
                btn2.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                btn3.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                btn4.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                //btn5.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                //btn6.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                hora_Cita=1;
            }
        });

        btn2= (Button) findViewById(R.id.sbtn32);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn2.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));
                btn1.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                btn3.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                btn4.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
               // btn5.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                //btn6.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                hora_Cita=2;
            }
        });

        btn3= (Button) findViewById(R.id.sbtn33);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btn3.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));
                btn1.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                btn2.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                btn4.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
               // btn5.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
               // btn6.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                hora_Cita=3;
            }
        });

        btn4= (Button) findViewById(R.id.sbtn34);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn4.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));
                btn1.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                btn2.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                btn3.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
               // btn5.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
              //  btn6.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                hora_Cita=4;
            }
        });

        btn5= (Button) findViewById(R.id.sbtn35);
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn5.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));
                btn1.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                btn2.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                btn3.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
               // btn4.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
              //  btn6.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                hora_Cita=5;
            }
        });

        btn6= (Button) findViewById(R.id.sbtn36);
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn6.setBackgroundColor(getResources().getColor(R.color.rojo_semitransparente));
                btn1.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                btn2.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                btn3.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
              //  btn4.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
              //  btn5.setBackgroundColor(getResources().getColor(R.color.rojo_transparente));
                hora_Cita=6;
            }
        });


    }
    private void startTracking(String trackingId) {
/*
        List<MarkerOption> markerOptionList = new ArrayList<>();
        String[] ids = trackingId.split(",");
        for (String id : ids) {
            MarkerOption option = new MarkerOption(id);
            option.setMarkerTitle(id);
            option.setIconMarker(icons[random.nextInt(icons.length)]);
            markerOptionList.add(option);
        }
        TrackingBuilder builder = new TrackingBuilder(markerOptionList);
        Teliver.startTracking(builder.build());*/
    }

    public void cargarDatos(){
        descripcion.setText(descricion);
        Precio.setText(precio);
    }

    public void AsignarSegundaDireccon (final String cita,final double latitud, final  double longitud,final  String direccion ,final  String referencia) {
        String url = URL + "asignar-lugar-recibir.php";
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            procesaDireccion(oJson , cita);
                            Log.d(" send","enviado datos...");
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id",""+cita);
                parameters.put("direcion",""+direccion);
                parameters.put("referencia",""+referencia);
                parameters.put("latitud",""+latitud);
                parameters.put("longitud",""+longitud);
                parameters.put("fecha",""+fechaEnviar);// asignar fecha del activity HOME
                parameters.put("hora",""+hora_Cita);
                return parameters;
            }
        });
    }

    private void procesaDireccion (JSONObject response, String cita) {
        try {
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    Direccion.setText("");
                    LugarReferencia.setText("");
                   // EnviarNotificacion(cita);
                    linearLayout.setVisibility(View.VISIBLE);
                    progressBarSingle.setVisibility(View.GONE);

                    Intent intent = new Intent(this, Home.class);
                    startActivity(intent);
                    break;
                case "2":

                    linearLayout.setVisibility(View.VISIBLE);
                    progressBarSingle.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "3":
                    linearLayout.setVisibility(View.VISIBLE);
                    progressBarSingle.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();
                    break;
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*Peticion al servidor de los datos de la cita*/
    public void  consultarInformacionCita(final String cita){

        String url = URL + "consultar-datos-cita.php";
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            RecibirDatos(oJson);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id",""+cita);
                //Log.d("Direccion","ID: "+cita +"Referencia: "+referencia+" Longitud: "+ longitud+" Latitud:  "+ latitud);
                return parameters;
            }
        });


    }

    private void RecibirDatos (JSONObject response) {
        try {
            String estadoC = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estadoC) {
                case "1":

                    lugarEntrga= response.getString("lugarEntrega");
                    auto = response.getString("auto");
                    idValet = response.getString("idValet");
                    descricion = response.getString("descripcion");
                    estado = response.getString("estadoCita");
                    precio = response.getString("precio");

                    cargarBotones();
                    SetOnclicButtom();
                    cargarDatos();
                    break;
                case "2":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "3":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();


                    break;
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void fechaS(View view) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);



        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Date date = new Date(year, monthOfYear, dayOfMonth - 1);

                        if (date.getDay() == 6 || date.getDay() == 0) {
                            Toast.makeText(getApplicationContext(), "Los Sábados y Domingos  no se brinda este servicio ", Toast.LENGTH_LONG).show();
                            cita.isweakend = false;
                        } else {
                            Log.d("Fecha", dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            String fechaCita = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            conulta_hora_x_cita_single (fechaCita);
                            fechaSingle.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            fechaEnviar = year +"-"+(monthOfYear + 1) +"-"+dayOfMonth;
                            mHour = c.get(Calendar.HOUR_OF_DAY);
                            Log.d ("Fecha", (mMonth+1)+" ::: "+(monthOfYear + 1)+"  "+year+" ::: "+mYear+" ::: "+mHour);

                            if ((mMonth == monthOfYear  )&& (mYear == year ) && (mDay == dayOfMonth)){/*
                                Log.d ("Fecha", "Es la misma fecha q hoy");
                                if (mHour > 8){
                                    Log.d ("Fecha", "Es más tarde que las 9:am");
                                    btn1.setEnabled(false);
                                    btn1.setBackgroundColor(Color.GRAY);

                                }
                                if (mHour >= 11){
                                    Log.d ("Fecha", "Es más tarde que las 11:am");

                                    btn2.setEnabled(false);
                                    btn2.setBackgroundColor(Color.GRAY);


                                }
                                if (mHour >= 13){

                                    Log.d ("Fecha", "Es más tarde que las 13:am");

                                    btn3.setEnabled(false);
                                    btn3.setBackgroundColor(Color.GRAY);

                                }
                                if (mHour >= 14){
                                    Log.d ("Fecha", "Es más tarde que las 14:am");

                                    btn4.setEnabled(false);
                                    btn4.setBackgroundColor(Color.GRAY);

                                }
                                if (mHour >= 16){
                                    Log.d ("Fecha", "Es más tarde que las 16:am");

                                   btn5.setBackgroundColor(Color.GRAY);
                                    btn5.setEnabled(false);
                                }
                               // btn6.setBackgroundColor(Color.GRAY);
                             //  btn6.setEnabled(false);
                            */}else{
                                Log.d ("Fecha", " hora diferente ");

                               btn1.setEnabled(true);
                               btn2.setEnabled(true);
                                btn3.setEnabled(true);
                               btn4.setEnabled(true);
                          //      btn5.setEnabled(true);
                           //     btn6.setEnabled(true);
                            }

                        }


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.show();

    }
    public void conulta_hora_x_cita_single (final String fecha){
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = URL + "consultar-cupo-cita.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("data",response);
                            JSONObject oJson = new JSONObject(response);
                            Get_Hora_cita_single(oJson);
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tard "+ e.getMessage() , Toast.LENGTH_LONG).show();
                            Log.e("data", "Error de parsing: "+ e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("fecha", fecha);
                return parameters;
            }
        });


    }

    private void Get_Hora_cita_single ( JSONObject jsonObject)
    {
        JSONArray jsonArray;
        int estado =0;
        try {
            estado = jsonObject.getInt("estado");
            Log.d("data", " Este es el estado:"+estado);
            switch (estado) {
                case 0:

                        btn1.setEnabled(true);
                        btn2.setEnabled(true);
                        btn3.setEnabled(true);
                        btn4.setEnabled(true);
                      //  btn5.setEnabled(true);
                       // btn6.setEnabled(true);


                    break;
                case 1:

                        btn1.setEnabled(true);
                        btn2.setEnabled(true);
                        btn3.setEnabled(true);
                        btn4.setEnabled(true);
                       // btn5.setEnabled(true);
                       // btn6.setEnabled(true);

                    String mensaje = jsonObject.getString("mensaje");
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case 2:
                    Log.d("data", " Operación dentro del estado:"+estado);
                    jsonArray = jsonObject.getJSONArray("horas");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject objeto = jsonArray.getJSONObject(i);
                            Log.d("data", ""+objeto.getInt("hora"));
                            int idHora = objeto.getInt("hora");
                            if(idHora==1){

                                    btn1.setEnabled(false);
                                    btn1.setBackgroundColor(Color.GRAY);

                                Log.d("data", "Cambia de color");
                            }else if(idHora==2){
                                 btn2.setEnabled(false);
                                    btn2.setBackgroundColor(Color.GRAY);


                            } else if (idHora==3){
                                btn3.setEnabled(false);
                                    btn3.setBackgroundColor(Color.GRAY);

                            } else if (idHora==4){
                                btn4.setEnabled(false);
                                    btn4.setBackgroundColor(Color.GRAY);


                            } else if (idHora == 5){
                              //  btn5.setEnabled(false);
                                  //  btn5.setBackgroundColor(Color.GRAY);


                            } else if (idHora == 6){
                              // btn6.setEnabled(false);
                                   // btn6.setBackgroundColor(Color.GRAY);


                            }

                        } catch (JSONException e) {
                            Log.e("data", "Error de parsing: " + e.getMessage());
                        }
                        Log.d("data", " Dentro del for de arreglo de horas despues de cambiar de color " );
                    }
                    break;
            }

        }catch (JSONException d){
            Log.e("JSON ENCODE>>>>>>>", "NO hay nada o mala formato: " + d.getMessage());
        }
    }

    public void getPicker(View view) {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(SingleActivity.this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void EnviarNotificacion(final String idcita /*, final String placa*/) {

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = URL + "NotificarAlCrearCita.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("NOTIFICACION ",""+response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERROR EN MODELOS", " " + error.getMessage());
                      /*  Toast.makeText(context,
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();*/
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();

                parameters.put("id", ""+idcita);
                parameters.put("placa", ""+auto);
                parameters.put("notificar", "nueva");
                return parameters;
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                PICK_LATITUDE = place.getLatLng().latitude;
                PICK_LONGITUD = place.getLatLng().longitude;

                PICK_DIRECCION = String.format("%s", place.getAddress());

                    SingleActivity.Direccion.setText(PICK_DIRECCION);


                Log.d("Activty ", PICK_DIRECCION + " " + PICK_LATITUDE + " " + PICK_LONGITUD);

            }
        }
    }
}
