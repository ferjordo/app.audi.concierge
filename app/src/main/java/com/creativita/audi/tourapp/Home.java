package com.creativita.audi.tourapp;

import android.*;
import android.Manifest;
import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import com.paymentez.android.Paymentez;
import com.paymentez.android.model.Card;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener {
    static final String URL = Constantes.URL; /* Url de de ubicación de servicio */

    private static final int PETICION_PERMISO_LOCALIZACION = 101;  /* Constante para petición de permizos*/
    static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 0;
    int LOGED = 0; /* Numero entero que nos comunica si el usuario esta logeado */
    double LONGITUD, LATITUD ;
    /**
     * variables para ubicación
     */
    static double  VLONGITUD, VLATITUD;
    static  String PLACA;
    static  boolean canTrack = false;
    double LongitudStop, LatitudStop;
    /**
     * variables para ubicación  mapa de valet en la parte de entrega
     */
    private GoogleApiClient apiClient; /* Variale  de google client */
    static String jsondata, IDFACEBOOK, LOGEDCLASSIC = "no";

    static int fragmentToPicker = 0; // entero para saber a  q textview mandar la direccion r ecuperada del texview
    static int fragmentFollowtrack = 0; // entero para saber a  q textview mandar la direccion r ecuperada del texview



    /* jsondata = es un string q recivira el objeto json con toda la información del usuario}
     * IDFACEBOOK = variable q se llenara con el numero de identificación de fb
     * LOGEDCLASSIC = variable que nos dice si el usuario se ha logeado con fb o con la forma clasica*/
    boolean bandview = false;
    static boolean bandMapValet = false;
    static String IDUSER_QUERY, TIPO_USER;
    JSONObject response;
    /* IDUSER_QUERY = ID del usuario en la base de datos para posteriores consultas
     * TIPO_USER = Que usuario es (Cliente, administrador, valet) sirve par acargar lso menus y posteriores consultas
     *JSONObject= Objeto Json que contendra la variable string jsondata  transformada en OBjeto json */

    int posicionado = 0; /* Varailbe q indica q solo se ubique en el mapa la primera vez*/

    /* variables para peticion de la ubicación  */
    LocationRequest locationRequest;
    FusedLocationProviderApi fusedLocationProviderApi;
    clienteMap map;

    private GoogleMap myMap;

    TextView user_name, user_email; /* TextView = para mostrar nombre e imagenes*/
    ImageView user_picture;
    NavigationView navigation_view;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    FrameLayout frame;
    DrawerLayout drawer;

    Bundle bund;

    private GoogleApiClient mGoogleApiClient;
    private int PLACE_PICKER_REQUEST = 1;

    static double PICK_LATITUDE, PICK_LONGITUD;
    static String PICK_DIRECCION;

    int mYear, mMonth, mDay, mHour, mMinute;
    static String Nombre, Mail, Celular, user , pass, UID;
    String placa=""; // Informacion enviada desde firebase por medio de una notificación

        //// VARIABLES APRA ENVIARLAS A CLOD FIRE STORE
    public static DocumentReference mDocRef;
    public static final String LATITUD_KEY = "latitud";
    public static final String LONGITUD_KEY = "longitud";
    public static final String SPEED_KEY = "speed";
    public static final String NOMBRE_KEY = "nombre";
    public static final String HORA_KEY = "hora";
    public static final String FECHA_KEY = "fecha";

    public static final String INIT_KEY = "nombre";
    public static final String PLACA_KEY = "placa";
    public static  String CITA_KEY = "";
    public static final String TAG_GPS = "Enviando..";

    ArrayList<LatLng> list = new ArrayList<>(); /// Lista de todas las  del recorrido para servidor
   static List<LatLng> historial = new ArrayList<>(); /// Lista de todas las  del recorrido



    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String mail) {
        Mail = mail;
    }

    public String getCelular() {
        return Celular;
    }

    public void setCelular(String celular) {
        Celular = celular;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.VmPolicy.Builder newbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(newbuilder.build());

        mDocRef = FirebaseFirestore.getInstance().document("Location/"+1 );

        Paymentez.setEnvironment(Constantes.PAYMENTEZ_IS_TEST_MODE, Constantes.PAYMENTEZ_CLIENT_APP_CODE, Constantes.PAYMENTEZ_CLIENT_APP_KEY);

        //remove();
        //  logout();
/*   Se recive datos del intend para guardarlos datos del perfil*/
        Intent intent = getIntent();
        LOGED = intent.getIntExtra("loged", LOGED);//bandera para saber si guardar preferencias o no
        if (intent.getStringExtra("classic") != null)/* OBTENGO Q TIPO DE LOGEO ES CON FB O CLASICO*/ {
            LOGEDCLASSIC = intent.getStringExtra("classic");
            Log.d("LOGING", "ES CLASICO:: " + LOGEDCLASSIC);
        } else {
            LOGEDCLASSIC = "no";
            Log.d("LOGING", "BANDERA  VALOR: " + LOGEDCLASSIC);
            ////////continuar viendo no se guar q si esta logeado de forma classic
        }

        if (LOGED == 1) { /* CUANDO YA SE A LOGEADO SE GUARDA LA INFORMACIÓN EN LAS PREFECENCIAS */
            jsondata = intent.getStringExtra("jsondata");
            IDUSER_QUERY = intent.getStringExtra("id");
            TIPO_USER = intent.getStringExtra("tipo");
            Celular = intent.getStringExtra("telefono");

            user = intent.getStringExtra("user");// para logear despues de mucho tiempo en firebase
            pass = intent.getStringExtra("pass");/// para loeagaear despues de mucho tiempo a firebase
            UID = intent.getStringExtra("UID");/// indentificador unico de FIrebase



            SavePerfil(jsondata, "yes", IDUSER_QUERY, TIPO_USER, Celular, UID );
            Log.d("LOGING", "ESTA LOGEADO");
            ///GUARDA CREDENCIALES PARA NICIO DE SESIÓN DESPUES DE FIREBASE PARA CAMBIAR LA CONTRASEÑA Y EL CORREO
            SaveCedenciales(user, pass);

/**OBTENER UID**/



        } else {

            jsondata = LoadPerfil(); /* ABRE LA APP DESPUES DE ABERLA CERRA*/
            Log.d("LOGING", jsondata);

        }



        Intro();
        if (bandview) {
            setContentView(R.layout.activity_home);
            bund = savedInstanceState;

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            //getSupportActionBar().setDisplayShowTitleEnabled(false);

            SharedPreferences Preferens = getSharedPreferences("Load_Profile", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = Preferens.edit();
            if (Preferens.getString("dia", "").equals("0") || Preferens.getString("dia", "").equals("")) {
                editor.putString("dia", "0");
                Log.d("NOTIFICACION", " " + Preferens.getString("dia", ""));
                editor.commit();
            }

            fragmentManager = getFragmentManager();
            frame = (FrameLayout) findViewById(R.id.contentFrame);

            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();


            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            setNavigationHeader();
            setUserProfile(jsondata);


            if (TIPO_USER.equals("1")) {
                map = clienteMap.newInstance();
                transaction = fragmentManager.beginTransaction();
                transaction.replace(frame.getId(), map);
                transaction.addToBackStack(null);
                transaction.commit();
                map.getMapAsync(this);
            } else if (TIPO_USER.equals("3")) {
                map = clienteMap.newInstance();
                transaction = fragmentManager.beginTransaction();
                transaction.replace(frame.getId(), map);
                transaction.addToBackStack(null);
                transaction.commit();
                map.getMapAsync(this);
            } else {
                onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_home));
            }
            //Obtener token y enviar al servidor
           // getToken ();

            RegistroToken RT = new RegistroToken();
            SharedPreferences PrefToken = getApplicationContext().getSharedPreferences("Load_Profile", Context.MODE_PRIVATE);
            RT.enviarTokenAlServidor(PrefToken.getString("token", ""),TIPO_USER,IDUSER_QUERY);
            Log.d("FireBse"," "+PrefToken.getString("token", "")+" "+ TIPO_USER+" "+IDUSER_QUERY);

            //CreateUserTeliver(IDUSER_QUERY , TIPO_USER);



        }////Termina la bandera para cargar loso views

        mGoogleApiClient = new GoogleApiClient
                .Builder(getApplicationContext())
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();



    }
    /** OBTENER UID**/



    /*obtener correo, nombre nemail y id para el encabezado */
    public void setUserProfile(String jsondata) {

        try {
            response = new JSONObject(jsondata);
            IDFACEBOOK = response.get("id").toString();
            setMail(response.get("email").toString());
            user_email.setText(getMail());
            setNombre(response.get("name").toString());
            user_name.setText(getNombre());
            Picasso.with(this).load(response.get("picture").toString())
                    .resize(150, 150)
                    .into(user_picture);
            //user_picture.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     /*
        Colocar el encabezado del menu
     */

    public void setNavigationHeader() {

        if (TIPO_USER.equals("1")) {
            navigation_view = (NavigationView) findViewById(R.id.nav_view);
            navigation_view.inflateMenu(R.menu.adminmenu);
            loadview();
        } else if (TIPO_USER.equals("2")) {
            navigation_view = (NavigationView) findViewById(R.id.nav_view);
            navigation_view.inflateMenu(R.menu.activity_home_drawer);
            loadview();
        } else if (TIPO_USER.equals("3")) {
            navigation_view = (NavigationView) findViewById(R.id.nav_view);
            navigation_view.inflateMenu(R.menu.menuvallet);
            loadview();
        }

        user_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeprofile();
            }
        });
        user_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeprofile();
            }
        });
        user_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeprofile();
            }
        });

    }

    public void loadview() {

        View header = LayoutInflater.from(this).inflate(R.layout.nav_header_home, null);
        navigation_view.addHeaderView(header);
        user_name = (TextView) header.findViewById(R.id.username);
        user_picture = (ImageView) header.findViewById(R.id.profile_pic);
        user_email = (TextView) header.findViewById(R.id.email);

    }

    public void Islogin() {
        if (AccessToken.getCurrentAccessToken() == null) {
            if (LOGEDCLASSIC.equals("yes")) {
                bandview = true;
                if (TIPO_USER.equals("2")) {

                    Log.d("LOGING", "CONSULTAS CITAS SEPARADAS POR USUARIO");
                } else {

                    Log.d("LOGING", "USUARIO ADMINBISTRADOR");
                }
            } else {
                Log.d("LOGING", "USUARIO NO LOGEADO");
                Intent intent = new Intent(Home.this, Log_in.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

        } else {
            Log.d("LOGING", "FACEBOOK");
            bandview = true;

        }
    }

    private void Intro() {


        SharedPreferences getPrefs = PreferenceManager
                .getDefaultSharedPreferences(getBaseContext());
        //  Crea el booleano para comprar la primera ejecución
        boolean isFirstStart = getPrefs.getBoolean("firstStart", true);
        //  Se oregunta si el activi nuca antes si ha iniciado
        if (isFirstStart) {
            //inicia  intro
            Intent i = new Intent(Home.this, intro.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            //  Make a new preferences editor
            SharedPreferences.Editor e = getPrefs.edit();
            //  Edit preference to make it false because we don't want this to run again
            e.putBoolean("firstStart", false);
            Log.d("LOGING", "PRIMERA VEZ");
            //  Apply changes
            e.apply();
        } else {
            Log.d("LOGING", "NO PRIMERA VEZ");
            Islogin();
        }


    }

    /*
     * Guarda la informacion del perfil
     */
    public  void SavePerfil(String data, String loged, String IDUSER_QUERY, String TIPO_USER, String Celular, String UID) {

        SharedPreferences Preferens = getSharedPreferences("Load_Profile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = Preferens.edit();
        editor.putString("JSON", data);
        editor.putString("login", loged);
        editor.putString("IDUSER_QUERY", IDUSER_QUERY);
        editor.putString("TIPO_USER", TIPO_USER);
        editor.putString("Celular",Celular);
        editor.putString("UID", UID);
        editor.commit();
    }
    public  void  SaveCedenciales (String user, String pass){
        SharedPreferences Preferens = getSharedPreferences("Load_Profile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = Preferens.edit();
        editor.putString("user", user);
        editor.putString("pass", pass);
        Log.d("pass",""+pass);
        editor.putString("correo", Home.Mail);
        editor.commit();

    }
    public  void  LoadCedenciales (){
        SharedPreferences Preferens = getSharedPreferences("Load_Profile", Context.MODE_PRIVATE);
        user=  Preferens.getString("user", user);
       pass=  Preferens.getString("pass", pass);
        Log.d("pass",""+pass);
      Mail =  Preferens.getString("correo", Home.Mail);

    }

    /**
     * Carga la informacion del perfil
     */
    public String LoadPerfil() {
        SharedPreferences Preferens = getSharedPreferences("Load_Profile", Context.MODE_PRIVATE);
        ///  IsLoged= Preferens.getBoolean("IsLog",false);
        String data = Preferens.getString("JSON", "");
        LOGEDCLASSIC = Preferens.getString("login", "");
        IDUSER_QUERY = Preferens.getString("IDUSER_QUERY", "");
        TIPO_USER = Preferens.getString("TIPO_USER", "");
        Celular = Preferens.getString("Celular","");
        UID = Preferens.getString("UID","");
        LoadCedenciales ();

        Log.d("LOGING", "JSON: " + data + " Es Clasico: " + LOGEDCLASSIC + " ID BASE: " + IDUSER_QUERY + " TIPO USUARIO: " + TIPO_USER + "CELULAR: "+Celular);
        return data;
    }
    private void remove(){
    LoginManager.getInstance().logOut();
    FirebaseAuth.getInstance().signOut();
    SharedPreferences Preferens = getSharedPreferences("Load_Profile", Context.MODE_PRIVATE);
    Preferens.edit().clear().commit();
}
    /*funcion q envia cierra sesion y envia al activity de login */
    public  void logout() {
        LoginManager.getInstance().logOut();
        FirebaseAuth.getInstance().signOut();
       // HyperTrack.stopTracking();
        SavePerfil("", "", "", "", "", "");
        SaveCedenciales ("", "");
        goLoginScreen();
    }
    public   void actualizar(String idCita, String Placa){
        mDocRef = FirebaseFirestore.getInstance().document( "Location"+ "/" + idCita);

    }

    /// regresar a la pantalla de inicio de secion
    private void goLoginScreen() {
        Intent intent = new Intent(this, Log_in.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("id", IDUSER_QUERY);
        startActivity(intent);
    }


    /*Inicia el Picker para seleccionar el lugar donde se recogera el auto */
    public void getPicker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();


        try {
            startActivityForResult(builder.build(Home.this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public void CallToPicker(View view) {

        getPicker();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getFragmentManager().getBackStackEntryCount() <= 2) {
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            Log.e("ID QUERY", " " + IDUSER_QUERY);

            Secondmenu frag = new Secondmenu();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(frame.getId(), frag);
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (id == R.id.nav_Map) {
            // Handle the camera action
            Log.e("ID QUERY", " " + IDUSER_QUERY);

            transaction = fragmentManager.beginTransaction();
            transaction.replace(frame.getId(), map);
            transaction.addToBackStack(null);
            transaction.commit();
            if (TIPO_USER.equals("1")) {
                map = clienteMap.newInstance();
                transaction = fragmentManager.beginTransaction();
                transaction.replace(frame.getId(), map);
                transaction.addToBackStack(null);
                transaction.commit();

                map.getMapAsync(this);
                //getCitasforMap();
            }

        } else if (id == R.id.nav_separar_cita) {

            cita frag = new cita();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(frame.getId(), frag);
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (id == R.id.nav_add_audi) {

            addCar frag = new addCar();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(frame.getId(), frag);
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (id == R.id.nav_my_audis) {

            misAutos frag = new misAutos();
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(findViewById(R.id.contentFrame).getId(), frag);
            ft.addToBackStack(null);
            ft.commit();


        } else if (id == R.id.nav_my_citas){
            Log.d("menu","My citas");
            citasActivas frag = new citasActivas();
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(findViewById(R.id.contentFrame).getId(), frag);
            ft.addToBackStack(null);
            ft.commit();

        } else if (id == R.id.nav_my_seminuevo){

            Seminuevos frag = new Seminuevos();
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(findViewById(R.id.contentFrame).getId(), frag);
            ft.addToBackStack(null);
            ft.commit();

        }else if (id == R.id.nav_send) {

            logout();
        } else if (id == R.id.nav_ad_send) {
            logout();

        }  else if (id == R.id.nav_ad_refound) {
            Refound_cargo frag = new Refound_cargo();

            transaction = fragmentManager.beginTransaction();
            transaction.replace(frame.getId(), frag);
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (id == R.id.nav_ad_modelo) {

            addmodelo frag = new addmodelo();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(frame.getId(), frag);
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (id == R.id.nav_ad_recibir || id == R.id.nav_vl_recibir) {


            estadoReparar frag = new estadoReparar();
            Bundle args = new Bundle();
            args.putString("estado", "recibir");
            frag.setArguments(args);


            transaction = fragmentManager.beginTransaction();
            transaction.replace(findViewById(R.id.contentFrame).getId(), frag);
            transaction.addToBackStack(null);
            transaction.detach(frag).attach(frag).commit();
            //transaction.commit();
            //nav_ad_reparacion
        } else if (id == R.id.nav_ad_reparacion || id == R.id.nav_vl_reparacion) {


            estadoReparar frag = new estadoReparar();
            Bundle args = new Bundle();
            args.putString("estado", "reparacion");
            frag.setArguments(args);

            transaction = fragmentManager.beginTransaction();
            transaction.replace(findViewById(R.id.contentFrame).getId(), frag);
            transaction.addToBackStack(null);
           // transaction.detach(frag).attach(frag).commit();

            transaction.commit();
            //nav_ad_reparacion
        } else if (id == R.id.nav_ad_entregado || id == R.id.nav_vl_entregado) {

            estadoReparar frag = new estadoReparar();
            Bundle args = new Bundle();
            args.putString("estado", "entregado");
            frag.setArguments(args);

            transaction = fragmentManager.beginTransaction();
            transaction.replace(findViewById(R.id.contentFrame).getId(), frag);
            transaction.addToBackStack(null);
            //transaction.detach(frag).attach(frag).commit();

            transaction.commit();
            //nav_ad_reparacion
        } else if (id == R.id.nav_ad_finalizar || id == R.id.nav_vl_finalizado) {
            //Este es para el fragmento



            estadoReparar frag = new estadoReparar();
            Bundle args = new Bundle();
            args.putString("estado", "finalizado");
            frag.setArguments(args);

            transaction = fragmentManager.beginTransaction();
            transaction.replace(findViewById(R.id.contentFrame).getId(), frag);
            transaction.addToBackStack(null);
            //transaction.detach(frag).attach(frag).commit();

            transaction.commit();

            //nav_ad_reparacion

            /*Intent intent = new Intent(Home.this,LogTrack.class);

            intent.putExtra("Nombre",Home.Nombre);
            intent.putExtra ("telefono", Home.Celular);
            startActivity(intent);*/
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void changeprofile() {
        userProfile frag = new userProfile();
        Bundle args = new Bundle();
        args.putString("userProfile", jsondata);
        frag.setArguments(args);
        transaction = fragmentManager.beginTransaction();
        transaction.replace(frame.getId(), frag);
        transaction.addToBackStack(null);
        transaction.commit();
        drawer.closeDrawer(GravityCompat.START);
    }

    public void getCitasforMapValet() {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = URL + "citas-valet.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            parseJson(oJson);
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("estado", "recibir");
                parameters.put("id_valet", IDUSER_QUERY);
                return parameters;
            }
        });

    }

    public void getCitasforMap() {

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        ///url provisional donde esta el servicio web local ////
        // http://autoimportscc.com/SOCIALHEART/Log-in.php

        String url = URL + "citas-estado.php";

        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            parseJson(oJson);
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("estado", "recibir");
                return parameters;
            }
        });
    }

    private List<citaClas> parseJson(JSONObject jsonObject) {
        List<citaClas> citas = new ArrayList<citaClas>();
        JSONArray jsonArray;
        int estado = 0;
        try {
            estado = jsonObject.getInt("estado");
            switch (estado) {
                case 1:
                    String mensaje = jsonObject.getString("mensaje");
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case 2:
                    jsonArray = jsonObject.getJSONArray("citas");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject objeto = jsonArray.getJSONObject(i);
                            //if(!(objeto.getString("longitud").equals(""))){
                            LatLng ubi = new LatLng(objeto.getDouble("latitud"), objeto.getDouble("longitud"));
                            myMap.addMarker(new MarkerOptions().position(ubi).title("PLACA: " + objeto.getString("auto")).icon(BitmapDescriptorFactory.fromResource(R.drawable.track_auto)));
                            Log.d("GPS", " " + objeto.getDouble("longitud") + " " + objeto.getDouble("latitud") + " " + objeto.getString("auto"));
                            // }
                        } catch (JSONException e) {
                            Log.e("JSON ENCODE>>>>>>>", "Error de parsing: " + e.getMessage());
                        }
                    }
                    break;
            }
        } catch (JSONException e) {
            Log.e("JSON ENCODE>>>>>>>", "Error de parsing: " + e.getMessage());
        }
        //getCitasforMap(citas);
        return citas;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        getLocation();

        LatLng ubi = new LatLng(LATITUD, LONGITUD);

        CameraPosition pos = CameraPosition.builder().target(ubi).zoom(13).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos));

        myMap = googleMap;

        if (TIPO_USER.equals("3")) {
            getCitasforMapValet();

        } else {

            getCitasforMap();
        }


        //apiClient.disconnect();
    }

    public void getLocation() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        fusedLocationProviderApi = LocationServices.FusedLocationApi;
        apiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        if (apiClient != null) {
            apiClient.connect();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Log.e("GPS", "PERMISOS ");
        if (requestCode == PETICION_PERMISO_LOCALIZACION) {
            //Log.d("GPS",".........ACEPTO....PERMISOS....");
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                //Permiso concedido
                //Log.d("GPS",".........ACEPTO....PERMISOS....");
                @SuppressWarnings("MissingPermission")
                Location lastLocation =
                        LocationServices.FusedLocationApi.getLastLocation(apiClient);
                String locationProviders = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                if (locationProviders == null || locationProviders.equals("")) {

                    dialogGPSupdate dialog = new dialogGPSupdate();
                    dialog.show(getFragmentManager(), "update");
                    //Log.d("GPS",".........ACEPTO....PERMISOS....");
                } else {
                    Toast.makeText(getApplicationContext(), "location :" + lastLocation.getLatitude() + " , " + lastLocation.getLongitude(), Toast.LENGTH_SHORT).show();
                    LATITUD  = lastLocation.getLatitude();
                    LONGITUD  = lastLocation.getLongitude();
                    Log.d("GPS", ".........ACEPTO....PERMISOS....");
                }
                Log.d("GPS", ".........ACEPTO....PERMISOS....");
            }
        }
    }

    public void setMapPosition() {
        if (posicionado == 0) {
            LatLng ubi = new LatLng(LATITUD, LONGITUD);
            CameraPosition pos = CameraPosition.builder().target(ubi).zoom(13).build();
            myMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos));
            posicionado = 1;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE, Manifest.permission.INTERNET},
                    PETICION_PERMISO_LOCALIZACION);

        } else {


            fusedLocationProviderApi.requestLocationUpdates(apiClient, locationRequest, this);
            String locationProviders = "";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
                //Equal or higher than API 19/KitKat
                    locationProviders =Settings.Secure.getString(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
            }else{
                //Lower than API 19
                locationProviders =Settings.Secure.getString(this.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            }
            locationProviders =Settings.Secure.getString(this.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

            if (locationProviders == null || locationProviders.equals("")) {

                dialogGPSupdate dialog = new dialogGPSupdate();
                dialog.show(getFragmentManager(), "update");
            } else {
                Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);

               /* zentrum.setLatitude(-2.169143);
                zentrum.setLongitude(-79.9194962);*/
                try {
                    LONGITUD = lastLocation.getLongitude();
                    LATITUD = lastLocation.getLatitude();

                }catch (Exception error){
                    LONGITUD = -79.9194962;
                    LATITUD = -2.169143;
                    Log.d("GPS", "Ubicación por defecto");
                }
                setMapPosition();
            }
        }
    }
    public static int getLocationMode(Context context) {
        int locationMode = 0;
        String locationProviders;
        //Settings.Secure.LOCATION_PROVIDERS_ALLOWED

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }


        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

            if (TextUtils.isEmpty(locationProviders)) {
                locationMode = Settings.Secure.LOCATION_MODE_OFF;
            }
            else if (locationProviders.contains(LocationManager.GPS_PROVIDER) && locationProviders.contains(LocationManager.NETWORK_PROVIDER)) {
                locationMode = Settings.Secure.LOCATION_MODE_HIGH_ACCURACY;
            }
            else if (locationProviders.contains(LocationManager.GPS_PROVIDER)) {
                locationMode = Settings.Secure.LOCATION_MODE_SENSORS_ONLY;
            }
            else if (locationProviders.contains(LocationManager.NETWORK_PROVIDER)) {
                locationMode = Settings.Secure.LOCATION_MODE_BATTERY_SAVING;
            }

        }

        return locationMode;
    }
    @Override
    protected void onStart() {
        super.onStart();
        //Para Place PICKER
        mGoogleApiClient.connect();

        mDocRef.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent( DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                if(documentSnapshot.exists()){


                    if(documentSnapshot.getReference().getPath().equals("Location/1")){
                       String txtModo="modo de seguimiento desactivado";
                       Log.d("GPS","El modo seguimiento esta desactivado");
                       canTrack=false;

                       // TrackRecibir.txtEstado.setText("Estado Inactivo");

                    }else {
                        Log.d("GPS","...");

                        //TrackRecibir.txtEstado.setText("Estado Activo");
                    }
                   // Log.d("GPS", "Nombre"+documentSnapshot.getString(NOMBRE_KEY)+" "+documentSnapshot.getReference().getPath());

                }
            }
        });

    }

    @Override
    protected void onStop() {
        //Para Place PICKER
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        /////////LEAR AQUI LAS INFO GUARDAD DE UBICACIÓN
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // Snackbar.make("", connectionResult.getErrorMessage() + "", Snackbar.LENGTH_LONG).show();
        //Para Place PICKER
        Toast.makeText(getApplicationContext(), "Error en la conexcion con cliente", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH) + 1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        int weekDay = c.get(Calendar.DAY_OF_WEEK);
        int mHora = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        LONGITUD = location.getLongitude();
        LATITUD = location.getLatitude();
        String SPEED = ""+location.getSpeed();
        setMapPosition();
        Log.d("GPS", "TEST "+ location.getLatitude()+" : "+location.getLongitude() +" PLACA: "+PLACA+ " CAN TRACK?: "+canTrack+ " Hora: "+ mHora+":"+mMinute + " FECHA:"+mDay+"-"+mMonth+"-"+mYear);

        if(canTrack){
            //Toast.makeText(getApplicationContext(), location.getLatitude()+" : "+location.getLongitude(), Toast.LENGTH_LONG).show();
            list.add(new LatLng(location.getLongitude(),location.getLatitude()));

            sendFireBase(""+location.getLatitude(), ""+location.getLongitude(), SPEED, ""+PLACA, ""+getNombre(), ""+ mHora+":"+mMinute, ""+mDay+"-"+mMonth+"-"+mYear);
            LongitudStop = location.getLongitude();
            LatitudStop= location.getLatitude();
            Log.d("GPS", "Enviando nueva ubicación "+ location.getLatitude()+" "+location.getLongitude());
            if(fragmentFollowtrack==1){
                TrackRecibir.txtEstado.setBackgroundColor(getResources().getColor(R.color.GPSactive));
                TrackRecibir.txtEstado.setText("Modo seguimiento activo \n Evite cerrar la aplicación o se detendrá el modo seguimiento");
            }


        }else{
            sendFireBase(""+LatitudStop, ""+LongitudStop, SPEED,PLACA, "...", "00:00", "dd-mm-yyyy ");
            if(fragmentFollowtrack==1) {
                TrackRecibir.txtEstado.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                TrackRecibir.txtEstado.setText(" Modo seguimiento desactivado \n Para activar: Forzar actualización de ubicación");
            }

        }
    }

    public void sendFireBase (final String latitud, final String longitud, String speed , final String Placa, final String Nombre, final String Hora, final  String Fecha){
       if(latitud.isEmpty() || longitud.isEmpty()){return;}
        Map<String, Object> dataToSave = new HashMap <String, Object>();
        dataToSave.put(LATITUD_KEY, latitud);
        dataToSave.put(LONGITUD_KEY, longitud);
        dataToSave.put(SPEED_KEY, speed);
        dataToSave.put(PLACA_KEY, Placa);
        dataToSave.put(NOMBRE_KEY, Nombre);
        dataToSave.put(HORA_KEY, Hora);
        dataToSave.put(FECHA_KEY, Fecha);

        //dataToSave.put(HISTORIAL_KEY, historial);

        mDocRef.set(dataToSave).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG_GPS, "Nombre Valet "+Nombre);
                Log.d(TAG_GPS, "Latitud "+latitud);
                Log.d(TAG_GPS, "Longitud "+longitud);
                Log.d(TAG_GPS, "Placa "+Placa);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.w(TAG_GPS, "Error al guardar documento "+e.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)  {
        Log.d("UID", "Tarjeta seleccionada");

        ///pace picker
        Log.d("Activty ", "onActivityResult");
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                PICK_LATITUDE = place.getLatLng().latitude;

                PICK_LONGITUD = place.getLatLng().longitude;
                //   PICK_DIRECCION = String.format("%s", place.getName())+" "+String.format("%s", place.getAddress());

                PICK_DIRECCION = String.format("%s", place.getAddress());
                if (fragmentToPicker == 1) {
                    cita.Calle1.setText(PICK_DIRECCION);
                } else if (fragmentToPicker == 2) {
                    SingleCita.Direccion.setText(PICK_DIRECCION);
                }

                //cita.zona.setText(PICK_DIRECCION);
                Log.d("Activty ", PICK_DIRECCION + " " + PICK_LATITUDE + " " + PICK_LONGITUD);

            }
        }


    }

    public void fecha(View view) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        Log.d("DateClic", " "+c.get(Calendar.DAY_OF_WEEK));
        if(c.get(Calendar.DAY_OF_WEEK)==6){
        if (Calendar.getInstance().getTime().getHours()>12){
            c.add(c.DAY_OF_MONTH, +5);

        }else{
            c.add(c.DAY_OF_MONTH, +4);

        }

        }else{
            Log.d("DateClic", "2");
            c.add(c.DAY_OF_MONTH, +2);
        }




        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Date date = new Date(year, monthOfYear, dayOfMonth - 1);

                        if (date.getDay() == 6 || date.getDay() == 0) {
                            Toast.makeText(getApplicationContext(), "Los Sábados y Domingos  no se brinda este servicio ", Toast.LENGTH_LONG).show();
                            cita.isweakend = false;
                        } else {
                            Log.d("Fecha", dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            String fechaCita =    year+"-"+(monthOfYear + 1) + "-"+dayOfMonth;
                            conulta_hora_x_cita (fechaCita);
                            cita.txtfecha.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            //cita.txtfecha.setBackgroundColor(Color.red(5));
                            cita.dia = "" + dayOfMonth;
                            cita.año = "" + year;
                            cita.mes = "" + (monthOfYear + 1);
                            cita.isweakend = true;

                        }


                    }
                }, mYear, mMonth, mDay);
        ///datePickerDialog.getDatePicker().se
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.show();

    }
    // Fecha para saber q horario de citas esta dosponible fin de Función
    //NO urlizada hasta hora

    public void conulta_hora_x_cita (final String fecha){
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = URL + "consultar-cupo-cita.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("data",response);
                            JSONObject oJson = new JSONObject(response);
                            Get_Hora_cita(oJson);
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tard "+ e.getMessage() , Toast.LENGTH_LONG).show();
                            Log.e("data", "Error de parsing: "+ e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("fecha", fecha);
                return parameters;
            }
        });


    }

    private void Get_Hora_cita ( JSONObject jsonObject){
        cita.btn1.setEnabled(true);
        cita.btn2.setEnabled(true);
        cita.btn3.setEnabled(true);
        cita.btn4.setEnabled(true);

        cita.btn1.setBackgroundDrawable(getDrawable(R.drawable.borderbottom));
        cita.btn2.setBackgroundDrawable(getDrawable(R.drawable.borderbottom));
        cita.btn3.setBackgroundDrawable(getDrawable(R.drawable.borderbottom));
        cita.btn4.setBackgroundDrawable(getDrawable(R.drawable.borderbottom));
        JSONArray jsonArray;
        int estado =0;
        try {
            estado = jsonObject.getInt("estado");
            Log.d("data", " Este es el estado:"+estado);
            switch (estado) {
                case 0:
                    cita.btn1.setEnabled(true);
                    cita.btn2.setEnabled(true);
                    cita.btn3.setEnabled(true);
                    cita.btn4.setEnabled(true);
      //              cita.btn5.setEnabled(true);
    //                cita.btn6.setEnabled(true);
                    break;
                case 1:
                    cita.btn1.setEnabled(true);
                    cita.btn2.setEnabled(true);
                    cita.btn3.setEnabled(true);
                    cita.btn4.setEnabled(true);
//                    cita.btn5.setEnabled(true);
  //                  cita.btn6.setEnabled(true);
                    String mensaje = jsonObject.getString("mensaje");
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case 2:
                    Log.d("data", " Operación dentro del estado:"+estado);
                    jsonArray = jsonObject.getJSONArray("horas");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject objeto = jsonArray.getJSONObject(i);
                            Log.d("data", ""+objeto.getInt("hora"));
                            int idHora = objeto.getInt("hora");
                            if(idHora==1){
                                cita.btn1.setEnabled(false);
                                cita.btn1.setBackgroundColor(Color.GRAY);




                                Log.d("data", "Cambia de color");
                            }else if(idHora==2){

                                cita.btn2.setEnabled(false);
                                cita.btn2.setBackgroundColor(Color.GRAY);


                            } else if (idHora==3){

                                cita.btn3.setEnabled(false);
                                cita.btn3.setBackgroundColor(Color.GRAY);


                            } else if (idHora==4){

                                cita.btn4.setEnabled(false);
                                cita.btn4.setBackgroundColor(Color.GRAY);


                            } else if (idHora == 5){

                                cita.btn5.setEnabled(false);
                                cita.btn5.setBackgroundColor(Color.GRAY);
                            } else if (idHora == 6){


                                cita.btn6.setEnabled(false);

                                cita.btn6.setBackgroundColor(Color.GRAY);
                            }

                        } catch (JSONException e) {
                            Log.e("data", "Error de parsing: " + e.getMessage());
                        }
                        Log.d("data", " Dentro del for de arreglo de horas despues de cambiar de color " );
                    }
                    break;
            }

        }catch (JSONException d){
            Log.e("JSON ENCODE>>>>>>>", "NO hay nada o mala formato: " + d.getMessage());
        }
    }

    public  void fechaSDir(View view) {
        final Calendar c = Calendar.getInstance();
        int  mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH)+1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        int dWeek = c.get(Calendar.DAY_OF_WEEK);
        int hOfDay = c.get(Calendar.HOUR_OF_DAY);
        int Hora =hOfDay;

        String fechaCita = mYear + "-" + mMonth + "-" + mDay;
        Log.d("hora", "dia "+dWeek+ " hora"+hOfDay);
       /* if(dWeek==6){
            //SingleCita.btn1
        }else if (dWeek==7 || dWeek ==1){}*/
       // conulta_hora_x_cita_single (fechaCita);


        SingleCita.eshoyContent.setVisibility(View.VISIBLE);
        SingleCita.HorasBotones.setVisibility(View.VISIBLE);
        if (dWeek==7 || dWeek ==1){
            Log.d("hora","Fin de semaa");

            SingleCita.fechaSingle.setVisibility(View.VISIBLE);
        }else {
            Log.d("hora","No finde semana "+Hora);
            conulta_hora_x_cita_single_HOY(fechaCita, Hora);


        }


    }

    public void fechaS(View view) {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH)+1;
               c.set(mYear,mMonth,mDay);
        //c.add(c.DAY_OF_MONTH, +0);



        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        Date date = new Date(year, monthOfYear, dayOfMonth - 1);

                        if (date.getDay() == 6 || date.getDay() == 0) {
                            Toast.makeText(getApplicationContext(), "Los Sábados y Domingos  no se brinda este servicio ", Toast.LENGTH_LONG).show();
                            cita.isweakend = false;
                        } else {
                            Log.d("Fecha", dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            String fechaCita = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            conulta_hora_x_cita_single (fechaCita);
                            SingleCita.fechaSingle.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            SingleCita.fechaEnviar = year +"-"+(monthOfYear + 1) +"-"+dayOfMonth;
                            mHour = c.get(Calendar.HOUR_OF_DAY);
                            Log.d ("Fecha", (mMonth+1)+" ::: "+(monthOfYear + 1)+"  "+year+" ::: "+mYear+" ::: "+mHour);

                            SingleCita.hora_Cita= 0;//reninicar para que tenga que escoger una hora
                        }


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.show();

    }

    public void conulta_hora_x_cita_single (final String fecha){
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = URL + "consultar-cupo-cita.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("data",response);
                            JSONObject oJson = new JSONObject(response);
                            Get_Hora_cita_single(oJson);
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tard "+ e.getMessage() , Toast.LENGTH_LONG).show();
                            Log.e("data", "Error de parsing: "+ e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("fecha", fecha);
                return parameters;
            }
        });


    }

    private void Get_Hora_cita_single_HOY ( JSONObject jsonObject, int Hora) {
        SingleCita.btn1.setEnabled(true);
        SingleCita.btn2.setEnabled(true);
        SingleCita.btn3.setEnabled(true);
        SingleCita.btn4.setEnabled(true);

        SingleCita.btn1.setBackground(getResources().getDrawable(R.drawable.borderbottom));
        SingleCita.btn2.setBackground(getResources().getDrawable(R.drawable.borderbottom));
        SingleCita.btn3.setBackground(getResources().getDrawable(R.drawable.borderbottom));
        SingleCita.btn4.setBackground(getResources().getDrawable(R.drawable.borderbottom));

        JSONArray jsonArray;
        int estado =0;
        try {
            estado = jsonObject.getInt("estado");
            Log.d("hora", " Este es el estado:"+estado);
            switch (estado) {
                case 0:
                    if(fragmentToPicker==2) {
                        SingleCita.btn1.setEnabled(true);
                        SingleCita.btn2.setEnabled(true);
                        SingleCita.btn3.setEnabled(true);
                        SingleCita.btn4.setEnabled(true);
                        // SingleCita.btn5.setEnabled(true);
                        //  SingleCita.btn6.setEnabled(true);
                        //setBackground(getResources().getDrawable(R.drawable.borderbottom));
                    }
                    break;
                case 1:
                    if(fragmentToPicker==2){
                        SingleCita.btn1.setEnabled(true);
                        SingleCita.btn2.setEnabled(true);
                        SingleCita.btn3.setEnabled(true);
                        SingleCita.btn4.setEnabled(true);
                        //SingleCita.btn5.setEnabled(true);
                        //SingleCita.btn6.setEnabled(true);
                    }
                    String mensaje = jsonObject.getString("mensaje");
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case 2:
                    Log.d("hora", " Operación dentro del estado:"+estado);
                    jsonArray = jsonObject.getJSONArray("horas");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject objeto = jsonArray.getJSONObject(i);
                            Log.d("hora", ""+objeto.getInt("hora"));
                            int idHora = objeto.getInt("hora");
                            if(idHora==1){
                                if(fragmentToPicker==2) {
                                    SingleCita.btn1.setEnabled(false);
                                    SingleCita.btn1.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
                                }
                                Log.d("hora", "Cambia de color");
                            }else if(idHora==2){
                                if(fragmentToPicker==2) {
                                    SingleCita.btn2.setEnabled(false);
                                    SingleCita.btn2.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));

                                }


                            } else if (idHora==3){
                                if(fragmentToPicker==2) {
                                    SingleCita.btn3.setEnabled(false);
                                    SingleCita.btn3.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
                                }

                            } else if (idHora==4){
                                if(fragmentToPicker==2) {
                                    SingleCita.btn4.setEnabled(false);
                                    SingleCita.btn4.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
                                }


                            } else if (idHora == 5){
                                if(fragmentToPicker==2) {
                                    SingleCita.btn5.setEnabled(false);
                                    SingleCita.btn5.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
                                }


                            } else if (idHora == 6){
                                if(fragmentToPicker==2) {
                                    SingleCita.btn6.setEnabled(false);
                                    SingleCita.btn6.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
                                }


                            }

                        } catch (JSONException e) {
                            Log.e("data", "Error de parsing: " + e.getMessage());
                        }
                        Log.d("data", " Dentro del for de arreglo de horas despues de cambiar de color " );
                    }
                    break;
            }

        }catch (JSONException d){
            Log.e("JSON ENCODE>>>>>>>", "NO hay nada o mala formato: " + d.getMessage());
        }
        bloquearHoras(Hora);

    }

    private void Get_Hora_cita_single ( JSONObject jsonObject) {
        SingleCita.btn1.setEnabled(true);
        SingleCita.btn2.setEnabled(true);
        SingleCita.btn3.setEnabled(true);
        SingleCita.btn4.setEnabled(true);

        SingleCita.btn1.setBackground(getResources().getDrawable(R.drawable.borderbottom));
        SingleCita.btn2.setBackground(getResources().getDrawable(R.drawable.borderbottom));
        SingleCita.btn3.setBackground(getResources().getDrawable(R.drawable.borderbottom));
        SingleCita.btn4.setBackground(getResources().getDrawable(R.drawable.borderbottom));

        JSONArray jsonArray;
        int estado =0;
        try {
            estado = jsonObject.getInt("estado");
            Log.d("hora", " Este es el estado:"+estado);
            switch (estado) {
                case 0:
                    if(fragmentToPicker==2) {
                        SingleCita.btn1.setEnabled(true);
                        SingleCita.btn2.setEnabled(true);
                        SingleCita.btn3.setEnabled(true);
                        SingleCita.btn4.setEnabled(true);
                       // SingleCita.btn5.setEnabled(true);
                      //  SingleCita.btn6.setEnabled(true);
                        //setBackground(getResources().getDrawable(R.drawable.borderbottom));
                    }
                    break;
                case 1:
                    if(fragmentToPicker==2){
                        SingleCita.btn1.setEnabled(true);
                        SingleCita.btn2.setEnabled(true);
                        SingleCita.btn3.setEnabled(true);
                        SingleCita.btn4.setEnabled(true);
                        //SingleCita.btn5.setEnabled(true);
                        //SingleCita.btn6.setEnabled(true);
                    }
                    String mensaje = jsonObject.getString("mensaje");
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case 2:
                    Log.d("hora", " Operación dentro del estado:"+estado);
                    jsonArray = jsonObject.getJSONArray("horas");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject objeto = jsonArray.getJSONObject(i);
                            Log.d("hora", ""+objeto.getInt("hora"));
                            int idHora = objeto.getInt("hora");
                            if(idHora==1){
                                if(fragmentToPicker==2) {
                                    SingleCita.btn1.setEnabled(false);
                                    SingleCita.btn1.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
                                }
                                    Log.d("hora", "Cambia de color");
                            }else if(idHora==2){
                                if(fragmentToPicker==2) {
                                    SingleCita.btn2.setEnabled(false);
                                    SingleCita.btn2.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));

                                }


                            } else if (idHora==3){
                                if(fragmentToPicker==2) {
                                    SingleCita.btn3.setEnabled(false);
                                    SingleCita.btn3.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
                                }

                            } else if (idHora==4){
                                if(fragmentToPicker==2) {
                                    SingleCita.btn4.setEnabled(false);
                                    SingleCita.btn4.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
                                }


                            } else if (idHora == 5){
                                if(fragmentToPicker==2) {
                                    SingleCita.btn5.setEnabled(false);
                                    SingleCita.btn5.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
                                }


                            } else if (idHora == 6){
                                if(fragmentToPicker==2) {
                                    SingleCita.btn6.setEnabled(false);
                                    SingleCita.btn6.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
                                }


                            }

                        } catch (JSONException e) {
                            Log.e("data", "Error de parsing: " + e.getMessage());
                        }
                        Log.d("data", " Dentro del for de arreglo de horas despues de cambiar de color " );
                    }
                    break;
            }

        }catch (JSONException d){
            Log.e("JSON ENCODE>>>>>>>", "NO hay nada o mala formato: " + d.getMessage());
        }
    }

    public void conulta_hora_x_cita_single_HOY (final String fecha, final int hora){
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url = URL + "consultar-cupo-cita.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("data",response);
                            JSONObject oJson = new JSONObject(response);
                            Get_Hora_cita_single_HOY(oJson, hora);
                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tard "+ e.getMessage() , Toast.LENGTH_LONG).show();
                            Log.e("data", "Error de parsing: "+ e.getMessage());
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("fecha", fecha);
                return parameters;
            }
        });


    }

    //            bloquearHoras (Hora);
    public void hora(View view) {

        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        Log.d("Fecha", hourOfDay + ":" + minute);
                        cita.txthora.setText(hourOfDay + ":" + minute);
                        cita.hora = "" + hourOfDay;
                        cita.minutos = "" + minute;
                    }
                }, mHour, mMinute, true);
        timePickerDialog.show();
    }
    public void bloquearHoras (int Hora){
        if(Hora>= 14){
            Log.d("hora","Mayor a 14");
            SingleCita.esHoy.setChecked(true);
            SingleCita.esHoy.setEnabled(false);
            SingleCita.fechaSingle.setVisibility(View.VISIBLE);
        }else if(Hora>=12){
            Log.d("hora","Mayor a 12");
            SingleCita.btn3.setEnabled(false);
            SingleCita.btn3.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
            SingleCita.btn2.setEnabled(false);
            SingleCita.btn2.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
        }else if(Hora>=10){
            Log.d("hora","Mayor a 10");
            SingleCita.btn2.setEnabled(false);
            SingleCita.btn2.setBackgroundColor(getResources().getColor(R.color.negro_semitransparente));
        }else{
            Log.d("hora","asdasd");
        }
    }

}