package com.creativita.audi.tourapp;

import android.*;
import android.Manifest;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.firestore.FirebaseFirestore;


import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class citasPorEstado extends Fragment implements OnMapReadyCallback {

    private static final String URL_BASE = Constantes.URL;
    private RequestQueue queue;
    ListView lstCitas;
    citasAdapter adapter;
    citasAdapterValet adapterValet;
    Context context;
    String estado;
    int estadocitaapp= 2;
    TextView cabecera;
    String dialogMessage, entregadoMessage, dialogMessageFecha;
    String siguiente;
    clienteMap map;
    private GoogleMap myMap;
    FragmentTransaction transaction;
    FragmentManager fragmentManager;
    FrameLayout frame;
    double Latitud, Longitud;
    citaClas citashow;
    boolean DeleteOfListe=true;
    String Telefono;
    int permissionCheck;
    private static final  int REQUEST_CODE_PHONE=0;

    int idCitaOnClick = 0; //Este es el entero q se envia al la actividad de mapa q va sevir como LookupID

    String telefonoCliente ="";

    /* Cuando SE AGREGE OTRO ESTADO A LAS CITAS */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            estado = this.getArguments().getString("estado");
            if (estado.equals("recibir")) {
                siguiente = "reparacion";
            } else if (estado.equals("reparacion")) {
                siguiente = "entregado";
            }else if (estado.equals("entregado")) {
                siguiente = "finalizado";
            }
            Log.d("menu",estado);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_citas_por_estado, container, false);

    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        Log.d("menu", "Nueva");

        lstCitas = (ListView) getActivity().findViewById(R.id.lstCitas);

        context = getActivity().getApplicationContext();
        fragmentManager = getFragmentManager();
        frame =(FrameLayout) getActivity().findViewById(R.id.contentFrame);

        queue = Volley.newRequestQueue(context);

        cabecera = (TextView) getActivity().findViewById(R.id.lblCitas);



        dialogMessage = "¿Seguro qué desea cambiar el estado de la cita a " + siguiente + "? o puede presionar llamar para contactar al cliente";
        entregadoMessage = "Está cita ya se finalizó";
        dialogMessageFecha= "La fecha de su cita no coincide con la fecha actual ¿DESEA CONTINUAR?";



        if (estado.equals("recibir")) {
            cabecera.setText(cabecera.getText() + " POR  RECIBIR" );
            Log.d("menu", " se puso cabecera" );

        } else if (estado.equals("reparacion")) {
            cabecera.setText(cabecera.getText() + " POR  REPARAR" );
        }else if (estado.equals("entregado")) {
            cabecera.setText(cabecera.getText() + " POR  ENTEGAR" );
        }else if (estado.equals("finalizado")) {
            cabecera.setText(cabecera.getText() + " FINALIZADAS" );
        }

        if (Home.TIPO_USER.equals("3")) {
            /**
             *
             * PARA EL USUARIO DE VALLET *
             * SE DEBEASIGNAR EL VALOR PARA DE LONGITUD Y LATITUD PARA QUE EL USUARIO VALER
             * PUEDA VER LA DIRECCIÓN Y LUGAR DE ENTREGA.
             *
             *
             * */
            adapterValet = new citasAdapterValet(context, estado);
            lstCitas.setAdapter(adapterValet);
            lstCitas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    final Calendar c = Calendar.getInstance();
                    int mMonth = c.get(Calendar.MONTH)+1;
                    int mDay = c.get(Calendar.DAY_OF_MONTH);
                    int  mYear =  c.get(Calendar.YEAR);

                    String fecha =mYear+"-"+mMonth+"-"+mDay;
                    Log.d("fecha", fecha);

                    citaClas cita = adapterValet.getItem(i);
                    if (estado.equals("recibir")) {
                        //Telefono = cita.getTelefono();
                        Log.d("NO ES FECHA","fecha cita:"+cita.getFecha()+" fecha actual: "+fecha);

                        String string = cita.getFecha();
                        String[] parts = string.split(" hora");
                        String HocaCita = parts[0]; //fechas sin hora
                        HocaCita.replace(" ", "");
                        Log.d("NO ES FECHA",HocaCita+" "+fecha);


                        if(HocaCita.equals(fecha)){
                            Log.d("ES FECHA", "CONTINUAR NORMALMENTE");

                            cargarMapaCita(""+cita.getCitaid(), "0"+cita.getTelefono(),cita.getLatitud(),cita.getLongitud(),cita.getAuto());

                        }else {

                            telefonoCliente = "0"+cita.getTelefono();
                            FechaDiferente(i, view.getContext(), cita.getCitaid(), cita.getTelefono(), cita.getLatitud(),
                                    cita.getLongitud(), cita.getAuto()).show();
                            Log.d("NO ES FECHA", "HOY NO ES LA FECHA DE SUS CITA ESTA SEGURO DE CONTINUAR!!");
                        }

                    } else if (estado.equals("reparacion")){
                        dialogestados( view.getContext()).show();
                    } else if (estado.equals("entregado")){


                        cargarmapa(cita);
                    }else if (estado.equals("finalizado")){
                        dialogEntregado(view.getContext()).show();

                    }
                }
            });


        } else {
            /**
             * PARA EL USUARIO ADMINISTRADOR *
             * */
            Log.d("menu", "Funcion cargar admin");

            consultacitasADMIN();

            cabecera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
/**
 * @estadicutaapp  es el estado de la cita dentro del menu de administrador para saber si esta o no estaasignado**/
                if (estado.equals("recibir")){
                if(estadocitaapp==0){
                    cabecera.setText(" CITAS POR  RECIBIR:   ASIGNADAS  ");
                    estadocitaapp=1;
                    consultacitasADMIN();

                }else if(estadocitaapp==1){
                    cabecera.setText(" CITAS  POR  RECIBIR:  TODAS  ");
                    estadocitaapp=2;
                    consultacitasADMIN();

                }else if(estadocitaapp==2){
                    cabecera.setText("CITAS POR  RECIBIR: NO ASIGNADAS  ");
                    estadocitaapp=0;
                    consultacitasADMIN();

                }

                }}
            });

        }


    }


    public void consultacitasADMIN(){
        adapter = new citasAdapter(context, estado, estadocitaapp);
        lstCitas.setAdapter(adapter);
        lstCitas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                citaClas cita = adapter.getItem(i);
                if (estado.equals("recibir")){
                    updatevalet(i, cita.getCitaid());

                }else if (estado.equals("reparacion")) {
                    /***
                     * Se envia aframento para llna lso datos de que fue loque se hizo a la cita
                     * para que el usuario tenga registro de que tuvo de mantenimiento
                     ***/
                    UpdateCitaAdmin frag = new UpdateCitaAdmin();
                    Bundle args = new Bundle();
                    args.putInt("id", cita.getCitaid());
                    args.putString("lugar", cita.getLugarEntrega());
                    args.putString("auto", cita.getAuto());
                    args.putString("fecha", cita.getFecha());
                    args.putString("estado", cita.getEstado());
                    frag.setArguments(args);
                    transaction =  fragmentManager.beginTransaction();
                    transaction.replace(frame.getId(),frag);
                    transaction.addToBackStack(null);
                    transaction.commit();

                } else if (estado.equals("entregado"))  {
                    updatevalet(i, cita.getCitaid());
                }else if (estado.equals("finalizado"))  {
                    dialogEntregado(view.getContext()).show();
                }
            }
        });

    }

    private void updatevalet(final int i, final int cita) {
       dialogUpdateSetValet dialog = new dialogUpdateSetValet();

        Bundle args = new Bundle();
        args.putString("idcita", ""+cita);
        dialog.setArguments(args);
        dialog.show(getFragmentManager(),"update");

    }




    public AlertDialog dialogEntregado(Context cont) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cont);

        builder.setTitle("Cita entregada")
                .setMessage(entregadoMessage)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();
    }

    public AlertDialog dialogestados(Context cont) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cont);

        builder.setTitle("Cita")
                .setMessage("Espera  a que se asigne un lugar de entrega")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();
    }

    public AlertDialog dialogNoUbicacion(Context cont) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cont);

        builder.setTitle("Sin ubicación")
                .setMessage("Espera  a que se asigne un lugar de entrega")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();

    }

    private void onUserLoginSuccess(final int idCitaOnClic , final String telefonoCliente) {
       /* HyperTrack.startTracking(new HyperTrackCallback() {
            @Override
            public void onSuccess(@NonNull SuccessResponse successResponse) {
                // Hide Login Button loader
                //loginBtnLoader.setVisibility(View.GONE);
                Log.d("NO ES FECHA", "55!!");
                Toast.makeText(getApplicationContext(), "Exito",
                        Toast.LENGTH_SHORT).show();
                Log.d("Live", ""+idCitaOnClic);
                /////este ya estaba comentado
                Intent intent = new Intent(getApplicationContext(),TestAEliminar.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);;
                intent.putExtra("IdCita",idCitaOnClic);
                intent.putExtra("telefonoCliente",telefonoCliente);
                //   intent.putExtra("tipo",Tipo);
                startActivity(intent);
/// fin del  q ya estabacomentado
            }

            @Override
            public void onError(@NonNull ErrorResponse errorResponse) {
                // Hide Login Button loader

                //loginBtnLoader.setVisibility(View.GONE);

                Toast.makeText(getApplicationContext(), "Error"
                                + " " + errorResponse.getErrorMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });*/
    }

        public  void Llamar( ){
            Intent j = new Intent(Intent.ACTION_CALL);
            j.setData(Uri.parse("tel:0" + Telefono));
            startActivity(j);
        }

public  AlertDialog FechaDiferente (final int i , final Context context,final int citaid, final String telefono,final String Lat ,final String Lon, final String placa){
    AlertDialog.Builder builder = new AlertDialog.Builder(context);
    builder.setTitle("Cita")
            .setMessage(dialogMessageFecha)
            .setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                           // MapaXrecibir(citaid);

                            cargarMapaCita(""+citaid, telefono, Lat, Lon, placa);
                            //checkForLocationSettings( citaid, telefono);
                            Log.d("Live", "22!!");

                           // createSimpleDialog(i, context, citaid, telefono).show();

                        }
                    })
            .setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            dialog.dismiss();
                        }});
    builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

    return builder.create();
}
void cargarMapaCita(String idCITA, String Telefono, String Lat, String Lon, String placa){
   /* Intent intent = new Intent(getApplicationContext(),TrackCitasRecibir.class)
            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    intent.putExtra("IdCita",idCITA);
    intent.putExtra("telefonoCliente",Telefono);
    intent.putExtra("latitud",Lat);
    intent.putExtra("longitud",Lon);
    intent.putExtra("placa",placa);
    //   intent.putExtra("tipo",Tipo);
    startActivity(intent);
    */
    ////////////////////
    TrackRecibir frag = new TrackRecibir();
    Bundle args = new Bundle();
    args.putString("IdCita",idCITA);
    args.putString("telefonoCliente",Telefono);
    args.putString("latitud",Lat);
    args.putString("longitud",Lon);
    args.putString("placa",placa);
    frag.setArguments(args);
    transaction =  fragmentManager.beginTransaction();
    transaction.replace(frame.getId(),frag);
    transaction.addToBackStack(null);
    transaction.commit();
}

    public AlertDialog createSimpleDialog(final int i, Context cont, final int cita, final  String numTelefono) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cont);
       Telefono = numTelefono;
        builder.setTitle("Cambiar estado de la cita")
                .setMessage(dialogMessage)
                .setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cambiarEstado(cita, i);
                    }
                })
                .setNegativeButton("Llamar Cliente",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                permissionCheck = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.CALL_PHONE);
                                if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                                    Llamar();
                                }else {
                                    // Explicar permiso
                                    if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                                        Toast.makeText(getActivity().getApplicationContext(), "El permiso es necesario para llamar.",
                                                Toast.LENGTH_SHORT).show();
                                    }

                                    // Solicitar el permiso
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE}, REQUEST_CODE_PHONE);
                                    }
                                }
                                //dialog.dismiss();
                            }
                        });
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();
    }

    public void cambiarEstado (final int cita,final int pos) {
        String url = URL_BASE + "cambiar-estado-cita.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            procesarCambio(oJson,pos, cita);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context,
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("cita",""+cita);
                parameters.put("estado",siguiente);
                Log.d("ESTADO ", siguiente);
                return parameters;
            }
        });
    }

    private void procesarCambio (JSONObject response, int position, int idcita) {
        try {
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":
                    Toast.makeText(context,
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "2":
                    Toast.makeText(context,
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "3":
                    Toast.makeText(context,
                            mensaje, Toast.LENGTH_LONG).show();
                    if(Home.TIPO_USER.equals("3")){
                        if(DeleteOfListe){
                            adapterValet.remove(position);
                            lstCitas.setAdapter(adapterValet);
                        }
                    }else{
                        adapter.remove(position);
                        lstCitas.setAdapter(adapter);
                    }
                    //EnviarNotificacion(idcita);
                    break;
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void activar(String placa, int IDCITA){

        Home.PLACA = placa;
        Home.mDocRef = FirebaseFirestore.getInstance().document( "Location"+ "/" + IDCITA);
        Log.d("Enviado..", "ID CITA: "+IDCITA);

        Home.canTrack=true;
    }
    public void cargarmapa( citaClas citas ){
        Latitud= citas.getLatitud2();
        Longitud = citas.getLongitud2();
        citashow =citas;
        if(Latitud==0.0) {
//          dialogNoUbicacion(getApplicationContext()).show();
            Toast.makeText(context,
                    "Aun no se a asignado lugar de entrega", Toast.LENGTH_LONG).show();
            Latitud  = -2.169143;
            Longitud = -79.9194962;
        }else{
            activar(citas.getAuto(), citas.getCitaid());
            Home.canTrack=true;
            Log.d("Enviando.."," TRACK true");

            map = clienteMap.newInstance();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(frame.getId(), map);
            transaction.addToBackStack(null);
            transaction.commit();
            map.getMapAsync(citasPorEstado.this);}

    }
    public AlertDialog createSimpleDialog( Context context) {
        Log.d("Live","Clic boton ");

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Aun no se a asignado lugar de entrega")
                .setMessage(dialogMessage)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d("Live","Clic boton ");
                    dialog.dismiss();
                            }
                        });
        builder.setIcon(getResources().getDrawable(R.drawable.anilllos));

        return builder.create();
    }

    public void MapaXrecibir( ){

        /*if(Latitud==0.0) {
            Toast.makeText(context,
                    "Aun no se a asignadi lugar de entrega", Toast.LENGTH_LONG).show();
            Latitud=-79.9194962;
            Longitud=-2.169143;

        }*/
        Log.d("LoadMapa","Carga mapa.."+idCitaOnClick);

        Intent intent = new Intent(getApplicationContext(),TrackCitasRecibir.class);
        intent.putExtra("IdCita",idCitaOnClick);
     //   intent.putExtra("tipo",Tipo);
        startActivity(intent);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        LatLng ubi = new LatLng(Latitud, Longitud);

        CameraPosition pos = CameraPosition.builder().target(ubi).zoom(13).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos));

if(!(Longitud==-2.169143)) { //-2.169143 es un valor asignado para cuando aun no esta asignado el valor por el cliente
    myMap = googleMap;
    myMap.addMarker(new MarkerOptions()
            .position(ubi)
            .title(" " + citashow.getAuto())
            .draggable(true).snippet("Ver más...")
            .icon(BitmapDescriptorFactory.fromResource(R.drawable.track_auto)));

        myMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            public void onInfoWindowClick(Marker marker)
            {
                DeleteOfListe=false;
                DialogValetFinalizar dialog = new DialogValetFinalizar();
                Bundle args = new Bundle();
                args.putString("placa", ""+citashow.getAuto());
                args.putString("direccion", citashow.getDirecionEnd());
                args.putString("referencia", citashow.getLugarEntrgraEnd());
                args.putInt("id",citashow.getCitaid());
                args.putString("telefono", citashow.getTelefono());

                dialog.setArguments(args);
                dialog.show(getFragmentManager(),"update");
              //  createSimpleDialog(3, getContext(),citashow.getCitaid()).show();

            }});

}
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_CODE_PHONE:
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)

                Llamar();

            else
                // Permiso denegado
                // Toast.makeText(getActivity().getApplicationContext()), "No se ha aceptado el permiso", Toast.LENGTH_SHORT).show();
                Log.d("Denegado","Denegado");
            return;
            // Gestionar el resto de permisos
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
