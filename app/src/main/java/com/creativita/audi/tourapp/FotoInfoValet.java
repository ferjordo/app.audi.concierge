package com.creativita.audi.tourapp;


import android.*;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class FotoInfoValet extends DialogFragment {

    Button dimis, llamar;
    TextView nombre, cedula ,correo;
    ImageView foto_valet_;
    String idCita;
    static final String URL = Constantes.URL;
    int permissionCheck;
    private static final  int REQUEST_CODE_PHONE=0;
    String celular, img;
    public FotoInfoValet() {
        // Required empty public constructor
    }

    public String getIdCita() {
        return idCita;
    }

    public void setIdCita(String idCita) {
        this.idCita = idCita;
    }
/*  @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_foto_info_valet, container, false);
    }*/

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AlertDialog.Builder build = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.fragment_foto_info_valet, null);
        dimis = (Button)v.findViewById(R.id.dimissFoto);
        dimis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        Log.d("Datos Valet","En Dialog id :"+idCita);

        nombre = (TextView) v.findViewById(R.id.name_valet);
        cedula = (TextView) v.findViewById(R.id.cedula_valet);
        correo = (TextView) v.findViewById(R.id.name_correo);
        llamar = (Button)v.findViewById(R.id.llamar_valet);
        foto_valet_ = (ImageView)v.findViewById(R.id.foto_valet_);
        llamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                permissionCheck = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.CALL_PHONE);
                if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    Llamar();
                }else {
                    // Explicar permiso
                    if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.CAMERA)) {
                        Toast.makeText(getActivity().getApplicationContext(), "El permiso es necesario para llamar.",
                                Toast.LENGTH_SHORT).show();
                    }

                    // Solicitar el permiso
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{android.Manifest.permission.CAMERA , android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CALL_PHONE}, REQUEST_CODE_PHONE);
                    }
                }
            }
        });
        ConsultarDatosValet( ""+idCita);

        build.setView(v);

        return build.create();


    }

    public void ConsultarDatosValet(final String id){
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url =Constantes.URL+"consultar-datos-valet.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            //Log.d("Data login","Este es el ID dentro de volley......"+id);
                            JSONObject oJson = new JSONObject(response);
                            procesarRespuesta(oJson);
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id", id );
                return parameters;
            }
        });

    }

   private void  procesarRespuesta(final JSONObject response){
        try{

            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":

                celular = response.getString("celular");
                img     = response.getString("img");

                this.nombre.setText(this.nombre.getText()+" "+response.getString("nombre"));
                this.correo.setText(this.correo.getText()+" "+response.getString("correo"));
                this.cedula.setText("");

                    Picasso.with(getApplicationContext())
                            .load(Constantes.URL+img)
                            .resize(300, 400)
                            .centerCrop()
                            .into(foto_valet_);
                Log.d("Datos Valet", nombre+" "+celular+" "+Constantes.URL+img+" "+correo.getText());
                break;

                case "2":
                    this.nombre.setText("Valet no asignado");
                    this.correo.setText("Valet no asignado");

                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "0":
                    this.nombre.setText("Valet no asignado");
                    this.correo.setText("Valet no asignado");
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_CODE_PHONE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)

                    Llamar();

                else
                    // Permiso denegado
                    // Toast.makeText(getActivity().getApplicationContext()), "No se ha aceptado el permiso", Toast.LENGTH_SHORT).show();
                    Log.d("Denegado","Denegado");
                return;
            // Gestionar el resto de permisos
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    public  void Llamar( ){
        Intent j = new Intent(Intent.ACTION_CALL);
        j.setData(Uri.parse("tel:0" + celular));
        startActivity(j);
    }


}
