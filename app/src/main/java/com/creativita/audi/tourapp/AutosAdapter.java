package com.creativita.audi.tourapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AutosAdapter extends ArrayAdapter<Auto>{

    private static final String URL_BASE = Constantes.URL+"autos-cliente.php";
    private static final String URL_Imagen = Constantes.URL_TO_IMG;
    private List<Auto> items;
    private RequestQueue queue;
    Context context;

    public AutosAdapter(final Context context){
        super(context,0);
        this.context = context;
        queue = Volley.newRequestQueue(this.context);
        queue.add(new StringRequest(Request.Method.POST, URL_BASE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            items = parseJson(oJson);
                            addAll(items);
                        }catch (JSONException e) {
                            Toast.makeText(context,
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("cliente",Home.IDUSER_QUERY);
                return parameters;
            }
        });
    }

    private List<Auto> parseJson(JSONObject jsonObject) {
        List<Auto> autos = new ArrayList<>();
        JSONArray jsonArray;
        int estado;
        try {
            estado = jsonObject.getInt("estado");
            switch (estado) {
                case 1:
                    String mensaje = jsonObject.getString("mensaje") + " Primero agregue uno";
                    Toast.makeText(context,
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case 2:
                    jsonArray = jsonObject.getJSONArray("autos");
                    for(int i = 0;i<jsonArray.length(); i++) {
                        try {
                            JSONObject objeto= jsonArray.getJSONObject(i);
                            Auto auto = new Auto(
                                    objeto.getString("modelo"),
                                    objeto.getString("placa"),
                                    objeto.getLong("kilometraje"),
                                    objeto.getString("imagen"));
                            autos.add(auto);
                        }catch (JSONException e) {
                            Log.e("JSON ENCODE>>>>>>>", "Error de parsing: "+ e.getMessage());
                        }
                    }
                    break;
            }
        }catch (JSONException e){
            Log.e("JSON ENCODE>>>>>>>", "Error de parsing: "+ e.getMessage());
        }
        return autos;
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public Auto getItem(int position) {
        return items.get(position);
    }

    @NonNull
    @Override
    public View getView(int i, View view,@NonNull ViewGroup viewGroup) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());

        //Salvando la referencia del View de la fila
        View listItemView;

        final ViewHolder holder;

        //Comprobando si el View no existe
        //Si no existe, entonces inflarlo
        listItemView = null == view ? layoutInflater.inflate(
                R.layout.mis_autos_lista,
                viewGroup,
                false) : view;

        Auto item = this.getItem(i);

        holder = new ViewHolder();
        holder.textModelo = (TextView) listItemView.findViewById(R.id.txtAutoModelo);
        holder.textPlaca = (TextView) listItemView.findViewById(R.id.txtAutoPlaca);
        holder.textKm = (TextView) listItemView.findViewById(R.id.txtAutoKm);
        holder.imagenAuto = (ImageView) listItemView.
                findViewById(R.id.imgAutoLista);
        Picasso.with(context).load(URL_Imagen + item.getImagen())
                .into(holder.imagenAuto);
        holder.textModelo.setText("Audi "+item.getModelo());
        holder.textPlaca.setText(item.getPlaca());
        holder.textKm.setText("" + item.getKm());

        // Procesar item
        return listItemView;
    }

    private static class ViewHolder {
        ImageView imagenAuto;
        TextView textModelo;
        TextView textPlaca;
        TextView textKm;
    }

}
