package com.creativita.audi.tourapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/*/
* actividad para cambiar la contraseña*/

public class changePassword extends Activity {

    static final String URL = Constantes.URL;
    String idFace="";
    String mail="";
    String Passs="";
    String dialogMessage;
    EditText curPass, newPass, confPass, newUser;
    Button btnChange;
    RequestQueue queue;
    ProgressBar pogress;
    LinearLayout content;
    boolean bandFB = false;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
       // LoadPerfil();

        idFace = getIntent().getStringExtra("id");
        mail = getIntent().getStringExtra("mail");
        queue = Volley.newRequestQueue(getApplicationContext());
        curPass = (EditText)findViewById(R.id.txtPassCur);
        newPass = (EditText)findViewById(R.id.txtPassNew);
        confPass = (EditText)findViewById(R.id.txtPassConf);
        newUser = (EditText)findViewById(R.id.txtUserNew);
        btnChange = (Button)findViewById(R.id.btnChangePass);
         pogress =  (ProgressBar)findViewById(R.id.progressPass);
         content = (LinearLayout)findViewById(R.id.contentPass);
        if(!idFace.equals("")) {/* En caso de haber iniciado session con fb se presenta el mensaje de que tambien se debe crear un nombre a su ausario*/
            curPass.setVisibility(View.INVISIBLE);
            dialogMessage = "Si cambias tu contraseña ya no podrás iniciar sesión con Facebook y deberás asignar un nombre para tu usuario";
            createSimpleDialog().show();
        }
        else {
            newUser.setVisibility(View.INVISIBLE);
        }
        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!idFace.equals("")){
                    if(newPass.getText().toString().equals("") || confPass.getText().toString().equals("") || newUser.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(),"CAMPOS VACIOS", Toast.LENGTH_LONG).show();
                    }
                    else {
                        int nueva = newPass.getText().toString().length();
                        int nueva2 = confPass.getText().toString().length();
                        Log.d("Pass","nueva "+nueva);
                        Log.d("Pass","nueva2 "+nueva2);
                        if(nueva<6 || nueva2 <6){
                            final AlertDialog.Builder builder = new AlertDialog.Builder(changePassword.this);

                            builder.setMessage("Su contraseña debe tener minimo 6 caracteres")
                                    .setTitle("Contraseña muy corta");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });

                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }else{
                        if (newPass.getText().toString().equals(confPass.getText().toString())) {
                            ///AQUII!!!!!!!!!!!!!!!!!!!!!!!!////////////////
                            pogress.setVisibility(View.VISIBLE);
                            content.setVisibility(View.GONE);
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            user.updatePassword(newPass.getText().toString())
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                updatePassword();
                                                bandFB=true;
                                            }else{

                                                ShowDialog();
                                            }}});
                           // Toast.makeText(getApplicationContext(),"Cambiar pass", Toast.LENGTH_LONG).show();

                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Contraseñas no cohinciden", Toast.LENGTH_LONG).show();
                        }
                        }
                    }
                }
                else {
                    if(newPass.getText().toString().equals("") || confPass.getText().toString().equals("") || curPass.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(),"CAMPOS VACIOS", Toast.LENGTH_LONG).show();
                    }
                    else {
                        if (newPass.getText().toString().equals(confPass.getText().toString())) {
                            if(newPass.getText().toString().length()>5){

                                Log.d("Pass",  mail+" - "+curPass.getText().toString());
                                pogress.setVisibility(View.VISIBLE);
                                content.setVisibility(View.GONE);
                                updatePassword();
                            }else{
                                final AlertDialog.Builder builder = new AlertDialog.Builder(changePassword.this);

                                builder.setMessage("Su contraseña debe tener minimo 6 caracteres")
                                        .setTitle("Contraseña muy corta");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                });

                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Contraseñas no cohinciden", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                }
            }
        };
    }


    public  void ShowDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Ha transcurrido mucho tiempo desde que inicio sesión es necesarió que cierre la sesión y vualva a ingresar ")
                .setTitle("Cambios no registrados, acciones necesarias");
        builder.setPositiveButton("Cerrar sesión", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //firebaseAuth = FirebaseAuth.getInstance();
                logout();

//firebaseAuth.getCurrentUser()
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public   void logout() {
        LoginManager.getInstance().logOut();
        FirebaseAuth.getInstance().signOut();
        //HyperTrack.stopTracking();
        SavePerfil("", "", "", "", "");
        goLoginScreen();
    }
    public void SavePerfil(String data, String loged, String IDUSER_QUERY, String TIPO_USER, String Celular) {

        SharedPreferences Preferens = getSharedPreferences("Load_Profile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = Preferens.edit();
        editor.putString("JSON", data);
        editor.putString("login", loged);
        editor.putString("IDUSER_QUERY", IDUSER_QUERY);
        editor.putString("TIPO_USER", TIPO_USER);
        editor.putString("Celular",Celular);
        editor.commit();
    }
    /// regresar a la pantalla de inicio de secion
    private void goLoginScreen() {
        Intent intent = new Intent(this, Log_in.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        //intent.putExtra("id", IDUSER_QUERY);
        startActivity(intent);
    }

    public AlertDialog createSimpleDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Cambio de contraseña")
                .setMessage(dialogMessage)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

        return builder.create();
    }

    private void updatePassword (){
        String url = URL + "usuario-password.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject oJson = new JSONObject(response);
                            procesarUpdate(oJson);
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id", Home.IDUSER_QUERY);
                parameters.put("tipo", Home.TIPO_USER);
                parameters.put("user",newUser.getText().toString());
                parameters.put("oldpass",curPass.getText().toString());
                parameters.put("newpass",confPass.getText().toString());
                Passs = confPass.getText().toString();

                Log.d("ChangeAA", "Id usuario:"+Home.IDUSER_QUERY+ " Tipo usuario:"+ Home.TIPO_USER +"  Usuario: "+newUser.getText().toString()+" Contraseña actual "+curPass.getText().toString() +" Contraseña nueva "+confPass.getText().toString());
                return parameters;
            }
        });

    }


    /*
    * AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Error Login FireBase", Toast.LENGTH_LONG).show();
                }
    *
    * */

    private void procesarUpdate (JSONObject response) {
        try{
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "0":
                    break;
                case "1":
                    pogress.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);
                    Toast.makeText(
                            getApplicationContext(),mensaje, Toast.LENGTH_LONG).show();
                    curPass.setText("");
                    confPass.setText("");
                    newUser.setText("");
                    break;
                case "2":
                    pogress.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(),mensaje + " intente más tarde", Toast.LENGTH_LONG).show();
                    break;
                case "3":
                    //Toast.makeText(getApplicationContext(),mensaje, Toast.LENGTH_LONG).show();
                    String idFB = ""+ response.getInt("userFB");
                    final String  newPassword = ""+newPass.getText();
                        /////Log-uot
                    if(bandFB){
                        pogress.setVisibility(View.GONE);
                        content.setVisibility(View.VISIBLE);
                        newPass.setText("");
                        confPass.setText("");
                        newUser.setText("");
                        Intent home = new Intent(changePassword.this,Home.class);
                        // pass.putExtra("id",idFace);
                        startActivity(home);



                    }else{
                        FirebaseAuth.getInstance().signOut();
                        firebaseAuth.signInWithEmailAndPassword(mail, curPass.getText().toString())
                                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                            user.updatePassword(newPassword)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                Log.d("FireBase", "User password updated.");
                                                                newPass.setText("");
                                                                confPass.setText("");
                                                                newUser.setText("");

                                                                Intent home = new Intent(changePassword.this,Home.class);
                                                                // pass.putExtra("id",idFace);
                                                                startActivity(home);

                                                            }else {
                                                                pogress.setVisibility(View.GONE);
                                                                content.setVisibility(View.VISIBLE);
                                                                Log.d("FireBase", "ERROR CHANGE"+task.getException().getMessage());
                                                            }

                                                        }
                                                    });
                                        }
                                    }
                                });

                    }



                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

}
