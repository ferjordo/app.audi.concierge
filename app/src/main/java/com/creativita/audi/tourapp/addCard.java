package com.creativita.audi.tourapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


import com.paymentez.android.Paymentez;
import com.paymentez.android.model.Card;
import com.paymentez.android.model.PaymentezPaymentSource;
import com.paymentez.android.rest.PaymentezService;
import com.paymentez.android.rest.TokenCallback;
import com.paymentez.android.rest.model.PaymentezError;
import com.paymentez.android.view.CardMultilineWidget;


public class addCard extends AppCompatActivity {
    Button buttonNext;
    CardMultilineWidget cardWidget;
    Context mContext;
    String  MAIL, UID;
    String TAG= "UID";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);

        mContext = this;
        Intent intent = getIntent();
        UID = intent.getStringExtra("UID");
        MAIL = intent.getStringExtra("mail");
        Log.d("UID", UID+" // MAIL: "+MAIL);

        cardWidget = (CardMultilineWidget) findViewById(R.id.card_add);
        buttonNext = (Button) findViewById(R.id.btnagg);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                buttonNext.setEnabled(false);

                Card cardToSave = cardWidget.getCard();
                if (cardToSave == null) {
                    buttonNext.setEnabled(true);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                    builder.setMessage("ERROR")
                            .setTitle("Datos invalidos");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();

                    return;
                }else{
                    final ProgressDialog pd = new ProgressDialog(addCard.this);
                    pd.setMessage("Enviando datos..");
                    pd.show();


                    Paymentez.addCard(mContext, UID, MAIL, cardToSave, new TokenCallback() {

                        public void onSuccess(Card card) {
                            buttonNext.setEnabled(true);
                            pd.dismiss();
                            if(card != null){
                                if(card.getStatus().equals("valid")){
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                                    builder.setMessage("status: " + card.getStatus() + "\n" +
                                                        "Card Token: " + card.getToken() + "\n" +
                                                        "transaction_reference: " + card.getTransactionReference())
                                            .setTitle("Card Successfully Added");
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                        }
                                    });

                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                    finish();

                                } else if (card.getStatus().equals("review")) {
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                                    builder.setMessage("status: " + card.getStatus() + "\n" +
                                            "Card Token: " + card.getToken() + "\n" +
                                            "transaction_reference: " + card.getTransactionReference())

                                            .setTitle("Card Under Review");
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                        }
                                    });

                                    AlertDialog dialog = builder.create();

                                    dialog.show();

                                } else {
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                                    /*builder.setMessage("" + "status: " + card.getStatus() + "\n" +
                                            "Card Token: " + card.getToken() + "\n" +
                                            "Card Valid CVC: " + card.validateCVC() + "\n" +
                                            "Card Valid Date: " + card.validateExpiryDate()+ "\n" +
                                            "Message: " + card.getMessage() + "\n" +
                                            "transaction_reference: " + card.getTransactionReference())*/
                                    builder.setMessage("Card rejected, please review your data or messages with the bank "+card.getStatus())

                                            .setTitle("Invalid Card");
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                        }
                                    });
                                    AlertDialog dialog = builder.create();
                                   /* Log.d("Card", "status: " + card.getStatus() + "\n" +
                                            "Card Token: " + card.getToken() + "\n" +
                                            "Card Valid CVC: " + card.validateCVC() + "\n" +
                                            "Card Valid Date: " + card. validateExpiryDate()+ "\n" +
                                            //"Card Valid Date: " + card. + "\n" +

                                            "transaction_reference: " + card.getTransactionReference());*/
                                   Log.d("Card",""+card.toString());
                                    dialog.show();

                                }


                            }

                            //TODO: Create charge or Save Token to your backend
                        }

                        public void onError(PaymentezError error) {
                            buttonNext.setEnabled(true);
                            pd.dismiss();
                            final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                            builder.setMessage( "Ocurrio una dificultad en el servidor intentelo  más tarde " )

                                    .setTitle("Servicio no disponible");

                            Log.d(TAG, "Type: " + error.getType() + "\n" +
                                    "Help: " + error.getHelp() + "\n" +
                                    "Description: " + error.getDescription() );
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();


                            //TODO: Handle error
                        }

                    });

                }
            }
        });


    }
}
