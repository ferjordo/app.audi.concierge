package com.creativita.audi.tourapp;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Modelos extends Fragment {
    FragmentTransaction transaction;
    FragmentManager fragmentManager;
    FrameLayout frame;
    ListView lstModelos;
    AdapterModelo adapter;
    Context context;


    public Modelos() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_modelos, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        lstModelos = (ListView)getActivity().findViewById(R.id.lstModelos);
        context = getActivity().getApplicationContext();
        fragmentManager = getFragmentManager();

        frame =(FrameLayout) getActivity().findViewById(R.id.contentFrame);
         adapter = new AdapterModelo(context);

        lstModelos.setAdapter(adapter);
        lstModelos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                modeloClass modelo = adapter.getItem(i);
                modeloA1 frag = new modeloA1();
                Bundle args = new Bundle();
                args.putString("modelo", modelo.getModelo());
                args.putString("titulo", modelo.getTitulo());
                args.putString("descripcion", modelo.descripcion);
                args.putString("img1", modelo.getImg1());
                args.putString("img2", modelo.getImg2());
                args.putString("img3", modelo.getImg3());
                args.putString("img4", modelo.getImg4());

                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });

             /*   ImageView a1 = (ImageView)getActivity().findViewById(R.id.a1);
        a1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 modeloA1 frag = new modeloA1();
                Bundle args = new Bundle();
                args.putString("modelo", "a1");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        ImageView a3 = (ImageView)getActivity().findViewById(R.id.a3);
        a3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modeloA1 frag = new modeloA1();
                Bundle args = new Bundle();
                args.putString("modelo", "a3");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        ImageView a4 = (ImageView)getActivity().findViewById(R.id.a4);
        a4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modeloA1 frag = new modeloA1();
                Bundle args = new Bundle();
                args.putString("modelo", "a4");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        ImageView a5 = (ImageView)getActivity().findViewById(R.id.a5);
        a5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modeloA1 frag = new modeloA1();
                Bundle args = new Bundle();
                args.putString("modelo", "a5");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        ImageView a6 = (ImageView)getActivity().findViewById(R.id.a6);
        a6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modeloA1 frag = new modeloA1();
                Bundle args = new Bundle();
                args.putString("modelo", "a6");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        ImageView a7 = (ImageView)getActivity().findViewById(R.id.a7);
        a7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modeloA1 frag = new modeloA1();
                Bundle args = new Bundle();
                args.putString("modelo", "a7");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        ImageView a8 = (ImageView)getActivity().findViewById(R.id.a8);
        a8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modeloA1 frag = new modeloA1();
                Bundle args = new Bundle();
                args.putString("modelo", "a8");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        ImageView q3 = (ImageView)getActivity().findViewById(R.id.q3);
        q3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modeloA1 frag = new modeloA1();
                Bundle args = new Bundle();
                args.putString("modelo", "q3");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        ImageView q5 = (ImageView)getActivity().findViewById(R.id.q5);
        q5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modeloA1 frag = new modeloA1();
                Bundle args = new Bundle();
                args.putString("modelo", "q5");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        ImageView q7 = (ImageView)getActivity().findViewById(R.id.q7);
        q7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modeloA1 frag = new modeloA1();
                Bundle args = new Bundle();
                args.putString("modelo", "q7");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        ImageView tt = (ImageView)getActivity().findViewById(R.id.tt);
        tt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modeloA1 frag = new modeloA1();
                Bundle args = new Bundle();
                args.putString("modelo", "tt");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        ImageView r8 = (ImageView)getActivity().findViewById(R.id.r8);
        r8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modeloA1 frag = new modeloA1();
                Bundle args = new Bundle();
                args.putString("modelo", "r8");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(frame.getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });*/
    }
}
