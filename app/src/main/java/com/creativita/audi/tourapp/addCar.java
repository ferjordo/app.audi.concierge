package com.creativita.audi.tourapp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class addCar extends Fragment {
    static final String URL = Constantes.URL;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final  int REQUEST_CODE_CAMERA=101;
    private static final  int PHOTO_SELECTED=102;

    int permissionCheck;
    Spinner modelos;
    Context context;
    List<String> models;
    String mCurrentPhotoPath;
    String imageFileName;
    EditText placa,placa_num ,  km, Anio;
    PackageManager pm;
    ImageView imgAuto;
    Button btnVer,btnImagenAuto;
    RequestQueue queue;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    FrameLayout frame;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_car, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        context = getActivity().getApplicationContext();
        fragmentManager = getFragmentManager();
        frame = (FrameLayout) getActivity().findViewById(R.id.contentFrame);
        Home.fragmentToPicker=1;

        pm = getActivity().getPackageManager();
        queue = Volley.newRequestQueue(context);
        placa = (EditText) getActivity().findViewById(R.id.placa);
        placa_num = (EditText) getActivity().findViewById(R.id.placa_num);
        km= (EditText) getActivity().findViewById(R.id.km);
        modelos = (Spinner) getActivity().findViewById(R.id.spnModelos);
        btnVer = (Button)getActivity().findViewById(R.id.btnVerAutos);
        btnImagenAuto = (Button)getActivity().findViewById(R.id.btnguardarimg);
        imgAuto = (ImageView)getActivity().findViewById(R.id.imgAuto);
        Anio= (EditText) getActivity().findViewById(R.id.anio);

        getModelos();

        /*
        * ENVIA AL FRAGMENTO DONDDE SE CARGA TODOS LOS MODELOS*/
        btnVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                misAutos frag = new misAutos();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(getActivity().findViewById(R.id.contentFrame).getId(),frag);
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        /*LLAMA AL INTENT PARA QUE ABRIR LA CAMARA*/
        imgAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /*
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setTitle("Cambiar  foto del auto");
                builder.setIcon(getResources().getDrawable(R.drawable.icono));
                builder.setMessage("Seleccione una opción para elegir foto");
                builder.setPositiveButton("Cámara", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        permissionCheck = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.CAMERA);
                        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                            captureImage();
                            //selectImage();
                        }
                        else {
                            // Explicar permiso
                            if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                                Toast.makeText(getActivity().getApplicationContext(), "El permiso es necesario para utilizar la cámara.",
                                        Toast.LENGTH_SHORT).show();
                            }

                            // Solicitar el permiso
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CALL_PHONE}, REQUEST_CODE_CAMERA);
                            }
                        }
                    }
                });
                builder.setNegativeButton("Galería", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                        permissionCheck = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
                        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                            // captureImage();
                            selectImage();
                        }
                        else {
                            // Explicar permiso
                            if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                                Toast.makeText(getActivity().getApplicationContext(), "El permiso es necesario para utilizar la cámara.",
                                        Toast.LENGTH_SHORT).show();
                            }

                            // Solicitar el permiso
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CALL_PHONE}, PHOTO_SELECTED);
                            }
                        }

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                */


                permissionCheck = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.CAMERA);
                if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    captureImage();
                }
                else {
                    // Explicar permiso
                    if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                        Toast.makeText(getActivity().getApplicationContext(), "El permiso es necesario para utilizar la cámara.",
                                Toast.LENGTH_SHORT).show();
                    }
                    // Solicitar el permiso
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_CAMERA);
                    }
                }
            }
        });

        /*
        *AGREG*/
        btnImagenAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(null!=imgAuto.getDrawable())
                {
                    if(placa.getText().toString().equals("")||placa_num.getText().toString().equals("")||km.getText().toString().equals("")||Anio.equals("")) {
                        Toast.makeText(getApplicationContext(),"CAMPOS VACIOS", Toast.LENGTH_LONG).show();
                    }
                    else {
                        AggAudi(view,imgAuto);
                    }
                }else{
                    Toast.makeText(context,
                            "Primero tome una foto", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    private void selectImage () {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PHOTO_SELECTED);
    }
    private void getModelos() {
        String url = URL + "listar-modelos.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            procesarModelos(json);
                        } catch (JSONException e) {
                            Log.d("ERROR JSON", "....." + e.getMessage());
                            Toast.makeText(context,
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERROR EN MODELOS", " " + error.getMessage());
                        Toast.makeText(context,
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ));
    }

    private void procesarModelos(JSONObject response) {
        try {
            int estado = response.getInt("estado");
            switch (estado) {
                case 0:
                    String mensaje = response.getString("mensaje");
                    Toast.makeText(context,
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case 1:
                    JSONArray models = response.getJSONArray("modelos");
                    this.models = new ArrayList<String>();
                    for (int i = 0; i < models.length(); i++) {
                        this.models.add(models.getString(i));
                        Log.d("MODELO>>>>>>", "" + models.get(i));
                    }
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, this.models);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        modelos.setAdapter(adapter);
    }


    /* FUNCION QUE ENVIA LOS DATOS AL SERVIDOR */
    private void AggAudi(final View view, final ImageView img) {
        String url = URL + "agregar-auto.php";
        queue.add(new MultipartRequest(Request.Method.POST, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                       /*RESPUESTA DEL SERVIDOR*/
                            JSONObject oJson = new JSONObject(response);
                          /*LLAMA A LA FUNCION Q ANALIZA LA RESPUESTA DEL SERVIDOR*/
                            VerificarRespuesta(oJson, view);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context,
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Snackbar.make(view, "Compruebe su conexion a internet", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("cliente", Home.IDUSER_QUERY);
                parameters.put("modelo", modelos.getSelectedItem().toString());
                parameters.put("km", km.getText().toString());
                parameters.put("placa", placa.getText().toString().toUpperCase()+"-"+placa_num.getText().toString());
                parameters.put("anio", Anio.getText().toString());

                return parameters;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // VARIALBLE IMAGEN QUE SE ENVIARA AL SERVIDOR

                params.put("txt_image", new DataPart(imageFileName+".jpeg", AppHelper.getFileDataFromDrawable(context, img.getDrawable()), "image/jpeg"));
                return params;
            }
        });
    }

    /* DETECTA CUAL ES LA RESPUESTA DEL SERVIDOR */
    private void VerificarRespuesta(JSONObject response, View view) {
        try {
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":
                    /*Modelo noexiste*/
                    Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    break;
                case "2":
                    Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    LoginManager.getInstance().logOut();
                    break;
                case "0":
                    Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    break;
                case "3":
                    Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    break;
                case "4":
                /* Auto creado*/
                    Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    placa.setText("");
                    km.setText("");
                    placa_num.setText("");
                    imgAuto.setImageBitmap(null);
                    ExitoCita frag = new ExitoCita();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(frame.getId(), frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    break;
                case "5":
                    Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void captureImage () {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (cameraIntent.resolveActivity(pm) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                //cUADNO EL ERROR OCURRE MIENTRAS SE CREA EL ARCHIVO

            }
            if (photoFile != null) {

                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            }

        }
    }

    private File createImageFile() throws IOException {
        // Creamos un archivo imagen
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "JPEG_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory() + "/picupload/autos";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();

        File image = new File(storageDir + "/" + imageFileName + ".jpg");

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void setPic() {
        // sE OPTIENE LAS DIMENSIONES DEL VIEW
        int targetW = imgAuto.getWidth();
        int targetH = imgAuto.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decodificando el archivo de imagen en un  Bitmap sized para llenar el View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor << 1;
        //bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        Matrix mtx = new Matrix();
        mtx.postRotate(0);
        // Rotating Bitmap
        Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);

        if (rotatedBMP != bitmap)
            bitmap.recycle();

        imgAuto.setImageBitmap(rotatedBMP);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            setPic();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    // Permiso aceptado
                    captureImage();
                else
                    // Permiso denegado
                    Toast.makeText(context, "No se ha aceptado el permiso", Toast.LENGTH_SHORT).show();
                return;
            case PHOTO_SELECTED:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    // Permiso aceptado
                    selectImage();
                else
                    // Permiso denegado
                    // Toast.makeText(getActivity().getApplicationContext()), "No se ha aceptado el permiso", Toast.LENGTH_SHORT).show();
                    Log.d("Denegado","Denegado");
                return;
            // Gestionar el resto de permisos
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
