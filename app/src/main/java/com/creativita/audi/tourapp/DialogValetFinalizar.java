package com.creativita.audi.tourapp;

import android.*;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;



public class DialogValetFinalizar extends DialogFragment {

    Button cancel , update , call;
    private static final String URL = Constantes.URL;

    private static RequestQueue queue;

    Integer idcita;

    TextView Placa, Direccion, LugarReferencia;

    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    FrameLayout frame;
    View v;
    int permissionCheck;
    String Telefono;
    private static final  int REQUEST_CODE_PHONE=0;




    public DialogValetFinalizar(){}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder build = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

         v = inflater.inflate(R.layout.dilog_valet_finalizar, null);

        fragmentManager = getFragmentManager();
        frame = (FrameLayout) getActivity().findViewById(R.id.contentFrame);

        Placa = (TextView) v.findViewById(R.id.placavalet) ;
        Direccion = (TextView) v.findViewById(R.id.dirvalet);
        LugarReferencia  = (TextView) v.findViewById(R.id.refValet);
        queue = Volley.newRequestQueue(getApplicationContext());

        Placa.setText(Placa.getText().toString()+" "+ this.getArguments().getString("placa"));
        Direccion.setText(Direccion.getText().toString()+" "+this.getArguments().getString("direccion"));
        LugarReferencia.setText(LugarReferencia.getText().toString()+" "+ this.getArguments().getString("referencia"));
        idcita = this.getArguments().getInt("id");
        Telefono = this.getArguments().getString("telefono");

        cancel =(Button)v.findViewById(R.id.cancelValet);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        update =(Button)v.findViewById(R.id.sendValet);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cambiarEstado ( idcita);
            }
        });

        call = (Button)v.findViewById(R.id.callValet);
        call.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        permissionCheck = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), android.Manifest.permission.CALL_PHONE);
                                        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                                            Llamar();
                                        }else {
                                            // Explicar permiso
                                            if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.CAMERA)) {
                                                Toast.makeText(getActivity().getApplicationContext(), "El permiso es necesario para llamar.",
                                                        Toast.LENGTH_SHORT).show();
                                            }

                                            // Solicitar el permiso
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{android.Manifest.permission.CAMERA , android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CALL_PHONE}, REQUEST_CODE_PHONE);
                                            }
                                        }
                                    }
                                }
        );

        build.setView(v);

        return build.create();
    }


    public  void Llamar( ){
        Intent j = new Intent(Intent.ACTION_CALL);
        j.setData(Uri.parse("tel:0" + Telefono));
        startActivity(j);
    }

    public void cambiarEstado (final int cita) {
        String url = URL + "cambiar-estado-cita.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            procesarCambio(oJson);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("cita",""+cita);
                parameters.put("estado","finalizado");
                return parameters;
            }
        });
    }

    private void procesarCambio (JSONObject response) {
        try {
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "2":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "3":

                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    citasPorEstado frag = new citasPorEstado();
                   // enviarNotificacion ();

                    Bundle args = new Bundle();
                    args.putString("estado", "finalizado");
                    frag.setArguments(args);
                    transaction =  fragmentManager.beginTransaction();
                    transaction.replace(getActivity().findViewById(R.id.contentFrame).getId(),frag);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    Home.canTrack=false;
                    dismiss();
                    break;
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }
    void enviarNotificacion (){
        String url = URL + "NotificarAlCrearCita.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            procesarCambio(oJson);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id",""+Home.IDUSER_QUERY);
                parameters.put("notificar", "Fin");

                return parameters;
            }
        });

    }

}
