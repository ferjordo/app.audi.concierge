package com.creativita.audi.tourapp;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class citasSeleccion extends Fragment {
    Context context;
    Button btnRecibir, btnReparacion, btnEntregar;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_citas_seleccion, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        context = getActivity().getApplicationContext();
        fragmentManager = getFragmentManager();

        btnRecibir = (Button)getActivity().findViewById(R.id.btnCitasRecibir);
        btnReparacion = (Button)getActivity().findViewById(R.id.btnCitasReparacion);
        btnEntregar = (Button)getActivity().findViewById(R.id.btnCitasEntregado);

        btnRecibir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                citasPorEstado frag = new citasPorEstado();
                Bundle args = new Bundle();
                args.putString("estado", "recibir");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(getActivity().findViewById(R.id.contentFrame).getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        btnReparacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                citasPorEstado frag = new citasPorEstado();
                Bundle args = new Bundle();
                args.putString("estado", "reparacion");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(getActivity().findViewById(R.id.contentFrame).getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        btnEntregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                citasPorEstado frag = new citasPorEstado();
                Bundle args = new Bundle();
                args.putString("estado", "entregado");
                frag.setArguments(args);
                transaction =  fragmentManager.beginTransaction();
                transaction.replace(getActivity().findViewById(R.id.contentFrame).getId(),frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }
}
