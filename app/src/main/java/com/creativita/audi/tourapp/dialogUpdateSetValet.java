package com.creativita.audi.tourapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class dialogUpdateSetValet extends DialogFragment {

    private static final String URL = Constantes.URL;


    Spinner valetspiner;
    private static RequestQueue queue;
    Context mcontext;
    Button update1,cancel1;
    List<String> valets;
    List<Valet> almacenvalet;
    JSONObject response;
    String idCita;
    String estado;
    public dialogUpdateSetValet(){

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder build = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_update_set_valet, null);


        update1 =(Button) v.findViewById(R.id.btnUpdate1);

        cancel1 =(Button) v.findViewById(R.id.btnCancelar1);

        valetspiner = (Spinner) v.findViewById(R.id.valetspiner);
        mcontext = v.getContext();
        queue = Volley.newRequestQueue(mcontext);
        idCita = this.getArguments().getString("idcita");
        Log.d ("Dialog", ""+ Integer.getInteger(idCita));
        almacenvalet =  new ArrayList<>();
        getValets  ();

        cancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

       update1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            Log.d("Valets Dilog "," "+idCita+ " el id del valet:"+ almacenvalet.get( valetspiner.getSelectedItemPosition()).getId() );
                CambiarValetCita ( idCita,  almacenvalet.get( valetspiner.getSelectedItemPosition()).getId());

            }
        });
       Button btnVerUbicacion  = (Button) v.findViewById(R.id.btnSee1) ;
       btnVerUbicacion.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

              // GetUrlToTrack(idCita);
               CargarActivity(  Integer.parseInt(idCita));

           }
       });
//btnVerUbicacion.setVisibility(View.GONE);
        build.setView(v);

        return build.create();
    }

    public  void GetUrlToTrack( final String idcita){
        Log.d("Live","Consultnado URL...");

        String url = Constantes.URL + "get-url-track.php";
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request= new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("Live","Se ha obtenido respuesta del serivdor...");

                            JSONObject oJson = new JSONObject(response);
                            procesaUrl(oJson);

                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id",""+idcita);
                return parameters;
            }
        };
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);


    }
    private void  procesaUrl(JSONObject response) {

        Log.d("Live","Obteniendo UrL...");

        try {
            String estado = response.getString("estado");
            //String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":

                    Log.d("Live",""+response.getString("url"));
                    String url = response.getString("url");
                    //Log.d("NOTIFICACION "," cambiar ventana: enviar notificación");
                    //  EnviarNotificacion(id, placa);
                    // dismiss();

                    if(url.equals("")||url.equals("null")){
                        //Toast.makeText(getApplicationContext(),"Aun no hay Url de seguimiento", Toast.LENGTH_LONG).show();
                        // dialoNOurl(getApplicationContext()).show();

                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                        builder.setMessage("El valet aun no ha acitivado el modo de seguimiento")
                                .setTitle("Esperando Valet");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();

                    }else{
                        //CargarActivity( url);
                    }
                    break;

                case "0":
                    Toast.makeText(getApplicationContext(),
                            "Cita no existe", Toast.LENGTH_LONG).show();
                    //dismiss();
                    break;

            }
        }catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void CargarActivity(int idCita){
        // Start User Session by starting MainActivity
        Intent MapaTrack = new Intent(
                getApplicationContext(), SingleTrackCita_cliente.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        MapaTrack.putExtra("id",idCita);
        //Log.d("Live", "::::"+id);
       // mainActivityIntent.putExtra();//No tiene sentido enviar este nuemero se necesita lnumero de la cita
        //Log.d("Live","URL: " +url+ " Telefono: "+Home.Celular);

        startActivity(MapaTrack);
    }

public void getValets  () {

    RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
    String url = URL + "listar-valet.php";
    queue.add(new StringRequest(Request.Method.POST, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject json = new JSONObject(response);
                        procesarvalets(json);
                    } catch (JSONException e) {
                        Log.d("ERROR JSON", "....." + e.getMessage());
                        Toast.makeText(mcontext,
                                "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR EN VALETS", " " + error.getMessage());
                    Toast.makeText(mcontext,
                            "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                }
            }
    ) {
        /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
        @Override
        protected Map<String, String> getParams() throws AuthFailureError {
            Map<String, String> parameters = new HashMap<>();


            return parameters;
        }
    });
}

    void procesarvalets( JSONObject response){
        int estado = 0;
        try {
            estado = response.getInt("estado");
            switch (estado) {
                case 1:
                    String mensaje = response.getString("mensaje") + " Agregue uno para separar asignar  citas";
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case 2:
                    JSONArray valet = response.getJSONArray("valets");
                    this.valets = new ArrayList<String>();
                    for (int i = 0; i < valet.length(); i++) {
                        JSONObject array = valet.getJSONObject(i);
                        this.valets.add(""+array.getString("nombre")+" "+array.getString("apellido"));
                        this.almacenvalet.add(new Valet(array.getString("id"),array.getString("nombre"),array.getString("apellido")));
                    }
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }



        if(estado == 2){
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, this.valets);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            valetspiner.setAdapter(adapter);
        }
    }


    public void CambiarValetCita (final String cita,final String valet) {
        String url = URL + "actualizar-cita-valet.php";
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            procesarCambioValet(oJson, cita);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("cita",""+cita);
                parameters.put("valet",""+valet);
                return parameters;
            }
        };
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }


    private void procesarCambioValet (JSONObject response, String cita) {
        try {
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":
                    Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();
                    //EnviarNoficacion(cita);
                    dismiss();
                    break;
                case "2":
                    Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();
                    dismiss();
                    break;
                case "3":
                  /*  final AlertDialog dialog = new AlertDialog.Builder(getApplicationContext())
                            .setTitle("Valet Ocupado")
                            .setMessage("El valet ya ha sido asignado a otra cita, por favor eliga otro valet")
                            .setPositiveButton("OK", null) //Set to null. We override the onclick
                            .create();
                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {

                        @Override
                        public void onShow(DialogInterface dialogInterface) {

                            Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                            button.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View view) {
                                    // TODO Do something
                                    Log.d("Live","Compartir ubicación");

                                    //Dismiss once everything is OK.
                                    dialog.dismiss();
                                }
                            });

                        }
                    });
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();*/
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    dismiss();

                    break;
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void EnviarNoficacion(final String idCita) {

        String url = URL + "Notificacion-valet.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                          //  Log.d("NOTIFICACION",""+response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id",""+almacenvalet.get( valetspiner.getSelectedItemPosition()).getId());
                parameters.put("cita",""+idCita);
                Log.d("","Cita: "+idCita);
                return parameters;
            }
        });


    }

}
