package com.creativita.audi.tourapp;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;



import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.facebook.FacebookSdk.getApplicationContext;

public class TrackCitasRecibir extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    LocationManager locationManager;
    String locationProvider;

    String collectionId = "";
    String LookID = "";
    String actionID = "";

    String TAG = "Live";
    String ExpextedID = "";
    String Telefono = "";
    int posicionado = 0;
    String CLIENTE_ID = "";
    Double Lat, Long;
    private GoogleApiClient apiClient; /* Variale  de google client */

    private GoogleMap myMap;


    String IDCITA = "";
    int logout = 0, id;
    String dialogMessage, URLtoTRACK, Placa;
    boolean canBack = false;
    boolean backHome = false;


    int permissionCheck;
    private static final int REQUEST_CODE_PHONE = 0;
    boolean click = false;

    FragmentTransaction transaction;
    FragmentManager fragmentManager;
    FrameLayout frame;
    ConsultasServidor consultasServidor;

    //private ProgressBar progressBar;

    Boolean Update = false;
    UptateTrack track;

    clienteMap map;
    String shareMessage;


    public static final String LATITUD_KEY = "latitud";
    public static final String LONGITUD_KEY = "longitud";
    public static final String TAG_GPS = "Enviando..";
    TextView mQuoteTextView;




    private DocumentReference mDocRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //Teliver.setTripListener(this);

        setContentView(R.layout.activity_track_citas_recibir);
        Intent intent = getIntent();
        IDCITA = intent.getStringExtra("IdCita");
        String Lat1 = intent.getStringExtra("latitud");
        String Long1 = intent.getStringExtra("longitud");
        Placa = intent.getStringExtra("placa");
        Lat = Double.parseDouble(Lat1);
        Long = Double.parseDouble(Long1);

        //// DIRECCIÒN DE LA BASE FIREBASE CLOUD
        mDocRef = FirebaseFirestore.getInstance().document(Placa + "/" + IDCITA);


        Log.d(TAG, "" + Long + " " + Lat);
        //CLIENTE_ID = intent.getIntExtra("IdCliente",id);


        actionID = "" + IDCITA;
        Telefono = "" + intent.getStringExtra("telefonoCliente");
        Log.d(TAG, "" + actionID + " " + Telefono);

        LookID = actionID;
        collectionId = actionID;
        ExpextedID = actionID;
        //track = new UptateTrack(LookID);

        //SharedPreferences.Editor editor = Preferens.edit();
        dialogMessage = "¿Seguro qué desea cambiar el estado de la cita a REPARAR ? ";

        // progressBar = (ProgressBar) findViewById(R.id.progressmap);

        //progressBar.setVisibility(View.GONE);
        consultasServidor = new ConsultasServidor(this);

        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS &&
                apiAvailability.isUserResolvableError(resultCode)) {
            Dialog dialog = apiAvailability.getErrorDialog(this, resultCode,
                    900);
            dialog.setCancelable(false);
            dialog.show();
        }
        fragmentManager = getFragmentManager();
        frame = (FrameLayout) findViewById(R.id.contentMap);

        map = clienteMap.newInstance();
        transaction = fragmentManager.beginTransaction();
        transaction.replace(frame.getId(), map);
        transaction.addToBackStack(null);
        transaction.commit();
        map.getMapAsync(this);

        // progressBar.setVisibility(View.GONE);
        // GetUrlToTrack(""+IDCITA); FUNCIÓN YA NO UTLIZADA

        //Toast.makeText(getApplicationContext(), "Se esta compartiendo la ubicación", Toast.LENGTH_SHORT).show();


        FloatingActionButton call = findViewById(R.id.callToOwer);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                permissionCheck = ContextCompat.checkSelfPermission(TrackCitasRecibir.this.getApplicationContext(), android.Manifest.permission.CALL_PHONE);
                if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    //Llamar();
                    Intent j = new Intent(Intent.ACTION_CALL);
                    j.setData(Uri.parse("tel:0" + Telefono));
                    startActivity(j);
                } else {
                    // Explicar permiso
                    if (ActivityCompat.shouldShowRequestPermissionRationale(TrackCitasRecibir.this, android.Manifest.permission.CAMERA)) {
                        Toast.makeText(getApplicationContext(), "El permiso es necesario para llamar.",
                                Toast.LENGTH_SHORT).show();
                    }

                    // Solicitar el permiso
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE}, REQUEST_CODE_PHONE);
                    }
                }
            }
        });
        FloatingActionButton finish = findViewById(R.id.finish);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salir(IDCITA);
            }
        });

        FloatingActionButton CambiarCita = findViewById(R.id.cambiarEstado);
        CambiarCita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("Live", "Clic boton 0");
                createSimpleDialog(TrackCitasRecibir.this, IDCITA).show();
                //track.setBand(false);
                //track.interrupt();
            }
        });



    }


    public AlertDialog showDialogAcitvarSeguimiento (Context context){

        Log.d("Live","Clic boton ");

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Habilitar seguimiento")
                .setMessage(dialogMessage)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d("Live","Clic boton ");
                                //

                            }
                        })
                .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("Live","Clic boton ");


                    }
                })
        .setCancelable(false);
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();

    }

    private void startTrip(String title, String msg, String userId, String trackingId) {
       /* try {
            TripBuilder builder = new TripBuilder(trackingId);
            if (!userId.isEmpty()) {
                PushData pushData = new PushData(userId.split(","));
                pushData.setPayload(msg);
                pushData.setMessage(title);
                builder.withUserPushObject(pushData);
            }
            builder.withInterval(2000); //Location update set to 5 seconds.
            builder.withDistance(5);
            Teliver.startTrip(builder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    public  void Llamar(){
        Intent j = new Intent(Intent.ACTION_CALL);
        j.setData(Uri.parse("tel:0" + Telefono));
    }


    public  void GetUrlToTrack( final String idcita){
        Log.d("Live","Consultnado URL...");

        String url = Constantes.URL + "get-url-track.php";
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("Live","Se ha obtenido respuesta del serivdor...");

                            JSONObject oJson = new JSONObject(response);
                            procesaUrl(oJson);

                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id",""+idcita);
                return parameters;
            }
        });
    }

    private void  procesaUrl(JSONObject response) {

        Log.d("Live","Obteniendo UrL...");

        try {
            String estado = response.getString("estado");
            //String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":
                    Log.d("Live","Exito");
                    CLIENTE_ID = response.getString("cliente");
                    //startTrip("Cita por Recebir","En seguimiento", "cliente_"+CLIENTE_ID,""+IDCITA);

                    break;

                case "0":
                    Toast.makeText(getApplicationContext(),
                            "Cita no existe", Toast.LENGTH_LONG).show();
                    //dismiss();
                    break;

            }
        }catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public AlertDialog createSimpleDialog( Context context, final String cita) {
        Log.d("Live","Clic boton ");

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Cambiar estado de la cita")
                .setMessage(dialogMessage)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d("Live","Clic boton ");

                                cambiarEstado(cita);
                            }
                        });
        builder.setIcon(getResources().getDrawable(R.mipmap.ic_launcher));

        return builder.create();
    }
    public void cambiarEstado (final String cita) {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        String url = Constantes.URL + "cambiar-estado-cita.php";
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            procesarCambio(oJson, cita);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("cita",""+cita);
                parameters.put("estado","reparacion");
                Log.d("ESTADO ", "reparacion");
                return parameters;
            }
        };
        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }
    private void procesarCambio (JSONObject response,  String idcita) {
        try {
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "2":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    break;
                case "3":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                   //EXITO
                   // salir(actionID); // SALIR DE TILEVIRE LIBRERIA Q HACE LIVE TRAKING
                    Intent home = new Intent(TrackCitasRecibir.this,Home.class);
                    home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(home);
                    //EnviarNotificacion(idcita);
                    break;
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PHONE) {
            //Log.d("GPS",".........ACEPTO....PERMISOS....");
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Llamar();
            }}else if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
            //validateTrip();
            }
        else{
            Log.d("","");
            Toast.makeText(getApplicationContext(), "Es necesario compartir los permisos",Toast.LENGTH_SHORT).show();
           // CustomToast.showToast(context, getString(R.string.text_location_permission));
    }
    }

    public void salir(String IDCITA){
        Toast.makeText(getApplicationContext(), "Saliste",Toast.LENGTH_SHORT).show();
       // Teliver.stopTrip(IDCITA);
    }

    public void setMapPosition() {
        if (posicionado == 0) {
            LatLng ubi = new LatLng(Lat, Long);
            CameraPosition pos = CameraPosition.builder().target(ubi).zoom(13).build();
            myMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos));
            posicionado = 1;
        }
    }





    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

     /*  LatLng ubi = new LatLng(Lat, Long);
        //setMapPosition();


        CameraPosition pos = CameraPosition.builder().target(ubi).zoom(13).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos));

        myMap = googleMap;
        myMap.addMarker(new MarkerOptions().position(ubi).title("PLACA: " + Placa));

   */ }
    @Override
    public void onBackPressed() {


                Intent intent = new Intent(getApplicationContext(),Home.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);;
                startActivity(intent);


    }




}

