package com.creativita.audi.tourapp;


public class citaClas {

    /*Objeto cita que se utiliza para presentar en list administrador y separar citas*/
    private String fecha, metodoPago, lugarEntrega, auto, estado, latitud,longitud,  LugarEntrgraEnd, descipcion , direcionEnd, telefono ;
    double latitud2, longitud2;
    private int Citaid;

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public citaClas (int id, String fecha, String metodoPago, String lugarEntrega, String auto, String estado) {
        setCitaid(id);
        setFecha(fecha);
        setMetodoPago(metodoPago);
        setLugarEntrega(lugarEntrega);
        setAuto(auto);
        setEstado(estado);
    }


    public citaClas(int id, String fecha,String metodoPago,String lugarEntrega, String auto, String estado, String latitud, String longitud, double latitud2, double longitud2,String LugarEntrgraEnd,
                    String direcionEnd, String telefono) {
        setCitaid(id);
        setFecha(fecha);
        setMetodoPago(metodoPago);
        setLugarEntrega(lugarEntrega);
        setAuto(auto);
        setEstado(estado);
        setLatitud(latitud);
        setLongitud(longitud);
        setLatitud2(latitud2);
        setLongitud2(longitud2);
        setLugarEntrgraEnd(LugarEntrgraEnd);
        setDirecionEnd(direcionEnd);
        setTelefono(telefono);
    }

    public citaClas(int id, String fecha,String metodoPago,String lugarEntrega, String auto, String estado, String latitud, String longitud) {
        setCitaid(id);
        setFecha(fecha);
        setMetodoPago(metodoPago);
        setLugarEntrega(lugarEntrega);
        setAuto(auto);
        setEstado(estado);
        setLatitud(latitud);
        setLongitud(longitud);


    }

    public String getDirecionEnd() {
        return direcionEnd;
    }

    public void setDirecionEnd(String direcionEnd) {
        this.direcionEnd = direcionEnd;
    }

    public void setLatitud2(double latitud2) {
        this.latitud2 = latitud2;
    }

    public void setLongitud2(double longitud2) {
        this.longitud2 = longitud2;
    }
    //latitud2, longitud2, LugarEntrgraEnd, descipcion


    public Double getLatitud2() {
        return latitud2;
    }

    public void setLatitud2(Double latitud2) {
        this.latitud2 = latitud2;
    }

    public Double getLongitud2() {
        return longitud2;
    }

    public void setLongitud2(Double longitud2) {
        this.longitud2 = longitud2;
    }

    public String getLugarEntrgraEnd() {
        return LugarEntrgraEnd;
    }

    public void setLugarEntrgraEnd(String lugarEntrgraEnd) {
        LugarEntrgraEnd = lugarEntrgraEnd;
    }

    public String getDescipcion() {
        return descipcion;
    }

    public void setDescipcion(String descipcion) {
        this.descipcion = descipcion;
    }

    public String getLatitud() {
        return latitud;
    }


    public String getLongitud() {
        return longitud;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getMetodoPago() {
        return metodoPago;
    }

    public void setMetodoPago(String metodoPago) {
        this.metodoPago = metodoPago;
    }

    public String getLugarEntrega() {
        return lugarEntrega;
    }

    public void setLugarEntrega(String lugarEntrega) {
        this.lugarEntrega = lugarEntrega;
    }

    public String getAuto() {
        return auto;
    }

    public void setAuto(String auto) {
        this.auto = auto;
    }

    public int getCitaid() {
        return Citaid;
    }

    public void setCitaid(int citaid) {
        Citaid = citaid;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setLatitud(String estado) {
        this.latitud = estado;
    }

    public void setLongitud(String estado) {
        this.longitud = estado;
    }

}
