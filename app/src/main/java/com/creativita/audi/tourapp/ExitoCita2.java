package com.creativita.audi.tourapp;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExitoCita2  extends android.app.Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Button btn, btn2;
    private ExitoCita.OnFragmentInteractionListener mListener;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    FrameLayout frame;

    public ExitoCita2() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ExitoCita2 newInstance(String param1, String param2) {
        ExitoCita2 fragment = new ExitoCita2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_exito_cita2, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        fragmentManager = getFragmentManager();
        frame = (FrameLayout) getActivity().findViewById(R.id.contentFrame);
        btn = (Button) getActivity().findViewById(R.id.back_home2);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Secondmenu frag = new Secondmenu();
                transaction = fragmentManager.beginTransaction();
                transaction.replace(frame.getId(), frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        btn2 = (Button) getActivity().findViewById(R.id.back_activas);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                citasActivas frag = new citasActivas();
                transaction = fragmentManager.beginTransaction();
                transaction.replace(frame.getId(), frag);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*  @Override
      public void onAttach(Context context) {
          super.onAttach(context);
          if (context instanceof OnFragmentInteractionListener) {
              mListener = (OnFragmentInteractionListener) context;
          } else {
              throw new RuntimeException(context.toString()
                      + " must implement OnFragmentInteractionListener");
          }
      }
  */
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

