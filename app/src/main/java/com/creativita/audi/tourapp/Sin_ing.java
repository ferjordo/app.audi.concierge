package com.creativita.audi.tourapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.MailTo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Parameter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Sin_ing extends AppCompatActivity {
    CallbackManager callbackManager;
    JSONObject jsonDatauser,profile_pic_data, profile_pic_url;
    EditText usuario,contraseña,nombre,apellido ,confirmcontraseña , Correo , celular, Cedula ;
    String  User ="" , Pass="", Name="" , Lastname="",confirmpass="", Mail , BANDCONFIR="yes",id="",img_User,Tipo, cedula="";
    String TAG = "FireBase";
    String UID;
    TextView TyC ;
    CheckBox checkterm ;
    int mYear, mMonth, mDay, mHour, mMinute;
    Button btnNacimiento ;
    String fechaNacimiento = "0";


    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;
    private ProgressBar progressBar;
    LinearLayout content;
    Boolean classic =false;
    LoginResult loginResultGlobal = null;
    //termycond



    final String URL= Constantes.URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sin_ing);
        usuario = (EditText) findViewById(R.id.userSing);
        contraseña=(EditText) findViewById(R.id.passSing);
        nombre= (EditText) findViewById(R.id.namesing);
        apellido = (EditText) findViewById(R.id.lastname);
        Correo =(EditText) findViewById(R.id.mail);
        celular=(EditText)findViewById(R.id.celular) ;
        Cedula=(EditText)findViewById(R.id.cedula_user) ;

        confirmcontraseña =(EditText) findViewById(R.id.confrimpass);

        callbackManager = CallbackManager.Factory.create();

        TyC = (TextView)findViewById(R.id.termycond);
        TyC.setMovementMethod(LinkMovementMethod.getInstance());

        checkterm = (CheckBox)findViewById(R.id.checkterm);

        btnNacimiento = (Button)findViewById(R.id.btnNacimiento);

        progressBar = (ProgressBar) findViewById(R.id.progressLogin);
        content = (LinearLayout) findViewById(R.id.contetnSingIn);

        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button2);
        loginButton.setReadPermissions("email");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
               //fb=true;
                loginResultGlobal =loginResult;
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), " Cancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(),
                        ""+error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    if(!classic){
                        getUserInfo(loginResultGlobal);
                        UID = user.getUid();
                    }
                }
            }
        };


    }

    private void handleFacebookAccessToken(AccessToken accessToken) {
        progressBar.setVisibility(View.VISIBLE);
        content.setVisibility(View.GONE);

        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Error Login FireBase", Toast.LENGTH_LONG).show();
                }
                progressBar.setVisibility(View.GONE);
                 content.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(firebaseAuthListener);
    }


    protected void getLoginDetails(LoginButton login_button){
        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult login_result) {
                getUserInfo(login_result);
            }

            @Override
            public void onCancel() {
                // code for cancellation
                Log.d("Data login","Cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                //  code to handle error
                Toast.makeText(getApplicationContext(),
                        ""+exception.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /*
      Metodo para obtener la infromacion del usuario logeado
    */
    protected void getUserInfo(LoginResult login_result){

        GraphRequest data_request = GraphRequest.newMeRequest(
                login_result.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject json_object,
                            GraphResponse response) {
                        try {
                            //JSONObject responsedataId = new JSONObject(jsondata);
                            id= ""+json_object.get("id").toString();
                            Mail= ""+json_object.get("email").toString();
                            Name= ""+json_object.get("first_name").toString();
                            Lastname= ""+json_object.get("last_name").toString();
                            profile_pic_data = new JSONObject(json_object.get("picture").toString());
                            profile_pic_url = new JSONObject(profile_pic_data.getString("data"));
                            img_User = profile_pic_url.getString("url");

                            Log.d("Data login","JSON FACEBOOK....."+json_object.toString());
                            verificarperfil(id,Mail,Name,Lastname);
                            DoJson();
                            //jsonDatauser=json_object;
                            // gotoMain(json_object);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("ERROR","ERROR......"+e.getMessage());
                        }
                    }
                });

        Bundle permission_param = new Bundle();
        permission_param.putString("fields","id,name,first_name,last_name,email,picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();

    }
    /*
    * cabiar de pantalla despues de loeegar*/
    void gotoMain(JSONObject json_object,String bandConfirm, String id_User, String uid){
        Intent intent = new Intent(Sin_ing.this,Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("loged",1);//bandera para saber si guardar preferencias o no
        intent.putExtra("classic",""+bandConfirm);
        intent.putExtra("jsondata",json_object.toString()); /// pbjeto que lleva toda l informacion de usuario
        intent.putExtra("id",id_User);
        intent.putExtra("tipo",Tipo);
        intent.putExtra("pass", Pass);
        intent.putExtra("UID", uid);
        startActivity(intent);
    }

    void  gotoLogin(){
    Intent intent = new Intent(Sin_ing.this,Log_in.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);

}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        Log.e("data",data.toString());
    }

    /*
    * Consultar al servidor para el logeo
    * https://audiguayaquil.com/SERVICE/sign-in.php
    * "user", id
      "nombre", nombre );
       apellido", apellido );
      "mail", mail );
      "celular",celular);
      "estado", "1" );
    * */
    /*
    * REGISTRO CON FACEBOOK*/
    public void verificarperfil(final String id, final  String mail,final  String nombre, final  String apellido){
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        String url =URL+"sign-in.php";
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            Log.d("Data login","Este es el ID dentro de volley......"+id);
                            JSONObject oJson = new JSONObject(response);
                            procesarRespuesta(oJson);
                        }catch (JSONException e){
                            e.printStackTrace();
                            Log.d("Data login","....."+e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*EN CASO DE ERROR */
                        Log.e("Data Login ERROR"," "+error.getMessage());
                        System.out.print(""+error);
                    }
                }
        ) {
            /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("user", id );
                parameters.put("nombre", nombre );
                parameters.put("apellido", apellido );
                parameters.put("mail", mail );
                parameters.put("celular",celular.getText().toString());
                parameters.put("estado", "1" );
                parameters.put("uid", UID);
                return parameters;
            }
        });

    }
    /*funcion para decodificar la respuesta del servicio web*/
    private void procesarRespuesta(JSONObject response){
        Log.d("Data login","MENSAJE......"+response.toString());
        try{
            String estado = response.getString("estado");
          // String mensaje = response.getString("mensaje");
            switch (estado) {
                case "2":
                    /*Registro del usuario registrado*/
                    String id_user = response.getString("cliente_id");
                    Log.d("Data login","MENSAJE......" + estado + "\nID: " + id_user);
                    //progressBar.setVisibility(View.GONE);
                   // content.setVisibility(View.VISIBLE);
                    Tipo = "2";
                    gotoMain(jsonDatauser,BANDCONFIR,id_user, UID);

                    break;
                case "0":
                    /*uusario ya existe*/
                   // Log.d("Data login","MENSAJE......"+ mensaje);

                    Toast.makeText(getApplicationContext(),
                            response.getString("mensaje"), Toast.LENGTH_LONG).show();

                    LoginManager.getInstance().logOut();
                    FirebaseAuth.getInstance().signOut();
                    gotoLogin();
                    break;
                case "1":
                    /*CCONTRASEÑA NO COICIDEN */
                    Log.d("Data login","MENSAJE......"+ "");
                    Toast.makeText(getApplicationContext(),
                            response.getString("mensaje"), Toast.LENGTH_LONG).show();
                    LoginManager.getInstance().logOut();
                    FirebaseAuth.getInstance().signOut();
                   // gotoLogin();
                    break;

            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public  void DoJson(){
        Map<String, String> Jsonperfil = new HashMap<>();
        Jsonperfil.put("id",id);
        Jsonperfil.put("name", Name +" "+ Lastname);
        Jsonperfil.put("email", Mail );
        Jsonperfil.put("picture", img_User);

        JSONObject jsonObject = new JSONObject(Jsonperfil);

        jsonDatauser= jsonObject;
    }

    /*
  * REGISTRO DE FORMA CLASICA SI UTLIZAR FACEBOOK*/
    public void ClassicSing(View view){

        User = usuario.getText().toString().replace(" ","");
        Pass = contraseña.getText().toString().replace(" ","");
        confirmpass= confirmcontraseña.getText().toString().replace(" ","");
        Name = nombre.getText().toString().replace(" ","");
        Lastname = apellido.getText().toString().replace(" ","");
        id = "";
        Mail = Correo.getText().toString().replace(" ","");
        cedula = Cedula.getText().toString().replace(" ","");
        classic=true;
        //checkterm.isChecked();

 //String url =URL+"sign-in.php";


        if(checkterm.isChecked()){
            progressBar.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
            Log.d("Term", "IF "+checkterm.isChecked());
            if (!(User.equals("")||Pass.equals("")||confirmpass.equals("")||Name.equals("")||Lastname.equals("")||Mail.equals("")||cedula.equals("")||fechaNacimiento.equals("0"))){/////campos em blanco
                if(Pass.equals(confirmpass)) {
                    if (Pass.length() > 5) {

                        firebaseAuth.createUserWithEmailAndPassword(Mail, Pass)
                                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            // Sign in success, update UI with the signed-in user's information
                                            Log.d(TAG, "createUserWithEmail:success");
                                            FirebaseUser user = firebaseAuth.getCurrentUser();
                                            //user.getEmail();
                                            UID = user.getUid();
                                            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                                            StringRequest request = new StringRequest(Request.Method.POST, URL + "sign-in.php",
                                                    new Response.Listener<String>() {
                                                        @Override
                                                        public void onResponse(String response) {
                                                            try {
                                                                JSONObject oJson = new JSONObject(response);

                                                                procesarRespuesta(oJson);
                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                                Log.d("Data login", "....." + e.getMessage());
                                                            }
                                                        }
                                                    },
                                                    new Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {
                                                            /*EN CASO DE ERROR */
                                                            Log.e("Data Login ERROR", " " + error.getMessage());
                                                            System.out.print("" + error);
                                                        }
                                                    }
                                            ) {
                                                /*TODAS LAS VARIABLES Q SE ENVIAN AL SERVIDOR */
                                                @Override
                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                    Map<String, String> parameters = new HashMap<>();
                                                    parameters.put("user", User);
                                                    parameters.put("nombre", Name);
                                                    parameters.put("apellido", Lastname); //text.replace(" ","")
                                                    parameters.put("mail", Mail);
                                                    parameters.put("user_pass", Pass);
                                                    parameters.put("celular", celular.getText().toString());
                                                    parameters.put("confir", confirmpass);
                                                    parameters.put("cedula", cedula);
                                                    parameters.put("fecha_nacimiento", fechaNacimiento);
                                                    parameters.put("uid", UID);
                                                    return parameters;
                                                }
                                            } ;
                                            /*
                                            request.setRetryPolicy(
                                                    new DefaultRetryPolicy(
                                                            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                                                            0,
                                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                                getRequestQueue().add(request);
                                            * */
                                            request.setRetryPolicy(
                                                    new DefaultRetryPolicy(
                                                            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                                                            0,
                                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                            queue.add(request);

                                        } else {
                                            // If sign in fails, display a message to the user.
                                            Log.w(TAG, "createUserWithEmail:failure", task.getException());

                                    /*Toast.makeText(EmailPasswordActivity.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();*/
                                            //updateUI(null);
                                            ShowDialog();
                                        }

                                        // ...
                                    }
                                });

                    }else{
                        progressBar.setVisibility(View.GONE);
                        content.setVisibility(View.VISIBLE);
                        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

                        builder.setMessage("Su contraseña debe tener minimo 6 caracteres")
                                .setTitle("Contraseña muy corta");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }else{
                    progressBar.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);

                    builder.setMessage("Su contraseñas no son iguales")
                            .setTitle("Contraseña  diferentes");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();

                }

                img_User = "{\"data\":{\"height\":120,\"is_silhouette\":false,\"url\":\"https:\\/\\/fb-s-d-a.akamaihd.net\\/h-ak-xlt1\\/v\\/t1.0-1\\/p120x120\\/17203046_1286998371377430_5342820977218930303_n_xx.jpg?oh=84f857499af0cbb6b6e1a931be42f49a&oe=596C046A&__gda__=1503240198_db88967ddbcd3483db0c3a7c66237111\",\"width\":120}}";
                DoJson();
                BANDCONFIR ="yes";
                ///fin campos en blanco
            }else{
                progressBar.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setMessage("Complete todos los campos correctamente")
                        .setTitle("Hay campos vacios");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        }else{
            Log.d("Term", " ELSE "+checkterm.isChecked());
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setMessage("Si desea utlizar este servicio debe aceptar los términos y condiciones")
                    .setTitle("Términos y condiciones");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }


    }

    public  void ShowDialog(){
        progressBar.setVisibility(View.GONE);
        content.setVisibility(View.VISIBLE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Revise todos los campos, quizas su correo este mal escrito")
                .setTitle("Parametro incorrecto");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void Nacimiento(View view){



        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR );
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        final  Date date1 = new Date(mYear - 18 , mMonth, mDay  );
        final  Date date2 = new Date(mYear - 80 , mMonth, mDay );


        Calendar min=Calendar.getInstance();
        min.set(1950, 1, 1, 0, 0);


        Calendar max=Calendar.getInstance();
        max.set(mYear-18, 11, 31, 0, 0);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                       // Log.d("date","After: "+date1.after(date));
                        btnNacimiento.setText("Fecha:"+ dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
                        fechaNacimiento = ""+year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(max.getTimeInMillis());
        datePickerDialog.getDatePicker().setMinDate(min.getTimeInMillis());

        datePickerDialog.show();    }
}
