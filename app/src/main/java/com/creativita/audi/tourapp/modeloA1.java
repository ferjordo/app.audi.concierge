package com.creativita.audi.tourapp;


import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class modeloA1 extends Fragment {
TextView titulo, slogan, descipciom;
ImageView principal, pequena1, pequena2, pequena3;
String   Titulo, descriipcion, src1, src2, src3, src4 ,modelo;
    public modeloA1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_modelo_a1, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            modelo = this.getArguments().getString("modelo");
            Titulo = this.getArguments().getString("titulo");
            descriipcion = this.getArguments().getString("descripcion");
            src1 = this.getArguments().getString("img1");
            src2 = this.getArguments().getString("img2");
            src3 = this.getArguments().getString("img3");
            src4 = this.getArguments().getString("img4");

        }


        titulo= (TextView)getActivity().findViewById(R.id.model_title);
        slogan= (TextView)getActivity().findViewById(R.id.model_slogan);
        descipciom= (TextView)getActivity().findViewById(R.id.txtModelDrescrip);
        principal =(ImageView)getActivity().findViewById(R.id.imgModelPrincipal);
        pequena1 =(ImageView)getActivity().findViewById(R.id.imgModellitle1);
        pequena2 =(ImageView)getActivity().findViewById(R.id.imgModellitle2);
        pequena3 =(ImageView)getActivity().findViewById(R.id.imgModellitle3);

        titulo.setText("Audi " + modelo);
        slogan.setText(Titulo);
        descipciom.setText(descriipcion);

        Picasso.with(getContext()).load(src1)
                .into(principal);
        Picasso.with(getContext()).load(src2)
                .into(pequena1);
        Picasso.with(getContext()).load(src3)
                .into(pequena2);
        Picasso.with(getContext()).load(src4)
                .into(pequena3);

        pequena1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(getContext()).load( src2)
                        .into(principal);                    }
        });
        pequena2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(getContext()).load(src3)
                        .into(principal);
                                  }
        });
        pequena3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(getContext()).load(src4)
                        .into(principal);
                           }
        });

       /* switch (modelo) {
            case "a1":
                titulo.setText("Audi A1");
                slogan.setText("Una gran apariencia y una gran idea");
                descipciom.setText("Naturaleza compacta: el nuevo Audi A1. La apariencia concentrada de una actitud moderna ante la vida, que ofrece posibilidades especiales, que presenta un rendimiento convincente y que siempre fascina. Además, está equipado con tecnologías eficientes. El Audi A1: un artista urbano que saciará tu apetito.");
               principal.setImageDrawable(getResources().getDrawable(R.drawable.a1_1));
                pequena1.setImageDrawable(getResources().getDrawable(R.drawable.a1_3));
                pequena2.setImageDrawable(getResources().getDrawable(R.drawable.a1_4));
                pequena3.setImageDrawable(getResources().getDrawable(R.drawable.a1_5));

                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a1_3));                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a1_4));                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a1_5));                    }
                });
                break;

            case  "a3":
                titulo.setText("Audi A3");
                slogan.setText("Apariencia impactante");
                descipciom.setText("¿Qué sucedería si la tecnología se pudiera utilizar de forma intuitiva? ¿O si el diseño se combinara con la funcionalidad innovadora? ¿Y si el carácter deportivo y la evolución se convirtieran en un único elemento?.\n" +
                        "Hemos encontrado la respuesta con el nuevo Audi A3. Su diseño habla un idioma único y su dinamismo impresiona en cada viaje. Rasgos que convierten al Audi A3 en un vehículo distinto al resto.");
                principal.setImageDrawable(getResources().getDrawable(R.drawable.a3_1));
                pequena1.setImageDrawable(getResources().getDrawable(R.drawable.a3_3));
                pequena2.setImageDrawable(getResources().getDrawable(R.drawable.a3_2));
                pequena3.setImageDrawable(getResources().getDrawable(R.drawable.a3_4));

                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a3_3));                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a3_2));                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a3_4));                    }
                });

                break;
            case  "a4":
                titulo.setText("Audi A4");
                slogan.setText("Concebido por la determinación");
                descipciom.setText("El nuevo Audi A4 es tan avanzado tecnológicamente que cualquier experiencia que tengas al subir en él te parecerá que es cosa de magia. Subir a bordo es sentir la máxima deportividad, elegancia e innovación. Encender sus faros es sentir que conviertes la noche en día. Pero, aunque lo parezca, confundir tus sentidos no es cosa de magia. Es el nuevo Audi A4.");
                principal.setImageDrawable(getResources().getDrawable(R.drawable.a4_1));
                pequena1.setImageDrawable(getResources().getDrawable(R.drawable.a4_2));
                pequena2.setImageDrawable(getResources().getDrawable(R.drawable.a4_3));
                pequena3.setImageDrawable(getResources().getDrawable(R.drawable.a4_2));

                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a4_2));                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a4_3));                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a4_2));                    }
                });

                break;
            case  "a5":
                titulo.setText("Audi A5");
                slogan.setText("Para conductores exigentes");
                descipciom.setText("Diseñado con un exterior totalmente nuevo, interior refinado y 252 caballos de fuerza, el  nuevo Audi A5 es atlético, elegante y emocionante. Y con sus tecnologías disponibles de asistencia al conductor y transmisión manual, es sofisticado y deportivo también.");
                principal.setImageDrawable(getResources().getDrawable(R.drawable.a5_1));
                pequena1.setImageDrawable(getResources().getDrawable(R.drawable.a5_3));
                pequena2.setImageDrawable(getResources().getDrawable(R.drawable.a5_4));
                pequena3.setImageDrawable(getResources().getDrawable(R.drawable.a5_2));
                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a5_3));                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a5_4));                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a5_2));                    }
                });
                break;
            case  "a6":
                titulo.setText("Audi A6");
                slogan.setText("Tecnología y carácter Audi");
                descipciom.setText("El Audi A6 reúne, en una simbiosis elegante y al mismo tiempo deportiva, innovadoras tecnologías con una conducción que sorprende por su agilidad. Un diseño que destila progreso y deportividad.La minuciosidad de un automóvil consiste en la interacción de todos sus puntos fuertes. Y sus numerosas posibilidades. El Audi A6 reúne, en una simbiosis elegante y al mismo tiempo deportiva, innovadoras tecnologías con una conducción que sorprende por su agilidad. El Audi A6 le fascinará desde el primer momento.");
                principal.setImageDrawable(getResources().getDrawable(R.drawable.a6_1));
                pequena1.setImageDrawable(getResources().getDrawable(R.drawable.a6_3));
                pequena2.setImageDrawable(getResources().getDrawable(R.drawable.a6_4));
                pequena3.setImageDrawable(getResources().getDrawable(R.drawable.a6_2));
                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a6_3));                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a6_4));                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a6_2));                    }
                });
                break;
            case  "a7":
                titulo.setText("Audi A7");
                slogan.setText("Eficiencia constante");
                descipciom.setText("Atracción, personalidad, carácter. el Audi A7 Sportback, la perfecta combinación entre las líneas Avant, Sedan y Coupé en un irresistible diseño capaz de atraer todas las miradas.");
                principal.setImageDrawable(getResources().getDrawable(R.drawable.a7_1));
                pequena1.setImageDrawable(getResources().getDrawable(R.drawable.a7_3));
                pequena2.setImageDrawable(getResources().getDrawable(R.drawable.a7_1));
                pequena3.setImageDrawable(getResources().getDrawable(R.drawable.a7_2));
                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a7_3));                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a7_1));                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a7_2));                    }
                });
                break;
            case  "a8":
                titulo.setText("Audi A8");
                slogan.setText("Eficiencia constante");
                descipciom.setText("Cuando el progreso es arte: La belleza expresada en imágenes. En un vehículo excepcional. El Audi A8. Imponente y elegante. Impresionante y atlético. Líneas perfectamente diseñadas para cumplir con su función. Brillante en cada detalle.");
                principal.setImageDrawable(getResources().getDrawable(R.drawable.a8_1));
                pequena1.setImageDrawable(getResources().getDrawable(R.drawable.a8_3));
                pequena2.setImageDrawable(getResources().getDrawable(R.drawable.a8_2));
                pequena3.setImageDrawable(getResources().getDrawable(R.drawable.a8_1));
                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a8_3));                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a8_2));                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.a8_1));                    }
                });

                break;
            case  "q3":
                titulo.setText("Audi Q3");
                slogan.setText("Actitud Progresista");
                descipciom.setText("Compacto, pero con un espacio interior completo. Su diseño progresivo y deportivo convierte al nuevo Audi Q3 en una verdadera atracción y destaca su marcado carácter de SUV urbano con gran capacidad de adaptación a todo tipo de terrenos. ");
                principal.setImageDrawable(getResources().getDrawable(R.drawable.q3_1));
                pequena1.setImageDrawable(getResources().getDrawable(R.drawable.q3_3));
                pequena2.setImageDrawable(getResources().getDrawable(R.drawable.q3_4));
                pequena3.setImageDrawable(getResources().getDrawable(R.drawable.q3_2));
                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.q3_3));                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.q3_4));                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.q3_2));                    }
                });

                break;
            case  "q5":
                titulo.setText("Audi Q5");
                slogan.setText("Más deportivo y versátil");
                descipciom.setText("El SUV combina la deportividad de una berlina de Audi con un carácter polivalente y un interior altamente flexible. Ya sea en materia de conectividad, eficiencia o sistemas de ayuda a la conducción, el nuevo Audi Q5 se coloca una vez más como referencia en su segmento.");
                principal.setImageDrawable(getResources().getDrawable(R.drawable.q5_1));
                pequena1.setImageDrawable(getResources().getDrawable(R.drawable.q5_3));
                pequena2.setImageDrawable(getResources().getDrawable(R.drawable.q5_2));
                pequena3.setImageDrawable(getResources().getDrawable(R.drawable.q5_1));
                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.q5_3));                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.q5_2));                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.q5_1));                    }
                });
                break;
            case  "q7":
                titulo.setText("Audi Q7");
                slogan.setText("Actitud Progresista");
                descipciom.setText("Atlético, atemporal, único. Con un aspecto deportivo realzado. Con más anchura y altura, y una presencia aún más impactante. Potente e impactante, pero aun así, ligero. Un diseño que habla un nuevo lenguaje.");
                principal.setImageDrawable(getResources().getDrawable(R.drawable.q7_1));
                pequena1.setImageDrawable(getResources().getDrawable(R.drawable.q7_3));
                pequena2.setImageDrawable(getResources().getDrawable(R.drawable.q7_4));
                pequena3.setImageDrawable(getResources().getDrawable(R.drawable.q7_2));
                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.q7_3));                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.q7_4));                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.q7_2));                    }
                });
                break;
            case  "tt":
                titulo.setText("Audi TT");
                slogan.setText("¡Échale la culpa a tu instinto!");
                descipciom.setText("Los nuevos diseños de los faros LED y Audi Matrix LED son fascinantes en todas sus características. El diseño exterior del Nuevo Audi TT rememora la primera generación con detalles como los arcos de rueda anchos y redondos, los tubos de escape mucho más centrados y la tapa del depósito de aluminio  con el nombre del modelo inscrito en ella.");
                principal.setImageDrawable(getResources().getDrawable(R.drawable.tt_1));
                pequena1.setImageDrawable(getResources().getDrawable(R.drawable.tt_3));
                pequena2.setImageDrawable(getResources().getDrawable(R.drawable.tt_4));
                pequena3.setImageDrawable(getResources().getDrawable(R.drawable.tt_2));
                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.tt_3));                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.tt_4));                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.tt_2));                    }
                });
                break;
            case  "r8":
                titulo.setText("Audi R8");
                slogan.setText("Nacido de la competición para dominar la ruta");
                descipciom.setText("El Audi R8 está diseñado en todos sus aspectos técnicos para hacerle sentir como si estuviera en un circuito, consiguiendo un dinamismo máximo, desde el chasis Audi Space Frame (ASF) hasta su aerodinámica o su nuevo sistema de tracción total quattro.");
                principal.setImageDrawable(getResources().getDrawable(R.drawable.r8_1));
                pequena1.setImageDrawable(getResources().getDrawable(R.drawable.r8_3));
                pequena2.setImageDrawable(getResources().getDrawable(R.drawable.r8_4));
                pequena3.setImageDrawable(getResources().getDrawable(R.drawable.r8_2));
                pequena1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.r8_3));                    }
                });
                pequena2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.r8_4));                    }
                });
                pequena3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        principal.setImageDrawable(getResources().getDrawable(R.drawable.r8_2));                    }
                });
                break;
        }*/

    }
}
