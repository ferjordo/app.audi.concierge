package com.creativita.audi.tourapp;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ConsultasServidor {
   Context context;

   ConsultasServidor (Context context){
       this.context = context;
       Log.d("Live","Consultando al servidor...");
   }

    private void procesaReady(JSONObject response) {
        try {
            String estado = response.getString("estado");
            String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();

                    Log.d("NOTIFICACION "," cambiar ventana: enviar notificación");
                    //  EnviarNotificacion(id, placa);
                    // dismiss();
                    break;
                case "2":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    //dismiss();
                    break;
                case "3":
                    Toast.makeText(getApplicationContext(),
                            mensaje, Toast.LENGTH_LONG).show();
                    //dismiss();

                    break;
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }
    ///////////CONTINUAR//////

    //////Inicia///
    public void valetIsReady( final String idcita, final String urlTrack){

        String url = Constantes.URL + "actualizarcita-url-track.php";
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                            procesaReady(oJson);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id",""+idcita);
                parameters.put("urlTrack",""+urlTrack);

                return parameters;
            }
        });


    }


    public  String GetUrlToTrack( final int idcita){
        Log.d("Live","Consultnado URL...");

        final String[] var = new String[1];
        String url = Constantes.URL + "get-url-track.php";
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("Live","Se ha obtenido respuesta del serivdor...");

                            JSONObject oJson = new JSONObject(response);
                            var[0] = procesaUrl(oJson);

                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("id",""+idcita);
                return parameters;
            }
        });

return var[0];
    }
    private String procesaUrl(JSONObject response) {
       String retorno="";
        Log.d("Live","Obteniendo UrL...");

        try {
            String estado = response.getString("estado");
            //String mensaje = response.getString("mensaje");
            switch (estado) {
                case "1":

                  Log.d("Live",""+response.getString("url"));
                  retorno= response.getString("url");
                    //Log.d("NOTIFICACION "," cambiar ventana: enviar notificación");
                    //  EnviarNotificacion(id, placa);
                    // dismiss();
                    break;

                case "0":
                    Toast.makeText(getApplicationContext(),
                            "Cita no existe", Toast.LENGTH_LONG).show();
                    //dismiss();
                break;

            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
   return retorno;
   }

    public void notificarLiveTracking( final String idcita, final String urlTrack){
//Log.d("Live","Se enviara la notifiaccion");
        String url = Constantes.URL + "Notificacion-track.php";
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject oJson = new JSONObject(response);
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error del servidor. Intente más tarde", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                "Compruebe su conexion a internet", Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parameters = new HashMap<>();
                parameters.put("cita",""+idcita);
                parameters.put("url",""+urlTrack);

                return parameters;
            }
        });


    }
}
